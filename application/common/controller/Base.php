<?php
namespace app\common\controller;
use think\exception\HttpException;
use think\Request as request;
use think\Response;
use think\Loader;
use think\Validate;
use app\publictool\controller\PulbicTool;
class Base{
    protected static $publicTool;
    protected $loader;
    public function __construct(){
        self::$publicTool = Loader::controller('\\app\\publictool\\controller\\PublicTool');
        $this->loader = new Loader();
    }

    protected static function sendResponse($input=null,$method=null){
        $publicTool = self::$publicTool;
        $input = strtolower($input);
        $resultTypeStock = ['get','put','post','select','delete'];
        $resultType = 'get';
        $inputData = [];
        $typeLocation = strpos($input,':');
        //查询获取的链接内容
        if(false === $typeLocation){
            throw new \Exception('获取的方式出现错误，如果您是前端开发人员请咨询后端！');
        }
        //设置获取方式
        $resultType = trim(substr($input,0,$typeLocation));

        if(false === strpos($input,'*')){
          $resultParams = explode(',',trim(substr($input,(++$typeLocation))));
          foreach($resultParams as $val){
            $resultInput = input($val);
            if(empty($resultInput)){
                continue;
            }
            $inputData[$val]=$resultInput;
          }
        }else{
            $inputData = input();
        }

        $request =request::instance();
        $module = $request->module();
        $controller = $request->controller();
        $action = $request->action();
        //排除的接口
        $unHashValueList = ['user/Index/register','user/Index/login','gym/Index/clearGymReservationCourse'];
        //判断是否在排除的接口内容中，转换用户的hashValue
        if(gettype($method) !== 'object'){
          throw new \Exception('传入的匿名函数有错误，如果您是前端开发人员请咨询后端！');
        }
        try{
          //转换hashVlaue的值
          if(!in_array(($module.'/'.$controller.'/'.$action),$unHashValueList)){
             $inputData['uuid'] = \app\user\model\User::parseHashValue($inputData['hash_value']);

          }
          $msg = $method($inputData,$publicTool);
        }catch(HttpException $e){
          $msg = $publicTool::msg('error',$e->getMessage(),$e->getStatusCode());
        }
        Response::create($msg,'json')->send();
        exit;
    }

}
