<?php
namespace app\common\model;
use app\common\model\ObserviceInterface;
use think\Loader;
class Observice extends ObserviceInterface{
    //观察者列表
    protected static $ObserviceStock=[];

    public static  function addObserivce($obService,$message){
        if(!class_exists($obService)){
            exception('指定的通知类不存在!');
        }
        if(!method_exists($obService,'receive')){
            exception('接收方法不存在!');
        }
        //左侧为通知方法=>右侧为接受方法
        self::$ObserviceStock[][$obService]=$message;
    }

    //通知观察者
    public static  function notify(){
        $stock = self::$ObserviceStock;
        foreach($stock as $key=>$val){
              key($val)::receive($val[key($val)]);
        }
    }
}
