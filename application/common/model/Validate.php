<?php
namespace app\common\model;
/**
 * [Validate 自定义验证器]
 */
class Validate{
  /**
   * [timeStamp 验证是否为时间戳格式]
   * @param  integer $value [输入时间戳]
   * @return [bool]         [返回布尔类型的内容]
   */
  public static function timeStamp($value=0){
      //不是数字类型
      if(!is_numeric($value)){return false;}
      $errorType = 19700101080015; //如果返回的是1970年的时间的话那么就证明时间戳是错误的
      $dateTime = date('YmdHis',$value);
      if($errorType == $dateTime){
          return false;
      }
      return true;
  }

}
