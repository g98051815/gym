<?php
namespace app\common\model;
//观察者接收方的接口
interface ObserviceReceiveInterface{

    public static function receive($message);

}
