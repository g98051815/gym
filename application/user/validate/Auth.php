<?php
namespace app\user\validate;
use think\Validate;

class Auth extends Validate{

  //修改教练用户组的接口
  public $fitnessChangeRule = [

    'uuid'=>'require|fitness',

  ];

  //修改未会员用户组的接口
  public $memberChangeRule = [

    'uuid'=>'require|member',

  ];

  //修改为老板用户组的接口
  public $bossChangeRule = [

    'uuid'=>'require|boss',

  ];


  protected $message = [
      'uuid.require'=>'用户的编号不能为空',
      'uuid.boss'=>'不能修改角色',
      'uuid.member'=>'不能修改角色',
      'uuid.fitness'=>'不能修改角色',
      'group.require'=>'用户组不能为空',
  ];


  protected function fitness($val){
    $signing = db('signing_fitness_instructor')->where(['fitness_instructor_id'=>$val])->count();
    if($signing > 0){
        return false;
    }
    return true;
  }


  protected function boss($val){
      $signing = db('gym_info')->where(['boss'=>$val])->count();
      if($signing > 0){
          return false;
      }
      return true;
  }

  protected function member($val){
      $signing = db('signing_fitness_instructor')->where(['member_id'=>$val])->count();
      if($signing > 0){
          return false;
      }
      return true;
  }






}
