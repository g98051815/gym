<?php
namespace app\user\model;
use app\common\model\Base;
use app\gym\model\MemberSigning;
use app\gym\model\FitnessTrainerSigning;
use app\reservation\model\ArrangeCourse;
use app\user\model\Icinfo;
use app\user\model\User;
use app\gym\model\GymInfo;
use think\Cache;
/**
 * [BossManager 获取老板主面板的信息]
 */
class BossManager extends Base{
      /**
       * [getManagerInfo 主面板信息]
       * 信息中需要获取如下信息
       * 签约会员的数量
       * 签约教练的数量
       * 今日约课的数量
       * 明日约课的数量
       * 上月新增的数量
       * 本月新增的数量
       * 总共课时的数量
       * 剩余课时的数量
       * @return [type] [返回老板主面板所需的信息]
       */
      public static function getManagerInfo($info){
          //查询当前老板的控制台是否需要推送禁用信息
          $disable = db('disable')->where(['uuid'=>$info])->find();
          if(!is_null($disable)){
            $disable['disabled'] == 1? abort(-20,json_encode($disable)):'';
          }
          $input = input();
          $where = ['boss'=>$info,'field'=>[
            'unique_id',
            'boss',
            'store_title'
            ]];
          //后续添加的流程
          if(array_key_exists('gym_id',$input)){
              //健身房查询
              $where = ['unique_id'=>$input['gym_id'],'list','field'=>[
                'unique_id',
                'boss',
                'store_title'
                ]];
          }
          //老板的角色
          $gymList = GymInfo::info($where);
          //健身房的数组
          $gymData = array_column($gymList['list'], 'unique_id');
          //个人自己的信息
          $result['self_info'] = User::info(['uuid'=>$info]);

          $result['gym_list'] = $gymList;

          //营业执照的信息
          $result['icinfo'] = Icinfo::info(['uuid'=>$info]);
          //所有的店面
          $result['all_gym_item'] = $gymList;
          //健身房下面的会员数量
          $result['member_count'] = MemberSigning::gymMemberCount(['gym_id'=>$gymData]);
          //健身房下面的教练列表
          $result['fitness_count'] = FitnessTrainerSigning::gymFitnessCount(['gym_id'=>$gymData]);
          //健身房下面的今日约课
          $result['course_make_today'] =  ArrangeCourse::gymCourse(['gym_id'=>$gymData]);
          //健身房下面的明日约课
          $result['course_make_tommorrow'] = ArrangeCourse::gymCourse(['gym_id'=>$gymData]);
          //上个月签约会员的数量
          $result['last_month_member_count'] = MemberSigning::gymMemberCount(['gym_id'=>$gymData,'date'=>'-1M']);
          //本月新增
          $result['at_month_member_count'] = MemberSigning::gymMemberCount(['gym_id'=>$gymData,'date'=>'1M']);
          //所有课时统计
          $result['total_hours'] = MemberSigning::gymMemberSum(['field'=>'bought_class','gym_id'=>$gymData]);
          //所有剩余课时
          $result['total_rest_hours']= MemberSigning::gymMemberSum(['field'=>'rest_of_class','gym_id'=>$gymData]);

          return $result;
      }

}
