<?php
namespace app\user\model;
use app\common\model\Base;
use app\common\model\Observice;
use think\Validate;
/**
 * app\user\model\MemberSigningApplication
 * [MemberSigningApplication 会员签约申请模型]
 */
class MemberSigningApplication extends Base{

  protected $table='gym_member_sign_fitness_application';

  protected static  $cacheTag='member_sign_application'; //缓存的名称,关闭使用缓存直接使用false

  protected static  $autoRefreshCache=true; //增加，修改，删除，自动更新缓存

  protected static  $autoPushCreateTime = true; //自动添加时间

  protected static  $autoPushUpdateTime = true; //自动添加修改时间

  /**
   * [memberSignApplication 受理会员的签约申请]
   * @return [Boolean] [true|false]
   */
  public static function memberSignApplication(){
      return self::couSave(
        [
          ['applicant_id','require|applicanted',' :申请人的编号不能为空！|您今天已经申请过了请明天再试'],
          ['acceptance_id','require','受理人的编号不能为空！'],
        ],
        function($input,$self){
            //添加用户的申请界面
            $allowField = true;
            $dataObject = new $self();
            //找到会员是否曾经和教练进行过签约
            $response = $dataObject->data($input)->allowField($allowField)->isUpdate(false)->save();
            //通知中心
            Observice::addObserivce('\\app\\user\\model\\AlertsMsg',['addressee'=>$input['acceptance_id'],'title'=>'签约申请','type'=>1,'addresser'=>$input['applicant_id'],'index_id'=>$dataObject->getLastInsID()]);
            //通知接口
            Observice::notify();
            return $response;
        }
      );
  }


  /**
   * [info 使用id查询申请的信息]
   * 一般是使用内部的通知接口进行访问的
   * @return [type] [返回的信息]
   */
  public static function info($info=[]){

      $validate = new Validate(
        ['id'=>'require'],
        ['id.require'=>'签约消息编号不能是空的！']
      );

      if(!$validate->check($info)){
          abort(-0056,$validate->getError());
      }

      $where = ['msa.id'=>$info['id'],'agree'=>2];

      $sqlQuery = self::sqlParams($info,
      [
        'field'=>
        [
          'msa.*',
          'usi.uuid',
          'usi.head_figure',
          'usi.nick_name',
          'usi.name'
        ],
      ]);

      $sqlResult = function($where,$sqlQuery){

         $result = self::where($where)->alias('msa');
         $result->field($sqlQuery['field']);
         $result->join('__USER_INFO__ usi','msa.applicant_id = usi.uuid','INNER');
         return $result;
      };

      $result = $sqlResult($where,$sqlQuery);

      $response = self::infoOfCache($result);

      return $response[0];
  }

  protected static function validateExtend($validate){
        //原有的验证模型
        parent::validateExtend($validate);
        $validate->extend(
          [
            //检查今天是否已经申请过了，最多一次
            'applicanted'=>function($val){
                //今天的时间
                $return = true;
                $valueData = self::getInputData();
                $where = ['applicant_id'=>$valueData['applicant_id'],'acceptance_id'=>$valueData['acceptance_id']];
                //查询今天是否有被申请过
                if(self::where($where)->whereTime('create_time','today')->count() > 0){
                    $return = false;
                }
                return $return;
            },
          ]
        );

  }

  /**
   * [agreeSign 教练同意签约]
   * @return [type] [description]
   */
  public static function agreeSign(){
          //修改状态
            $info = self::info(self::getInputData());
            if(empty($info)){abort(-22,'消息已被操作！');}
            self::monity();
            $notify=[
              'addressee'=>array_column($info,'applicant_id')[0],
              'title'=>'教练同意了您的签约申请！',
              'type'=>1,
              'index_id'=>self::getInputData('id'),
              'addresser'=>array_column($info,'acceptance_id')[0],
            ];
            Observice::addObserivce('\\app\\user\\model\\AlertsMsg',$notify);
            Observice::notify();
            return true;
  }

  /**
   * [refuseSign 教练拒绝签约]
   * @return [type] [description]
   */
  public static function refuseSign(){
           $info = self::info(self::getInputData());
           if(empty($info)){abort(-22,'消息已被操作！');}
           self::monity();
           $notify=[
            'addressee'=>array_column($info,'applicant_id')[0],
            'title'=>'教练拒绝了您的签约申请！',
            'type'=>1,
            'index_id'=>self::getInputData('id'),
            'addresser'=>array_column($info,'acceptance_id')[0],
           ];
           Observice::addObserivce('\\app\\user\\model\\AlertsMsg',$notify);
           Observice::notify();
           return true;
  }

  /**
   * [monity 修改操作]
   * @return [type] [description]
   */
  public static function monity(){
      return self::couSave(
        [
          ['id','require','消息的编号不能为空!'],
        ],
        function($input,$self){
          $allowField = true;
          $dataObject = new $self();
          $where = ['id'=>$input['id']];
          return $dataObject->allowField($allowField)->isUpdate(true)->save($input,$where);
        }
      );
  }


}
