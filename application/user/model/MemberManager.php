<?php
namespace app\user\model;
use app\common\model\Base;
use app\user\model\User as user;
use app\reservation\model\ArrangeCourse;
use app\gym\model\MemberSigning as msing;
/**
 * [MemberManager 会员的主面板信息]
 */
class MemberManager extends Base{
      protected static  $cacheTag='user'; //缓存的名称,
      protected $table = 'gym_user_info';
      protected static  $autoPushCreateTime = true; //自动添加时间
      protected static  $autoPushUpdateTime = true; //自动添加修改时间
      /**
       * [getManagerInfo 获取会员的主面板信息]
       * 信息中需要获取如下的信息
       * 1.用户的信息
       * 2.用户的头像
       * 3.用户的格言
       * 4.用户所属的健身房
       * 5.用户的剩余课时
       * @return [type] [会员会员的内容信息]
       */
      public static function getManagerInfo($uuid){
          $where = ['uuid'=>$uuid];
          $field= [
              'head_figure',
              'last_login_time',
              'motto',
              'name',
              'nick_name',
              'status',
              'gender',
              'phone_num',
              'uuid'
          ];
          $result = self::where($where)->field($field);
          //连接健身房查询当前健身房的名称
          $userInfo = self::infoOfCache($result);
          //查询当前会员健身房和教练的信息
          $fitnessGym = msing::info(['member_id'=>$uuid]);
          if(!empty($fitnessGym)){
              //健身房的和教练的信息
              $userInfo[0]['gym_fitness_info'] = $fitnessGym[0];
          }
          $userInfo[0]['latley_course_time'] = ArrangeCourse::getMemberLatleyCourse($uuid);
          return $userInfo[0];
      }

}
