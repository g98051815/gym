<?php
namespace app\user\model;
use app\common\model\Base;
/**
 * [Motto 用户的格言模板]
 */
class Motto extends Model{

  protected $table = 'gym_motto_tpl';

  public static function info(){
    return self::where('*')->select();
  }

}
