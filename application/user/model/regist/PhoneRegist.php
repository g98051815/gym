<?php
namespace app\user\model\regist;
use think\Validate;
use app\publictool\controller\PublicTool;
use think\Loader;
use app\common\model\Base;
/**
*手机号码注册功能
*1.此功能一共涉及三个功能
*1.验证码
*2.手机验证码
*/
class PhoneRegist extends Base{

    protected $table = 'gym_user_info';

    protected $type =
    [
        'phone_num'=>'integer',
    ];
    /**
    *用户注册
    * id , name , nick_name , phone_num , email_address , password , gender , create_time , note ,unique_identification
    *
    */
    public static function signUp($data){
        $data = self::getInputData();
        //$response = $result->getData();
        //dump($response);exit;
        $validate = new Validate(
            [
                'name'=>'require|max:25',
                'nick_name'=>'require',
                'phone_num'=>function($val,$rule){
                    //验证手机号码的正则表达式

                    if(empty($val)){
                        return '手机号码不能为空！';
                    }
                    $patten ='/^0?(13|14|15|17|18)[0-9]{9}$/';
                    if(!preg_match($patten,$val)){
                        return '手机号码错误！';
                    }
                    $count = self::where(['phone_num'=>$val])->count();
                    if($count){
                        return '您已经注册过了，请登录';
                    }
                    return true;
                },
                'email_address'=>'email|require',
                'password'=>'require|max:15',
            ],
            [
                'name.require'=>'名称必须填写',
                'name.max'=>'名称最多不能超过25个字符',
                'email_address.require'=>'邮箱地址不能为空！',
                'email_address.email'=>'邮箱格式不正确!',
                'nick_name.require'=>'昵称必须填写',
                'phone_num'=>'手机号码必须填写',
                'password.require'=>'密码必须填写',
                'password.max'=>'密码必须是15位',
            ]
        );

        $result = $validate->check($data);

        if(!$result){
            abort(-0002,$validate->getError());
        }
        $allowField = ['name','nick_name','phone_num','email_address','password','uuid','create_time','last_login_time'];
        $data['password'] = PublicTool::passwordEncryption($data['password']);
        $data['phone_num'] = (int)($data['phone_num']);
        $data['uuid'] = PublicTool::createUniqueId(); //生成唯一的id
        $data['create_time'] = time();
        $data['last_login_time'] = time();
        $dataObject = new self();
        $save = $dataObject->data($data)->allowField($allowField)->isUpdate(false)->save();
        if(!$save){
          abort(-0004,'注册失败请重新尝试!');
        }
          $relObj = static::where(['uuid'=>$data['uuid']])->find();
          $login = Loader::model('\\app\\model\\user\\login\\PhoneLogin');
          $user = Loader::model('\\app\\model\\user\\User');
          $hashValue = $login::generateUniqueSignForUser();
          //进行登录操作
          $login = $user::monity(['uuid'=>$data['uuid'],'hash_value'=>$hashValue]);
          //使用观察者通知权限中心进行分配权限
          Observice::addObservice('\\app\\user\\model\\AuthGroupRelation');
          Observice::notify();
          //注册成功返回哈希值的内容
          return $hashValue;
        }


}
