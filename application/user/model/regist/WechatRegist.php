<?php
namespace app\user\model\regist;
use app\common\model\Base;
/**
 * [WechatRegist 微信注册类]
 * 微信小程序测试项目 ：密码： 82ec7b138d65e1d6c5835ee62c7307a6
 * 微信小程序测试项目appid ： wx7f5eb47f46b52ca1
 */
class WechatRegist extends Base{

    protected $table = 'gym_other_account';

    protected static $baseUrl = 'https://api.weixin.qq.com/sns/jscode2session'; //微信登录的基础地址

    protected static $appid = 'wx7f5eb47f46b52ca1'; //微信的appid

    protected static $secret = '82ec7b138d65e1d6c5835ee62c7307a6';  //微信的app密码

    protected static $jscode = '';  //微信的jscode

    protected static $grant_type='authorization_code';  //获取的内容

    protected static $platFormType = 3; //微信小程序平台代表3

    protected static  $cacheTag='platform'; //缓存的名称,关闭使用缓存直接使用false

    protected static  $autoPushCreateTime = false; //自动添加时间

    protected static  $autoPushUpdateTime = false; //自动添加修改时间

    /**
     * [signIn 登录菜单]
     * @param  array  $data [登录内容]
     * @return [type]       [此参数没有返回追]
     */
    public static function signIn(){
        $data = self::getInputData();
        $baseInfo = false;
        $roleInfo = false;
         if(!array_key_exists('code',$data)){
             abort(-0007,'缺少微信登录的jscode');
         }
        self::$jscode = $data['code'];
        $user = '\\app\\user\\model\\User';
        $request = self::requestSessionKey();//获取sessionKey
        if(self::userExists($request)){
          //当查询存在这个内容的时候返回true
          $result = self::info($request);
          $hashValue = \app\user\model\User::hashValue($result['uuid']);
          $roleInfo = \app\user\model\Auth::getRoleInfo($result['uuid']);
          if(false !== $roleInfo){
              $baseInfo = true;
          }
        }else{
          $user = $user::newUser();
          $uuid = $user['uuid'];
          $hashValue= $user['hash_value'];
          $data['uuid'] = $uuid;
          $data['platform_type'] = self::$platFormType;
          $data['openid'] = $request['openid'];
          $dataObject = new self();
          $result = $dataObject->data($data)->allowField(true)->isUpdate(false)->save();
          if(false == $result){
              abort(-32,'系统错误请重试！');
          }

        }
        $response=['openid'=>$request['openid'],'session_key'=>$request['session_key'],'hash_value'=>$hashValue,'base_info'=>$baseInfo,'role_info'=>$roleInfo];
        return $response;
  }



    protected static function requestSessionKey(){
        $baseUrl = self::$baseUrl;
        $requestData['appid'] = self::$appid;
        $requestData['secret'] = self::$secret;
        $requestData['js_code'] =self::$jscode;
        $requestData['grant_type'] = self::$grant_type;
        $requestParams = http_build_query($requestData);
        $curl = curl_init();
        $curlOpt[CURLOPT_URL] = $baseUrl.'?'.$requestParams; //基础的内容
        $curlOpt[CURLINFO_SSL_VERIFYRESULT] = false;
        $curlOpt[CURLOPT_SSL_VERIFYHOST]=false;
        $curlOpt[CURLOPT_SSL_VERIFYPEER]=false;
        $curlOpt[CURLOPT_RETURNTRANSFER]=true;
        $curlOpt[CURLOPT_CONNECTTIMEOUT_MS] = 80000; //8秒的超时时间
        curl_setopt_array($curl,$curlOpt);
        $exec = curl_exec($curl);
        $error = curl_error($curl);
        if(!empty($error)){abort(curl_errno($curl),$error);}
        curl_close($curl);
        $response = json_decode($exec,true);
        if(array_key_exists('errcode',$response)){
                  abort(-0004,'微信的js授权码无效,微信错误：'.$response['errmsg'].',微信的错误编码:'.$response['errcode']);
        }
        return $response;
    }


    /**
     * [userExists 查询用户是否存在]
     * @param  [integer] $openId [开放平台的id]
     * @return [boolean]         [布尔类型的返回值]
     */
    protected static function userExists($openId){
        $platFormType = self::$platFormType;
        $where = ['platform_type'=>$platFormType,'openid'=>$openId['openid']];
        $count = self::where($where)->count();
        if($count > 0){
          //证明已经注册过了
            return  true;
        }
        //证明没有注册过
        return false;
    }


    public static function info($data=[]){
        $where=['platform_type'=>self::$platFormType,'openid'=>$data['openid']];
        return self::where($where)->find();
    }

}
