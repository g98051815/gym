<?php
namespace app\user\model;
use app\common\model\Base;
use think\Loader;
class AuthGroupRelation extends Base{

  protected $table = 'gym_auth_group_relation';
  protected static $publicTool = null; //开放工具

  public static function init(){
    parent::init();
    self::$publicTool = Loader::controller('\\app\\publictool\\controller\\PublicTool');
  }

  /**
   * [recevice 观察者模式用户，给新注册的用户分配权限使用]
   * @param  [type] $message [获取到的内容消息]
   * @return [void]          [此方法没有返回值]
   */
  public static function recevice($message){
      self::setInputData($message);
      self::configurationPower();
  }

  /**
   * [configurationPower 分配用户的权限]
   * @return [type] [分配用户的权限]
   */
  public static function configurationPower(){
      $inputData = self::$inputData;
      $save = false;
      $where = [];
      $validate = new Validate(
        [
          'uid'=>'require',
        ],
        [
          'uid.require'=>'用户的id错误！'
        ]
      );
      if($validate->check($inputData)){
          abort(-0002,$validate->getError());
      }
      $dataObject = new self();
      if(!array_key_exists('group_id',$inputData) || empty($inputData['group_id'])){
          $inputData['group_id'] = 3;
      }else{
        $where = ['uid'=>$data['uid']];
        if(self::where($where)->count() > 0){
            $save = $dataObject->allowField(['uid','group_id'])->save($inputData,$where);
        }else{
            $save = $dataObject->data($inputData)->save();
        }
        if(!$save){
          abort(-0004,'授权失败，请重试！');
        }

        return true;
      }

  }

  /**
   * [monityRule 删除当前用户的角色信息]
   * @return [type] [description]
   */
  public static function monityRule(){
    $input = self::getInputData();
    $allowField = true;
    $validate = Loader::validate('Auth');
    $auth = Loader::model('\\app\\user\\model\\Auth');
    $role = $auth::getRoleInfo($input['uuid']);
    $get = $role.'ChangeRule';
    $validate->rule($validate->$get)->check($input)?:abort($validate->getError());
    $input['group_id'] = $auth::getRoleStock($input['group_id']);
    $dataObj = new self();
    $where = ['uid'=>$input['uuid']];
    $dataObj->where($where)->delete()?:abort(-15,'操作失败');
    return [];
  }

  /**
   * [info 获取用户组的信息,可以使用用户进行，同时也可以使用用户组进行搜索]
   * @param  integer $limit     [限制的行数]
   * @param  integer $page      [限制的页数]
   * @param  array   $condition [搜索的条件]
   * @return [array]             [返回的内容]
   */
  public static function info($limit=1,$page=1,$condition=[]){
      $where = [];
      if(array_key_exists('uid',$condition)){
            $where = ['uid'=>$condition['uid']];
      }
      if(array_key_exists('group_id',$condition)){
            $where = ['group_id'=>$condition['group_id']];
      }else{
        abort(-0006,'请键入搜索条件！');
      }
      $response = self::where($where)->limit($limit)->page(1)->select();
      return collecton($response)->toArray();
  }


  /**
   * [setRole 设置用户的角色]
   */
  public static function setRole(){
      return self::couSave(
        [
          ['uuid','require|exists','用户的:编号不能为空！|已经设置完成角色不能重新设置！'],
          ['role','require','用户的角色不能为空！']
        ],
        function($input,$self){
            $dataObject = new $self();
            $auth = '\\app\\user\\model\\Auth';
            $roleStock = $auth::$roleStock;
            $allowField = true;
            $input['uid'] = $input['uuid'];
            $input['group_id'] = $roleStock[$input['role']];
            if($input['group_id'] == 2){
                  \app\gym\model\FitnessTrainerSigning::setInputData(
                    [
                      'gym_id'=>config('signing.default_gym_id'),
                      'fitness_instructor_id'=>$input['uid'],
                    ]
                  );
                  //和默认的健身房进行签约
                  \app\gym\model\FitnessTrainerSigning::signing();
            }
            unset($input['role']);
            unset($input['uuid']);
            return $dataObject->data($input)->allowField($allowField)->isUpdate(false)->save();
        }
      );
  }





  public static function validateExtend($validate){
      parent::validateExtend($validate);
      $validate->extend(
        [
          'exists'=>function($val){
              $return = true;
              $role = \app\user\model\Auth::getRoleInfo($val);
              if(false !== $role){
                  $return = false;
              }
              return $return;
          }
        ]
      );
  }



}
