<?php
namespace app\user\model;
use app\common\model\Base;
use think\Validate;
use app\common\model\ObserviceReceiveInterface;
class AlertsMsg extends Base implements ObserviceReceiveInterface{
    protected $table = 'gym_notifycation';
    protected static $body=[];
    protected $messageQueue = [
      1=>'\app\\user\\model\\MemberSigningApplication',//消息通知类型
      3=>'\\app\\reservation\\model\\ReservationCourse', //授权类型表示是否同意
      2=>'\\app\\reservation\\model\\ReservationCourse',//授权类型表示是否同意
      4=>'\\app\\gym\\model\\Unsigning',//解约通知
      5=>'\\app\\gym\\model\\Unsigning',//课程时间增加或者减少
    ];
    //接收通知的内容

    public static function receive($message){
        self::$body = $message;
        self::input();
    }

    /**
    *输入消息
    *`addressee` char(64) NOT NULL COMMENT '收信人',
    *`title` char(80) NOT NULL COMMENT '信息抬头',
    *`type` tinyint(4) NOT NULL COMMENT '消息类型',
    *`create_time` int(15) unsigned NOT NULL DEFAULT '0' COMMENT '消息的创建时间',
    *`reading` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '已读否;1:已读，0:未读',
    *`addresser` char(64) NOT NULL COMMENT '发信人',
    */
    public static function input($input=[]){
        if(empty($input)){
            $input = self::$body;
        }
        $validate = new Validate(
          [
            'addressee'=>'require',
            'title'=>'require',
            'type'=>'require',
            'addresser'=>'require'
          ],
          [
            'addressee.require'=>'收信人必须存在!',
            'title.require'=>'标题必须存在！',
            'type.require'=>'消息类型必须存在！',
            'addresser.require'=>'发件人必须存在！'
          ]
        );
        if(!$validate->check($input)){
              abort(-0005,$validate->getError());
        }
        $dataObject = new self();
        $input['create_time'] = time();
        $result = $dataObject->data($input)->isUpdate(false)->save();
        if(!$result){
            abort(0006,'通知失败，您可以选择手动通知！');
        }
        return true;
    }

    /**
     * [info 查询信息]
     * @param  [type]  $addressee [收信人]
     * @param  integer $limit     [限制行数]
     * @param  integer $page      [第几页]
     * @param  string  $orderBy   [排序规则]
     * @return [array]             [返回数组类型的内容]
     */
    public static function info($addressee,$limit=10,$page=1,$order='al.create_time desc'){
        $validate = new Validate(
          [
            'uuid'=>'require'
          ],
          [
            'uuid.require'=>'用户的编号不能为空！',
          ]
        );
        !$validate->check($addressee)?abort(-0025,$validate->getError()):'';
        $where = ['addressee'=>$addressee['uuid'],'reading'=>0];
        $field = ['al.id','al.addressee','al.title','al.type','al.create_time','al.reading','al.addresser','usi.head_figure','al.index_id'];
        $response = self::where($where)->alias('al')->join('__USER_INFO__ usi','al.addresser = usi.uuid','INNER')->limit($limit)->field($field)->page($page)->order($order)->select();
        return $response;
    }

    /**
     * 已经读取
    */
    public static function alReading(){
        self::couSave(
            [],
            function($input,$self){
                $where = ['id'=>$input['id']];
                $dataObj  = new $self();
                $allowField = ['reading'];
                $input['reading'] = 1;
                return $dataObj->allowField($allowField)->save($input,$where);
            }

        );
        return [];
    }

    /**
     * [detail 消息的详情信息,使用策略模式进行获取内容]
     * @param  [integer] $alertId      [消息的id]
     * @param  [integer] $indexId      [消息的id]
     * @param  [消息发送者的编号] $addresser  [消息的发送者编号]
     * @param  [type] $headerFigure [发送消息人的头像]
     * @return [type]               [返回数组形式的信息]
     */
    public static function detail($alertId,$indexId,$addresser,$headerFigure){

    }

    /**
    *修改
    */
    public static function montify(){
      $inputData = self::$input;
    }

    /**
    *已经读过了
    */
    public static function alreadyReading($id=null){
        if(is_null($id)){
            abort(-0001,'消息的id不能为空！');
        }
        $where = ['id'=>$id,'reading'=>0];
        $save = self::save(['reading'=>1],$where);
    }




}
