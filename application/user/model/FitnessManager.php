<?php
namespace app\user\model;
use app\common\model\Base;
use app\reservation\model\ArrangeCourse as course;
use app\gym\model\MemberSigning;
use app\user\model\User;
use app\gym\model\FitnessTrainerSigning;
class FitnessManager extends Base{

    /**
     * [getManagerInfo 信息中需要获取如下的信息]
     * 需要获取的信息列表如下所示:
     * 1.目前下面会员的数量
     * 2.所有会员剩余的课时数量
     * 3.历史的总课时的数量
     * 4.
     * 5.今日约课的列表信息
     * 6.今日约课的列表信息
     * 7.
     * @param  [type] $uuid [用户的uuid信息]
     * @return [type]       [返回所需的必要信息]
     */
    public static function getManagerInfo($uuid){
       //第一次登陆如果没有签约健身房的话就和默认的健身房进行签约
        if(FitnessTrainerSIgning::signingCount(['fitness_instructor_id'=>$uuid]) < 1){
          //和默认的健身房进行签约操作
          FitnessTrainerSIgning::setInputData(['fitness_instructor_id'=>$uuid,'gym_id'=>config('signing.default_gym_id')]);
          FitnessTrainerSigning::signing();
        }
        //教练的个人信息
        $response['user_info'] = User::info(['uuid'=>$uuid]);
        $response['sign_gym_info'] = FitnessTrainerSigning::gymFitnessSigningInfo(['fitness_id'=>$uuid]);
        //目前教练的约课数量
        $aboutClass = course::aboutClassInfo(['fitness_id'=>$uuid,'condition'=>'today']);
        //目前教练下面签约会员的数量
        $signingSum = MemberSigning::signingMemberCount($uuid);
        $restOfClass = MemberSigning::fitnessRestClassInfoSum($uuid);
        //获取教练的剩余课程时间数量
        $response['about_class']['first'] = 0; //今日最早约课的
        $response['about_class']['last'] = 0; //今日最晚约课的
        $response['about_class']['today'] = $aboutClass['list']; //今日约课信息
        $response['about_class']['count'] = $aboutClass['count']; //约课的数量总计
        $response['about_class']['surplus'] = $aboutClass['surplus']; //约课的数量剩余
        $response['signing_sum'] = $signingSum; //当前签约的会员总数量
        $response['rest_of_class'] = $restOfClass; //当前剩余课程的总数量
        $response['history_class_count'] = MemberSigning::historyClassCount(['fitness_id'=>$uuid]);
        if($aboutClass['first'] !== 0){
            //设置今日的最早时间
            $response['about_class']['first'] = $aboutClass['first'];
            $response['about_class']['last'] = $aboutClass['first'];
        }
        if($aboutClass['last'] !== 0){
            //设置今日的最晚时间
            $response['about_class']['last'] =$aboutClass['last'];
        }
        return $response;
    }

}
