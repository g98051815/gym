<?php
namespace app\user\model;
use app\common\model\Base;
/**
 * 上传工商信息的内容：完成
 *当用户选择的角色是老板的时候需要录入老板的工商资料信息
 *提交用户的工商资料
*/
class Icinfo extends Base{
     protected $table = 'gym_icinfo';  //连接的健身房表

     protected static  $cacheTag='base'; //缓存的名称,关闭使用缓存直接使用false

     protected static  $autoRefreshCache=true; //增加，修改，删除，自动更新缓存

     protected static  $autoPushCreateTime = true; //自动添加时间

     protected static  $autoPushUpdateTime = true; //自动添加修改时间

     /**
      * [initial 调用初始化的方法]
      * @return [type] [Boolean值的内容非真即是假的]
      */
     public static function initial(){

     }

     /**
      * [push 新增用户的查询内容]
      * @return [type] [Boolean值的内容非真即是假的]
      */
     public static function push(){
        //获取所有的内容
        $inputData = self::getInputData();
        return self::couSave(
          [
            ['uuid',function($val,$data){
                $return = true;
                $where = ['uuid'=>$val];
                if(empty($val)){
                    $return = '用户的编号不能为空！';
                }
                if(self::where($where)->count() > 0){
                    $return =  '用户的编号已经存在,so不需要更改！';
                }
                return $return;
            }],
            ['legal_person_name','require','法人的姓名不能为空！'],
            ['legal_person_card_id','require|card_id','法人的身份证号码:不能为空！| 格式不正确！'],
            ['legal_person_phone_num','require|phone','法人的手机号码:不能为空！| 格式不正确！'],
            ['business_license','require','营业执照必须上传！']
          ],
          function($input,$self){
              $allowField = true;
              $dataObject = new $self();
              return $dataObject->data($input)->allowField(true)->isUpdate(false)->save();
          },$inputData
        );
     }

     /**
      * [monity 修改方法]
      * @return [type] [Boolean 非真即是假]
      */
     public static function monity(){
       //获取所有的内容
       $inputData = self::getInputData();
       return self::couSave(
         [
           ['uuid',function($val,$data){
                $return  = true;
                $where = ['uuid'=>$val];
                if(empty($val)){
                  $return  = '用户的编号不能为空！';
                }
                if(self::where($where)->count() < 1){
                  $return  = '用户的编号不存在，so无法修改！';
                }
                return $return;
           }],
         ],
         function($input,$self){
             $dataObject = new $self();
             $where = ['uuid'=>$input['uuid']];
             if(array_key_exists('uid',$input)){
                $where = ['uuid'=>$input['uid']];
             }
             return $dataObject->allowField(true)->save($input,$where);
         },$inputData
       );
       return [];
     }

     public static function info(array $info=[]){
          $where = ['uuid'=>$info['uuid']];
          $sqlParams = self::sqlParams($info,['order'=>'create_time','limit'=>'1', 'field'=>true,'page'=>1]);
          $sql = self::where($where)->order($sqlParams['order'])->field($sqlParams['field'])->page($sqlParams['page'])->limit($sqlParams['limit']);
          $result = self::infoOfCache($sql);
          return $result;
     }

}
