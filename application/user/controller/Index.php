<?php
namespace app\user\controller;
use app\common\controller\Base;
use app\user\model\User as user;
/**
*表中的字段
* id , name , nick_name , phone_num , email_address , password , gender , create_time , note ,unique_identification
*/
class Index extends Base{

   /**
    * [changeRule 梗概程序角色信息的接口]
    * @return [type] [description]
    */
   public function changeRule(){
     self::sendResponse(
         'get:*',
         function($input,$publicTool){
             $notify = $this->loader->model('\\app\\user\\model\\AuthGroupRelation');
             $notify::setInputData($input);
             $response = $notify::monityRule();
             return $publicTool->msg('success','request success',1,$response);
         });

   }

    /**
     * 已经读取通知
    */
    public function notifyAlready(){
        self::sendResponse(
            'get:*',
            function($input,$publicTool){
                $notify = $this->loader->model('\\app\\user\\model\\AlertsMsg');
                $notify::setInputData($input);
                $response = $notify::alReading();
                return $publicTool->msg('success','request success',1,$response);
            });
    }


    public function mottoTpl(){
      self::sendResponse(
          'get:*',
          function($input,$publicTool){
              $response = db('motto_tpl')->select();
              return $publicTool->msg('success','request success',1,$response);
          });
    }

    /**
     * 工商信息
    */
    public function icInfo(){
        self::sendResponse(
            'get:*',
            function($input,$publicTool){
                $notify = $this->loader->model('\\app\\user\\model\\Icinfo');
                $notify::setInputData($input);
                $response = $notify::push();
                return $publicTool->msg('success','request success',1,[]);
            });
    }

    /**
     * [icInfoShow工商信息查询]
     * @return [type] [description]
     */
    public function icInfoShow(){
      self::sendResponse(
          'get:*',
          function($input,$publicTool){
              $notify = $this->loader->model('\\app\\user\\model\\Icinfo');
              $info = $notify::info($input);
              return $publicTool->msg('success','request success',1,$info);
          });
    }



    /**
     * [icInfoShow工商信息查询]
     * @return [type] [description]
     */
    public function icInfoMonity(){
      self::sendResponse(
          'get:*',
          function($input,$publicTool){
              $notify = $this->loader->model('\\app\\user\\model\\Icinfo');
              $notify->setInputData($input);
              $notify::monity();
              return $publicTool->msg('success','request success',1,[]);
          });
    }



    /**
    *退出登录
    */
    public function offline(){
         $msg = MsgTpl::msg('error','hello');
         $response = Response::create($msg,'json','0000',[],[]);
         $response->send();
         $uuid = input('get.uuid');
         $user = new \app\user\model\User;
         $user->offline($uuid);
    }

    /**
     * [info 查询会员的信息]
     * @return [type] [description]
     */
    public function info(){
        //查询当前的信息并返回接口的信息
        self::sendResponse(
        'get:*',
        function($input,$publicTool){
          $input['uuid'] = array_key_exists('uid',$input)? $input['uid']:$input['uuid'];
          $response = User::info($input);
          return $publicTool->msg('success','request success',1,$response);
        });
    }

    //检测权限
    public function checkAuth(){
        Loader::model('Auth');
        $response = $auth::check(input('get.uuid'));
        dump($response);
        exit;
    }
//输入消息接口
public function input() {
    self::sendResponse(
    'get:*',
    function($input,$publicTool){
    $alerts = $this->loader->model('\\app\\user\\model\\AlertsMsg');
    $alerts::setInputData($input);
    $response = $alerts::input();
    return $publicTool::msg('success','request success',1,$response);
  });

}
//查询信息接口
public function information(){
    self::sendResponse(
   'get:*',
    function($input,$publicTool){
    $alerts = $this->loader->model('\\app\\user\\model\\AlertsMsg');
    $response = $alerts::info($input);
    return $publicTool::msg('success','request success',1,$response);
  });
}
//用户登录接口
public function login(){
    self::sendResponse(
    'get:*',
    function($input,$publicTool){
    $login = $this->loader->model('\\app\\user\\model\\User');
    $login::setInputData($input);
    $response = $login::signIn();
    return $publicTool::msg('success','request success',1,$response);
  });
}
//用户注册接口
public function register(){
    self::sendResponse(
    'get:*',
    function($input,$publicTool){
      $register = $this->loader->model('\\app\\user\\model\\regist\\PhoneRegist');
      $register::setInputData($input);
    $response = $register::signUp($input);
    return $publicTool::msg('success','request success',1,$response);
  });
}


//用户微信注册接口
 public function wechatRegistered(){
     self::sendResponse(
     'get:*',
     function($input,$publicTool){
       $wechatRegistered = $this->loader->model('\\app\\user\\model\\regist\\WechatRegist');
       $wechatRegistered::setInputData($input);
     $response = $wechatRegistered::signIn($input);
     return $publicTool::msg('success','request success',1,$response);
   });
 }

/**
 * [sigin 会员签约教练]
 * @return [type] [description]
 */
public function signing(){
  self::sendResponse(
  'get:*',
  function($input,$publicTool){
    $wechatRegistered = $this->loader->model('\\app\\user\\model\\MemberSigningApplication');
    $input['applicant_id'] = $input['uuid'];
    $wechatRegistered::setInputData($input);
  $response = $wechatRegistered::memberSignApplication($input);
  return $publicTool::msg('success','request success',1,[]);
});
}


/**
 * [userInfoMonity 用户修改]
 * @return [type] [description]
 */
 public function userInfoMonity(){
   self::sendResponse(
     'get:*',
     function($input,$publicTool){
        $monity = $this->loader->model('\\app\\user\\model\\User');
        $monity::setInputData($input);
        $monity::monity();
        return $publicTool::msg('success','request success',1,[]);
     }
   );
 }

 /**
  * [setRole 设置用户的角色]
  */
 public function setRole(){
    self::sendResponse(
      'get:*',
      function($input,$publicTool){
        $role = $this->loader->model('\\app\\user\\model\\AuthGroupRelation');
        $role::setInputData($input);
        $role::setRole();
        return $publicTool::msg('success','request success',1,[]);
      }
    );
 }

 /**
  *测试的接口
  */
 public function test(){
    $gymInfo = $this->loader->model('\\app\\gym\\model\\GymInfo');
    $gymInfo->getAllGyms(['boss'=>'2db04240496c5f1bb6f759537f8d3b33','limit'=>false,'page'=>false]);
    $statistical = $this->loader->model('\\app\\reservation\\model\\ArrangeCourse');
    $response = $statistical->memberFitnessList(['gym_id'=>array_column($gymInfo->getAllGyms(['boss'=>'2db04240496c5f1bb6f759537f8d3b33','limit'=>false,'page'=>false]),'unique_id'),'week'=>'week']);
    //支持少选
    dump($response);
 }

 /**
  * [userManager 会员的主页接口]
  * @return [type] 返回会员自己的朋友圈信息
  */
 public function userManager(){
     self::sendResponse('get:*',function($input,$publicTool){
       //用户的主页信息
       $manager = $this->loader->model('\\app\\user\\model\\User');
       $response = $manager::managerInfo($input);
       return $publicTool::msg('success','request success',1,$response);
     });
 }

  /**
   * [SigningInquiry 会员签约查询]
   */
  public function memberSigningInquiry(){
      self::sendResponse('get:*',function($input,$publicTool){
       //用户的主页信息
       $manager = $this->loader->model('\\app\\user\\model\\MemberSigningApplication');
       $response = $manager::info($input);
       return $publicTool::msg('success','request success',1,$response);
     });
  }

  /**
   * [memberUnsignInquiry 会员解约查询]
   * @return [type] [description]
   */
  public function memberUnsignInquiry(){
    self::sendResponse('get:*',function($input,$publicTool){
     //用户的主页信息
     $manager = $this->loader->model('\\app\\gym\\model\\Unsigning');
     $response = $manager::info($input);
     return $publicTool::msg('success','request success',1,$response);
   });
  }

}

?>
