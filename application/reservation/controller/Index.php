<?php
namespace app\reservation\controller;
use app\common\controller\Base;
class Index extends Base{
  //教练接受约课接口
  public function coach(){
      self::sendResponse(
      'post:*',
      function($input,$publicTool){
      $coach = $this->loader->model('\\app\\reservation\\model\\ArrangeCourse');
      $coach::setInputData($input);
      $response = $coach::accept();
      return $publicTool::msg('success','request success',1,$response);
    });
  }
  //会员发起约课接口
 public function member(){
    self::sendResponse(
      'get:*',
      function($input,$publicTool){
        $member = $this->loader->model('\\app\\reservation\\model\\ReservationCourse');
        $input['member_id'] = $input['uuid'];
        $member::setInputData($input);
        $response = $member::initiate($input);
        return $publicTool::msg('success','request success',1,[]);
      });
 }

 /**
  * 获取约课的唯一标识
 */
 public function getCourseUniqid(){
     self::sendResponse(
         'get:*',
         function($input,$publicTool){
             $member = $this->loader->model('\\app\\reservation\\model\\ReservationCourse');
             $member::setInputData($input);
             $response = $member::genderQid();
             return $publicTool::msg('success','request success',1,$response);
         });
 }

    /**
     * 获取会员的约课信息
    */
     public function getCourseInfo(){
         self::sendResponse(
             'get:*',
             function($input,$publicTool){
                 $member = $this->loader->model('\\app\\reservation\\model\\ReservationCourse');
                 $response = $member::info($input);
                 return $publicTool::msg('success','request success',1,$response);
             });
     }

 /**
  * [aboutOfClass 查询教练的约课信息]
  * @return [type] [查询教练的约课信息]
  */
 public function aboutOfClass(){
   self::sendResponse(
     'get:*',
     function($input,$publicTool){
       $class = $this->loader->model('\\app\\reservation\\model\\ArrangeCourse');
       $aboutClass = $class::aboutClassInfo($input);
       $response['about_class']['first'] = 0; //今日最早约课的
       $response['about_class']['last'] = 0; //今日最晚约课的
       $response['about_class']['today'] = $aboutClass['list']; //约课的会员信息
       $response['about_class']['count'] = $aboutClass['count']; //约课的数量总计
       $response['about_class']['surplus'] = $aboutClass['surplus']; //约课的数量剩余
       if($aboutClass['first'] !== 0){
           //设置今日的最早时间
           $response['about_class']['first'] = $aboutClass['first'];
           $response['about_class']['last'] = $aboutClass['first'];
       }
       if($aboutClass['last'] !== 0){
           //设置今日的最晚时间
           $response['about_class']['last'] =$aboutClass['last'];
       }
       return $publicTool::msg('success','request success',1,$response);
     });
 }

 /**
  * [trainingCate 添加训练动作分类]
  * @return [type] [description]
  */
 public function addTrainingCate(){
   self::sendResponse(
     'post:*',
     function($input,$publicTool){
       $vent = $this->loader->model('\\app\\reservation\\model\\ExerciseVent');
       //添加教练的id
       $input['fitness_id'] = $input['uuid']; unset($input['uuid']);
       //添加训练动作分类
       $input['parent'] = $vent::$exerciseVentCateId;
       $input['amount'] = 0; //表示没有计量单位
       $vent::setInputData($input);
       $response = $vent::push();
       return $publicTool::msg('success','request success',1,$response);
     });
 }


 /**
  * [trainingCate 添加训练动作]
  * @return [type] [description]
  */
 public function addTraining(){
   self::sendResponse(
     'post:*',
     function($input,$publicTool){
       $vent = $this->loader->model('\\app\\reservation\\model\\ExerciseVent');
       $input['fitness_id'] = $input['uuid']; unset($input['uuid']);
       $vent::setInputData($input);
       $response = $vent::push();
       return $publicTool::msg('success','request success',1,$response);
     });
 }

 /**
  * [training 查询训练动作分类]
  * @return [type] [description]
  */
 public function trainingCateInfo(){
   self::sendResponse(
     'post:*',
     function($input,$publicTool){
       $vent = $this->loader->model('\\app\\reservation\\model\\ExerciseVent');
       $input['parent'] = 0;
       $vent::setInputData($input);
       $response = $vent::info($input);
       return $publicTool::msg('success','request success',1,$response);
     });
 }


     /**
      * [training 查询训练动作]
      * @return [type] [description]
      */
     public function trainingInfo(){
       self::sendResponse(
         'post:*',
         function($input,$publicTool){
           $vent = $this->loader->model('\\app\\reservation\\model\\ExerciseVent');
           $vent::setInputData($input);
           $response = $vent::info($input);
           return $publicTool::msg('success','request success',1,$response);
         });
     }

     //添加课程动态
     public function courseDynamicPush(){
         self::sendResponse(
             'post:*',
             function($input,$publicTool){
                 $vent = $this->loader->model('\\app\\reservation\\model\\GymCourseDynamic');
                 $vent::setInputData($input);
                 $response = $vent::initale($input);
                 return $publicTool::msg('success','request success',1,$response);
             });
     }

     /**
      * [courseHistory 课程历史]
      * @return [type] [description]
      */
     public function courseHistory(){
       self::sendResponse(
           'get:*',
           function($input,$publicTool){
               $course = $this->loader->model('\\app\\reservation\\model\\ArrangeCourse');
               $response = $course::info($input);
               return $publicTool::msg('success','request success',1,$response);
           });
     }
}
