<?php
namespace app\reservation\model;
use app\common\model\Base;
use app\common\model\Observice;
use think\Validate;
/**
 *课程动态表，发布课程动态的内容
 * `id` int(15) unsigned NOT NULL AUTO_INCREMENT COMMENT '课程动态表,连接圈子功能',
 *`course_content_id` int(15) unsigned NOT NULL COMMENT '课程动态的id',
 *`coach_reviews` varchar(255) NOT NULL COMMENT '教练的点评',
 *`coach_id` char(64) NOT NULL COMMENT '教练的id',
 *`gym_id` char(64) NOT NULL COMMENT '健身房的id',
 *`member_id` char(64) NOT NULL COMMENT '会员的id',
 *`create_time` int(64) NOT NULL COMMENT '创建时间',
 */
class GymCourseDynamic extends Base{

  protected $table = 'gym_course_dynamic';

  protected static  $autoRefreshCache=true; //增加，修改，删除，自动更新缓存

  protected static  $autoPushCreateTime = true; //自动添加时间

  protected static  $autoPushUpdateTime = true; //自动添加修改时间

  /**
   * [initale 新增或者修改内容]
   * @return [bool] [真或者假]
   */
  public static function initale(){
      $id = self::getInputData('course_content_id');
      if(self::where(['course_content_id'=>$id])->count() > 0){
        return  self::monity();
      }else{
        return  self::push();
      }
  }

  protected  static function push(){
    $inputData = self::getInputData();
    $validate = new Validate(
      [
        'coach_id'=>'require', //教练的id不能为空
        'coach_reviews'=>'require', //教练的点评不能为空
        'course_content_id'=>'require',//课程内容的id
        'gym_id'=>'require', //健身房的id
        'member_id'=>'require' //会员的id
      ],
      [
        'coach_id.require'=>'教练的编号不能为空！', //coach_id
        'coach_reviews.require'=>'教练的评语不能为空！', //coach_reviews
        'gym_id.require'=>'健身房的编号不能为空！',
        'member_id.require'=>'会员的编号不能为空！',
      ]
    );
    //课程动态验证
    if(!$validate->check($inputData)){

        abort(-3,$validate->getError());

    }

    $dataObject = new self();
    $save = $dataObject->data($inputData)->allowField(true)->isUpdate(false)->save();
    if(!$save){
        abort('保存失败请重试！');
    }
    $lastId = $dataObject->getLastInsID();
    foreach(json_decode($inputData['course_content_img'],true) as $val){
      db('course_dynamic_img')->insert([
          'course_dynamic_id'=>$lastId,
          'img_url'=>$val,
          'create_time'=>time(),
          'last_update_time'=>time()
      ]);
    }
    Observice::addObserivce('\\app\\circle\\model\\Circle', ['index_id'=>$lastId,'type'=>1,'fitness_id'=>$inputData['coach_id'],'gym_id'=>$inputData['gym_id'],'uuid'=>$inputData['member_id']]);
    Observice::notify();
    \app\gym\model\MemberSigning::setInputData(
      [
        'fitness_id'=>$inputData['coach_id'],
        'member_id'=>$inputData['member_id'],
        'gym_id'=>$inputData['gym_id'],
        'option'=>2,
        'hours'=>1
      ]
    );
    //减少课程的数量
    \app\gym\model\MemberSigning::memberHoursOption();
    return [];
  }

  /**
   * [monity 课程动态信息修改]
   * @return [bool] [真|假]
   */
  protected static function monity(){
    $inputData = self::getInputData();
    $validate = new Validate(
      [
        'coach_id'=>'require', //教练的id不能为空
        'coach_reviews'=>'require', //教练的点评不能为空
        'course_content_id'=>'require',//课程内容的id
        'gym_id'=>'require', //健身房的id
        'member_id'=>'require' //会员的id
      ],
      [
        'coach_id.require'=>'教练的编号不能为空！', //coach_id
        'coach_reviews.require'=>'教练的评语不能为空！', //coach_reviews
        'gym_id.require'=>'健身房的编号不能为空！',
        'member_id.require'=>'会员的编号不能为空！',
      ]
    );
    $dataObject = new self();
    $where = ['course_content_id'=>$inputData['course_content_id']];
    $inputData['last_update_time'] = time();
    $lastId = self::where($where)->field(['id'])->find()->toArray()['id'];
    foreach(json_decode($inputData['course_content_img'],true) as $val){
     $insert = db('course_dynamic_img')->insert([
          'course_dynamic_id'=>$lastId,
          'img_url'=>$val,
          'create_time'=>time(),
          'last_update_time'=>time()
      ]);
    }

    $save = $dataObject->allowField(true)->isUpdate(true)->save($inputData,$where);
    if(!$save){
        abort(-04,'操作失败请重试！');
    }
    return [];
  }

}
