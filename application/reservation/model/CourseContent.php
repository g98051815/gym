<?php
namespace app\reservation\model;
use app\common\model\Base;
/**
*课程详情的内容
*/
class CourseContent extends Base{

  protected $table = 'gym_course_content';

  /**
  *查询课程内容的详情
  */
  public static function info( $info = [] ){
      $where = ['course_id'=>$info['course_id']];
      $sqlQuery = self::sqlParams($info,[
        'field'=>[
          'ev.title',
          'ev.amount',
          'cc.length',
          'cc.repeat'
        ],
        'page'=>1,
      ]);
      $sqlResult = function($where,$sqlQuery){
          $result = self::where($where)->alias('cc')->field($sqlQuery['field']);
          //gym_exercise_vent
          $result->join('__EXERCISE_VENT__ ev','cc.exercise_vent_id = ev.id','INNER');
          return $result;
      };
      $count = $sqlResult($where,$sqlQuery)->count();
      $result = $count > 0 ? self::infoOfCache($sqlResult($where,$sqlQuery)->limit($count)) : [];
      return $result;
  }
}
