<?php
namespace app\reservation\model;
use app\common\model\Base;
/**
 * [ExerciseVent 训练动作的分类]
 */
class ExerciseVent extends Base{

  protected static  $cacheTag='base'; //缓存的名称,关闭使用缓存直接使用false

  protected static  $autoRefreshCache=true; //增加，修改，删除，自动更新缓存

  protected static  $autoPushCreateTime = true; //自动添加时间

  protected static  $autoPushUpdateTime = true; //自动添加修改时间
  //训练动作分类的id
  public static  $exerciseVentCateId = 0;

  public static $returnExercisVent=0;

  /**
   * [push 训练动作的添加]
   * @return [type] [description]
   */
  public static function push(){
      self::couSave(
        [
          ['title','require|cate_repeat','标题:不能为空！|已经存在！'],
          ['parent','require','上级标题不能为空！'],
          ['amount','require','计量单位不能为空！'],
        ],
        function($input,$self){
            $allowField = ['title','parent','amount','create_time','fitness_id','last_update_time'];
            $dataObject = new $self();
            $result = $dataObject->data($input)->allowField($allowField)->isUpdate(false)->save();
            self::$returnExercisVent = $dataObject->getLastInsId();
            return $result;
        }
      );
      return ['id'=>self::$returnExercisVent];
  }


  protected static function validateExtend($validate){
        $validate->extend(
          [
            'cate_repeat'=>function($val){
                $return  = true;
                if(self::where(['fitness_id'=>self::getInputData('fitness_id'),'title'=>self::getInputData('title')])->count() > 0){
                    $return =  false;
                }
                return $return;
            }
          ]
        );
  }

  /**
   * [info 训练动作查询接口]
   * @return [type] [description]
   */
  public static function info($info=[]){
      $where = ['fitness_id'=>['in',[$info['uuid'],"default"]],'parent'=>$info['parent']];
      $sqlQuery = self::sqlParams($info,['field'=>true,'order'=>'create_time desc']);
      $sqlResult= function($where){
        return self::where($where);
      };
      $response = self::infoOfCache($sqlResult($where)->order($sqlQuery['order'])->field($sqlQuery['field']));
      return $response;
  }

}
