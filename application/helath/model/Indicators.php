<?php
namespace app\helath\model;
use app\common\model\Base;
use think\exception\HttpException;
use think\Validate;
/**
*指标管理
*
*/
class Indicators extends Base{
    protected $table = 'gym_indicators';

    protected $statusCode = []; //状态值
    /**
    *数据库字段名称
    *uuid 用户的uuid
    *bust 胸围指数
    *waist 腰围
    *hip_dimension 臀为
    *weight 体重
    *bmi bmi指数
    *body_fat_rate 体内脂肪率
    *uuid=1&bust=23&waist=25&weight=140&bmi=90&body_fat_rate=30&hip_dimension=20
    */
    public static function push(array $data){
        $validate = new Validate(
            [
                'uuid'=>'require',
                'bust'=>'require|number',
                'waist'=>'require|number',
                'weight'=>'require|number',
                'hip_dimension'=>'require|number',
                'bmi'=>'require|number',
		'uid'=>'require',
                'body_fat_rate'=>'require|number'
            ],
            [
                'uuid.require'=>'uuid必须！',
                'bust.require'=>'胸围必须',
                'waist.require'=>'腰围必须',
                'hip_dimension.require'=>'臀为必须',
                'weight.require'=>'体重必须',
                'bmi.require'=>'bim指数必须',
                'body_fat_rate.require'=>'体脂率必须',
                'bust.number'=>'胸围格式不正确',
                'waist.number'=>'腰围格式不正确',
                'hip_dimension.number'=>'臀为格式不正确',
                'weight.number'=>'体重格式不正确',
		'uid.require'=>'会员的编号不能为空',
                'bmi.number'=>'bim指数格式不正确',
                'body_fat_rate.number'=>'体脂率格式不正确'
            ]
        );
	
        if(!$validate->check($data)){
            //添加检查失败
            throw new HttpException(-0001,$validate->getError());
        }
        $allowField = true;
        $dataObject = new self();
        $data['create_time'] = time();
	$data['uuid'] = $data['uid'];
        $add = $dataObject->data($data)->allowField($allowField)->isUpdate(false)->save();
        if(!$add){
            throw new HttpException(-0001,'添加失败，请重试！');
        }
        return ['id'=>$dataObject->getLastInsID()];
    }

    //指标修改
    public function motify(array $data,$delete=false){
        $data = addslashes($data);
        $allowField = [];
        if(!$delete){
              $validate = new Validate(
            [
                'uuid'=>'require',
            ],
            [
                'uuid.require'=>'uuid必须！',
            ]
         );
            if($validate->check($data)){
                //修改报错
                throw new HttpException(-0002,$validate->getError());
            }
        }
        $where = ['uuid'=>$data['uuid']];
        $save = $this->isUpdate(true)->save([$data,$where]);
        if(!$save){
            throw new HttpException(-0002,'修改失败请重试！');
        }
        return $data;
    }

    /**
    *指标列表假删除
    */
    public function falseDelete(array $data = []){
        $data['status'] =0;
        $this->motify($data);
        return true;
    }

    /**
    *健康指标查询直接是通过用户的id返回健康指标和时间戳的一种查询类型
    *需要分页内容,返回列表的数组形式
    */
    public static function info(array $info,$limit=10){
        //模拟几种查询情况
        $validate = new Validate(
           ['uuid'=>'require']
          ,['uuid.require'=>'用户的编号必须填写']
        );
        if(!$validate->check($info)){
          abort(-0020,$validate->getError());
        }
        $where['uuid']=$info['uuid'];
        if(array_key_exists('page', $info)){
          $page=$info['page'];unset($info['page']);
        }else{
          $page = 1;
        }

        $result = self::where($where)->limit($limit)->order('create_time desc')->page($page)->select();
        $response =  collection($result)->toArray();
        return $response;
    }


}
