<?php
namespace app\helath\controller;
use app\common\controller\Base;

class Index extends Base{
  protected static $indicators; //健康信息管理模型
  protected static $publicTool; //公共工具库模型

  public function input(){
    self::sendResponse(
    'get:*',
    function($input,$publicTool){
    $indicators = $this->loader->model('\\app\\helath\\model\\Indicators');
    $indicators::setInputData($input);
    $response = $indicators::push($input);
    return $publicTool::msg('success','request success',1,$response);
  });
  }

public function info(){
   self::sendResponse(
     'get:*',
     function($input,$publicTool){
       $info = $this->loader->model('\\app\\helath\\model\\Indicators');
       $info::setInputData($input);
       $response = $info::info($input);
       return $publicTool::msg('success','request success',1,$response);
     });
   }
}
