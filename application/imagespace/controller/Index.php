<?php
namespace app\imagespace\controller;
use app\common\controller\Base;
use app\imagespace\model\Image as img;
class Index extends Base{
//添加图片
  public function push(){
    self::sendResponse(
      'get:*',
      function($input,$publicTool){
          img::setInputData($input);
          img::push();
          return $publicTool::msg('success','request success',1,[]);
      }
    );
  }
//修改图片
  public function modify(){
    self::sendResponse(
      'get:*',
      function($input,$publicTool){
          img::setInputData($input);
          img::modify();
          return $publicTool::msg('success','request success',1,[]);
      }
    );
  }


  /**
   * [delete 目前为假的删除]
   * @return [appResponse] [返回删除成功，或者删除失败！]
   */
  public function delete(){
    self::sendResponse(
      'get:*',
      function($input,$publicTool){
          $input['status']=0;
          img::setInputData($input);
          img::modify();
          return $publicTool::msg('success','request success',1,[]);
      }
    );
  }


  public function info(){
    self::sendResponse(
      'get:*',
      function($input,$publicTool){
          $response = img::info($input);
          return $publicTool::msg('success','request success',1,$response);
      }
    );
  }
}
