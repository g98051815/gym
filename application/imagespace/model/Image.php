<?php
namespace app\imagespace\model;
use app\common\model\Base;
use app\publictool\controller\MsgTpl;
/**
 * [Image 相册目前是教练的相册]
 */
class Image extends Base{
	//设置数据库名
	 protected $table = "gym_photo_album";
	 protected static  $cacheTag='gym_photo_album'; //缓存的名称,关闭使用缓存直接使用false
	 protected static  $autoPushCreateTime = true; //自动添加时间
	 protected static  $autoPushUpdateTime = true; //自动添加修改时间
	//  `id` int(15) unsigned NOT NULL COMMENT '主键',
  //  `category_id` int(15) unsigned NOT NULL DEFAULT '1' COMMENT '分类的id',
  //  `img_url` varchar(255) NOT NULL COMMENT '图片的地址',
  //  `uuid` char(64) NOT NULL COMMENT '用户的唯一标识',
  //  `title` varchar(255) NOT NULL COMMENT '图片的名字'
  public static function push(){

			self::couSave(
				[
					['uuid','require','教练的唯一标识不能为空！'],
					['img_url','require','图片地址不能为空']
				],
				function($inputData,$self){
						$dataObject = new $self();
						return $dataObject->data($inputData)->allowField(true)->isUpdate(false)->save();
				}
			);

  }

	/**
	 * [modify description]
	 * @return [type] [description]
	 */
	//修改图片
	public static function modify(){
		self::couSave(
			[
				['id','require','图片的编号不能为空!']
			],
			function ($inputData,$self){
				$dataObject = new $self();
				$where = ['id'=>$inputData['id']];
				$allowField = ['img_url','title','last_update_time','status'];
				return $dataObject->allowField($allowField)->save($inputData,$where);
			}
		);
	}


	/**
	 * [info description]
	 * @param  array  $info [description]
	 * @return [type]       [description]
	 */
  public static function info(array $info=[]){
				$sql = function($info){
				if(array_key_exists("uuid",$info)){
						$where = ['uuid'=>$info['uuid']];
				}
				if(array_key_exists("uid",$info)){
					$where = ['uuid'=>$info['uid']];
				}
				$where['status'] = 1;
				$params = self::sqlParams($info,['order'=>'create_time desc','field'=>true,'limit'=>10,'page'=>1]);
				$result = self::where($where)->field($params['field'])->order($params['order'])->limit($params['limit'])->page($params['page']);
				return [$result,$params];
			};
			list($result,$params) = $sql($info);
			$count = $sql($info)[0]->count();
			$response = self::infoOfCache($result);
			if(empty($response)){
					$response = [
						['img_url'=>'2016-08-28_57c2c087654e8.jpg'],
						['img_url'=>'2016-08-28_57c2c087654e8.jpg'],
						['img_url'=>'2016-08-28_57c2c087654e8.jpg'],
						['img_url'=>'2016-08-28_57c2c087654e8.jpg']
					];
			}
			return MsgTpl::createListTpl($response,$count,$params['limit'],$params['page']);
  }
}
