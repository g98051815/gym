<?php
namespace app\wechat\controller;
use app\common\controller\Base;

class Index extends Base{

	public function getToken(){
		self::sendResponse('get:*',function($input,$public){
			$protected = 'http://';
      $sslProtected = 'https://';
      $host = $_SERVER['HTTP_HOST'];
      $path = '/gym/public/images/';
      $filename = 'xcxqrcode.jpeg';
			$xcximagepath = PUBLIC_PATH.'images/'.$filename;
			if(file_exists($xcximagepath)){
				$response = file_get_contents($xcximagepath);
				header('Content-Type:application/json;');
				echo json_encode(['src'=>'data:image/jpeg;base64,'.base64_encode($response),
				'https_src'=>$sslProtected.$host.$path.$filename,'http_src'=>$protected.$host.$path.$filename],JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
				exit;
			}
			$token = $this->loader->model('\\app\\wechat\\model\\Token');
			$token::setGrantType($input['grant_type']);
			$token::setAppid($input['appid']);
			$token::setSecret($input['secret']);
			$tokenRequest = json_decode($token::run(),true);
			$gewx = $this->loader->model('\\app\\wechat\\model\\GetWxaCode');
			$gewx::setToken($tokenRequest['access_token']);
			$gewx::setInputData($input);
			$response = $gewx::run();
			//写入小程序的内容
			file_put_contents($xcximagepath,$response);
			echo json_encode(['src'=>'data:image/jpeg;base64,'.base64_encode($response),
			'https_src'=>$sslProtected.$host.$path.$filename,'http_src'=>$protected.$host.$path.$filename],JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		});
	}
}
