<?php
namespace app\wechat\model;

class Token{
	
	protected static $getCreateQrCodeTokenUri = 'https://api.weixin.qq.com/cgi-bin/token';
	
	protected static $grantType;
	
	protected static $appid;
	
	protected static $secret;
	
	/**
	*设置传入的数据
	*/
	public static function setGrantType( $val ){
		
		self::$grantType = $val;		

	}
	

	public static function setAppid( $val ){
		
		self::$appid = $val;	
	
	}

	public static function setSecret( $val ){
		
		self::$secret = $val;
	
	}

	public static function getGrantType(){
		
		return self::$grantType;	
	
	}

	public static function getAppid(){
	
		return self::$appid;
	
	}

	public static function getSecret(){
	
		return self::$secret;	
	
	}

	public static function run(){
		
		$curl = curl_init();
		$uriParams = http_build_query(
			[
				'grant_type'=>self::getGrantType(),
				'appid'=>self::getAppid(),
				'secret'=>self::getSecret(),
			]
		);
		$setOptArray = [
			CURLOPT_URL=>self::$getCreateQrCodeTokenUri.'?'.$uriParams,
			CURLOPT_RETURNTRANSFER=>true,
			CURLOPT_SSL_VERIFYPEER=>false,
			CURLOPT_SSL_VERIFYHOST=>false,
		];
		curl_setopt_array($curl,$setOptArray);
		$exec = curl_exec($curl);
		curl_close($curl);
		return $exec;
	
	}
	
	
}
