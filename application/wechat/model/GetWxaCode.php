<?php
namespace app\wechat\model;

class GetWxaCode{
	
	protected static $url='https://api.weixin.qq.com/wxa/getwxacode';	

	protected static $token;
	
	protected static $inputData;

	public  static function setToken( $token ){

		
		self::$token = $token;

	}
	
	public static function setInputData( $val ){
		
		self::$inputData = $val;
		
	}	

	public static function run(){
		
		$curl = curl_init();
		$uriOpt = [
			'access_token'=>self::$token,
		];
		$headers = array("Content-type: application/json;charset=UTF-8","Accept: application/json","Cache-Control: no-cache", "Pragma: no-cache");
		$curlSetOpt = [
			CURLOPT_URL=>self::$url.'?'.http_build_query($uriOpt),
			CURLOPT_RETURNTRANSFER=>true,
			CURLOPT_SSL_VERIFYPEER=>false,
			CURLOPT_SSL_VERIFYHOST=>false,
			CURLOPT_POST=>true,
			CURLOPT_POSTFIELDS=>self::$inputData['params'],
			CURLOPT_HTTPHEADER=>$headers,
		];
		
		curl_setopt_array($curl,$curlSetOpt);
		$exec = curl_exec($curl);
		curl_close($curl);
		return $exec;
	}
}
