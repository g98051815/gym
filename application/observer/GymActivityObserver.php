<?php
namespace app\observer;
use app\common\model\Observer;
/**
 * [GymActivityObserver 健身房活动观察者]
 * 其中需要通知三部分的内容
 * 1.通知会员自己成功报名参加了某某健身房的活动，以及活动的时间和地点
 * 2.通知健身房的老板当前已经有人参加健身房的活动了。
 */
class GymActivityObserver implements Observer{
  /**通知观察者中做的什么样子的事情！*/
    
}
