<?php
namespace app\admin\model;
use app\common\model\Base;
class Statistics extends Base{


    /**
     * [push 添加统计数据]
     * @return [type] [description]
     */

    /**
     * [info 查询所有内容每日的活跃量]
     * @return [type] [description]
     */
    public static function info(){

        return self::infoOfCache(self::whereTime('create_time','today')->select());

    }

    //获取健身房的数量
    public static function gymNum(){
        //统计健身房的数量
        $result['gym_count'] = db('gym_info')->where(true)->count();
        //统计教练的数量
        $result['fitness_count'] = db('sign_gym')->where(true)->count();
        //统计会员的数量
        $result['member_count'] = db('signing_fitness_instructor')->where(true)->count();
        //总用户量
        $result['user_count'] = db('user_info')->where(true)->count();
        //课程动态总量
        $result['arrcource_count'] = db('course_dynamic')->where(true)->count();

        return $result;
    }


}
