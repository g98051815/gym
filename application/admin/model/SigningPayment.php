<?php
namespace app\admin\model;
use app\common\model\Base;
use app\publictool\controller\MsgTpl;
use think\Loader;
class SigningPayment extends Base{

  protected $table = 'gym_contribution_record';

  /**
   * [push 添加老板的缴费记录]
   * @return [type] [description]
   */
  public static function push(){
      //获取添加的记录数量
      $input = self::getInputData();
      $validate = Loader::validate('SingningPayment');
      $validate->check($input)?:abort(-15,$validate->getError());
      $input['create_time'] = time();
      $dataObj = new self();
      return $dataObj->data($input)->allowField(true)->isUpdate(false)->save();
  }

  /**
   * [info 缴费的列表信息]
   * @return [type] [description]
   */
  public static function info( $info = [] ){
      $where = true;

      if(array_key_exists('gym_id',$info)){
          $where = ['gym_id'=>$info['gym_id']];
      }

      $sqlQuery = self::sqlParams($info,[
        'field'=>true,
        'limit'=>10,
        'page'=>1,
        'order'=>'create_time desc'
      ]);
      $sqlResult = function($where){
          return self::where($where);
      };
      $count = $sqlResult($where)->count();
      $result = $count > 0? collection($sqlResult($where)->field($sqlQuery['field'])->limit($sqlQuery['limit'])->page($sqlQuery['page'])->select())->toArray() : [];
      return MsgTpl::createListTpl($result,$count,$sqlQuery['limit'],$sqlQuery['page']);
  }

  //查询列表的内容
  public static function infoArrayCondition( $info = [] ){



  }


  public static function monity(){


  }


}
