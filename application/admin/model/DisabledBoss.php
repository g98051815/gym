<?php
namespace app\admin\model;
use app\common\model\Base;

class DisabledBoss extends Base{

  protected $table = 'gym_disable';

  /**
   * [push 添加数据的接口]
   * @return [type] [description]
   */
  public static function initialide(){
      $input = self::getInputData();
      $dataObj = new self();
      $result = false;
      if(self::where($where = ['uuid'=>$input['uuid']])->count() > 0){
          $result = $dataObj->allowField(true)->save($input,$where);
      }else{
          $result = $dataObj->allowField(true)->data($input)->save();
      }
      return $result?[]:abort(-15,"操作失败");
  }


}
