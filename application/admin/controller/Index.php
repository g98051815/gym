<?php
namespace app\admin\controller;
use app\common\controller\Base;
use app\gym\model\GymInfo;
class Index extends Base{

  //查询所有的健身房和健身房的过期时间
  public function allGymInfo(){
    self::sendResponse(
      'get:*',
      function($input,$publicTool){
        $obj = $this->loader->model('\\app\\gym\\model\\GymInfo');
        $response = $obj::gymAllExport($input);
        return $publicTool::msg('success','request success',1,$response);
    });
  }

  //增加记录
  public function addLine(){
    self::sendResponse(
      'get:*',
      function($input,$publicTool){
        $obj = $this->loader->model('\\app\\admin\\model\\SigningPayment');
        $obj::setInputData($input);
        $response = $obj::push($input);
        return $publicTool::msg('success','request success',1,[]);
    });
  }

  //记录的列表
  public function lineInfo(){
    self::sendResponse(
      'get:*',
      function($input,$publicTool){
        $obj = $this->loader->model('\\app\\admin\\model\\SigningPayment');
        $response = $obj::info($input);
        return $publicTool::msg('success','request success',1,$response);
    });
  }

  /**
   * [Manager 总后台的主页]
   */
  public function Manager(){
    self::sendResponse(
      'get:*',
      function($input,$public){
        $obj = $this->loader->model('\\app\\admin\\model\\Statistics');
        $response = $obj::gymNum();
        return $public::msg('success','request success',1,$response);
      });
  }

  /**
   * [pushDisabled 给老板推送内容]
   * @return [type] [description]
   */
  public function pushDisabled(){
    self::sendResponse(
      'get:*',
      function($input,$public){
        $obj = $this->loader->model('\\app\\admin\\model\\DisabledBoss');
        foreach(array_column(json_decode($input["boss_value"],true),'boss') as $val){
          $setInput['uuid'] = $val;
          $setInput['disabled'] = $input['disabled'];
          $setInput['show_value'] = $input['show_value'];
          $obj->setInputData($setInput);
          $response = $obj::initialide();
        }
        return $public::msg('success','request success',1,$response);
      });
  }

}
