<?php
namespace app\admin\validate;
use think\Validate;

class SingningPayment extends Validate{

  public $input = [
    'gym_id'=>'require',
    'gym_boss_id'=>'require',
    'total'=>'require',
    'be_verdue'=>'require'
  ];

  protected $message = [
    'gym_id.require'=>'健身房的编号不能为空！',
    'gym_boss_id.require'=>'健身房老板的编号不能为空！',
    'total.require'=>'金额不能为空',
    'be_verdue.require'=>'到期时间不能为空!',
  ];

}
