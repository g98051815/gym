<?php
namespace app\questionnaire\controller;
use app\questionnaire\model;
use think\Response;
use think\Loader;
use think\exception\HttpException;
use app\common\controller\Base;
class Index extends Base{
  //问题分类接口
 public function issue(){
   $input = input('get.');
   $QuestionType = Loader::model('\\app\\questionnaire\\model\\QuestionType');
   $publicTool   = Loader::controller('\\app\\publictool\\controller\\PublicTool');
   try{
     $response = $QuestionType::push($input);
   }catch(HttpException $e){
     $msg = $publicTool::msg('error',$e->getMessage(),$e->getStatusCode());
     Response::create($msg,'json')->send();
   }
 }
 //问题判断类型接口
 public function issueType(){
   $input = input('get.');
   $QuestionType = Loader::model('\\app\\questionnaire\\model\\QuestionType');
   $publicTool   = Loader::controller('\\app\\publictool\\controller\\PublicTool');
   try{
     $response = $QuestionType::dataSum($input);
   }catch(HttpException $e){
     $msg = $publicTool::msg('error',$e->getMessage(),$e->getStatusCode());
     Response::create($msg,'json')->send();
   }
 }
 //添加答案接口
 public function addResult(){
   $input = input('get.');
   $Answer = Loader::model('\\app\\questionnaire\\model\\Answer');
   $publicTool = Loader::controller('\\app\\publictool\\controller\\PublicTool');
   try{
     $response = $Answer::push($input);
   }catch(HttpException $e){
     $msg = $publicTool::msg('error',$e->getMessage(),$e->getStatusCode());
     Response::create($msg,'json')->send();
   }
 }

 /**
 *回答问题
 */
 public function answerQuestions(){
   self::sendResponse('GET:*',function($input,$public){
       $answerQuestions = $this->loader->model('\\app\\questionnaire\\model\\QuestionnaireAnswer');
       $answerQuestions->setInputData($input);
       $response = $answerQuestions::push();
       return $public::msg('success','request success',1,$response);
   });
 }

 /**
 *回答问题的查询列表
 */
 public function answerQuestionsInfo(){
   self::sendResponse('GET:*',function($input,$public){
       $answerQuestions = $this->loader->model('\\app\\questionnaire\\model\\QuestionnaireAnswer');
       $response = $answerQuestions::info($input);
       return $public::msg('success','request success',1,$response);
   });
 }

 /**
  * [typeInfo 问题的类型查询]
  * 接口参数类型
  * gym_id 健身房的编号 （使用当前用户的健身房的编号进行查询）
  * @return [type] [description]
  */
 public function typeInfo(){
    self::sendResponse('GET:*',function($input,$public){
        $questionType = $this->loader->model('\\app\\questionnaire\\model\\QuestionType');
        $response = $questionType::info($input);
        return $public::msg('success','request success',1,$response);
    });

 }


 /**
  *问题查询的接口
  */
 public function questionInfo(){
   self::sendResponse('GET:*',function($input,$public){
       $questionType = $this->loader->model('\\app\\questionnaire\\model\\Question');
       $response = $questionType::info($input);
       return $public::msg('success','request success',1,$response);
   });
 }

}
