<?php
namespace app\questionnaire\model;
use think\Validate;
use think\Loader;
use app\common\model\Base;
/**
 *`id` int(15) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增长id主键',
 *`name` varchar(100) NOT NULL COMMENT '分类名称',
 *`create_time` int(15) unsigned NOT NULL COMMENT '创建的时间',
 */


/**
 * [Question 问题分类模型]
 */
class QuestionType extends Base{
  protected $table = 'gym_question_types'; //问题分类表
  protected static $inputData = [];
  protected static $cacheTag='question_types';
  protected static $publicTool = null;
  protected static $defaultGymId = 'default_gym';
  /**
   * [init 初始化的字段]
   * @return [type] [初始化的字段内容]
   */
  public static function init(){
    parent::init();
    //调用公共工具类
    self::$publicTool = Loader::controller('\\app\\publictool\\controller\\PublicTool');
    self::$defaultGymId = config('signing.default_gym_id');
  }

    /**
     * [push 增加问题的分类内容]
     * @return [bool] [返回布尔类型的值，非真即假]
     */
    public static function push(){
        $inputData = self::getInputData();
        $allowField = ['title','gym_id','create_time'];
        $validate = new Validate(
          [
            'title'=>'require',
            'gym_id'=>'require',
          ],
          [
            'title.require'=>'必须填写分类的名称',
            'gym_id.require'=>'用户编号格式的内容是不能为空的！'
          ]
        );
        if($validate->check($inputData)){
            abort(-0003,$validate->getError());
        }
        $title = $inputData['title'];
        if(self::typeExists($title)){
            abort(-0005,'类型的标题已经存在请换一个标题试一试！');
        }
        $inputData['create_time'] = time();
        $dataObject = new self();
        $save = $dataObject->data($inputData)->allowField($allowField)->save();
        if(!$save){
            abort(0007,'分类添加失败，请重试！');
        }
        return true;
    }


    /**
     * [info description]
     * @param  array  $condition [查询的条件]
     * @return [array]            [返回查询的类型数据]
     */
    public static function info(array $condition=[]){
        $validate = new Validate(
          [
            'gym_id'=>'require',
          ],
          [
            'gym_id.require'=>'健身房的id不能是空的！',
          ]
        );
        if(!$validate->check($condition)){
            abort(-0015,$validate->getError());
        }
        $where = ['gym_id'=>$condition['gym_id']];
        $sqlQuery = self::sqlParams($condition,
        [
          'field'=>['title','id'],
          'order'=>'create_time desc'
        ]);
        $sqlResult = function($where,$sqlQuery){
             return self::where($where)->field($sqlQuery['field']);
        };

        if($sqlResult($where,$sqlQuery)->count() < 1){
           //使用默认的健身房使用的内容
            $where = ['gym_id'=>self::$defaultGymId];
        }
        $count = $sqlResult($where,$sqlQuery)->count();
        $result = $sqlResult($where,$sqlQuery)->order($sqlQuery['order']);
        return self::infoOfCache($result);
    }


    /**
     * [typeExists 判断类型是否存在，以名称进行判断]
     * @param string $title 分类的标题名称
     * @return [type] [返回布尔类型的数据 true|false]
     */
    public static function typeExists($title=null){
       if(self::dataSum($title)>0){
          return true;
       }
       return false;
    }


    /**
     * [dataSum description]
     * @return [type] [description]
     */
    public static function dataSum($title=null,$uuid=''){
        if(empty($title)){
          abort(-0004,'必须填写抬头的内容数据！');
        }

        if(empty($uuid)){
          abort(-0010,'永不的编号也是不能为空的！');
        }

        if(is_string($title)){
          abort(-0005,'搜索的格式不正确！');
        }

        if(is_string($uuid)){
          abort(-0011,'用户的编号格式不正确！');
        }
        $where = ['title'=>$title,'uuid'=>$uuid];
        return self::where($where)->count();
    }




}
