<?php
namespace app\questionnaire\model;
use think\Model;
// CREATE TABLE `gym_answer_supplement` (
//   `id` int(15) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增长id主键',
//   `supplement_key` varchar(100) NOT NULL COMMENT '补充的键名',
//   `create_time` varchar(100) NOT NULL COMMENT '创建的时间',
//   `answer_id` int(15) unsigned NOT NULL COMMENT '答案的id',
//   PRIMARY KEY (`id`),
//   KEY `gym_question_supplement_id_IDX` (`id`) USING BTREE,
//   KEY `gym_question_supplement_answer_id_IDX` (`answer_id`) USING BTREE
// ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='问卷补充表'
// class AnswerSupplement extends Model{
//   protected $table = '';
//   protected static $publicTool = null;
//   protected static $inputData = [];
class AnswerSupplement extends Model{
  protected $table = 'gym_answer_supplement';
  protected static $inputData = [];
  protected static $publicTool = '';

  public static function push(){
      $inputData = self::getInputData();
      $allowField = ['supplement_key','answer_id'];
      $validate = new Validate(
        [
          'supplement_key'=>'require',
          'answer_id'=>'require',
        ],
        [
          'supplement_key.require'=>'补充问题的名称必须存在！',
          'answer_id'=>'require',
        ]
      );
      if(!$validate->check($validate)){
          abort(-0016,$validate->getError());
      }
      if(self::supplementKeyExists($inputData)){
          abort(-0020,'您填写的补充内容已经存在无需添加！');
      }
      $inputData['create_time'] = time();
      $dataObject = new self();
      $save = $dataObject->data($data)->allowField($allowField)->save();
      if(!$save){
          abort(-0021,'无法保存您的内容！');
      }
      return true;
  }

  /**
   * [info 搜索的内容]
   * @param  array  $condition [搜索的条件]
   * @return [type]            [返回的是数组类型的内容]
   */
  public static function info(array $condition=[]){
      $validate = new Validate(
        [
          'answer_id'=>'require',
        ],
        [
          'answer_id.require'=>'问题的内容！',
        ]
      );
      if(!$validate->check($condition)){
          abort(-0016,$validate->getError());
      }
      $where = ['answer_id'=>$condition['answer_id']];
      return collection(self::where($where)->field(true)->select());
  }

  /**
   * [supplementKeyExists 查询键名是否存在]
   * @param  array  $condition [查询的条件]
   * @return [boolean]            [返回的内容]
   */
  public static function supplementKeyExists(array $condition=[]){
      if(self::dataSum($condition) > 0){
          return true;
      }
      return false;
  }


  /**
   * [dataSum 统计数组]
   * @return [integer] [返回integer的内容信息即使是0]
   */
  public static function dataSum($condiiton){
      $validate = new Validate(
        [
          'supplement_key'=>'require',
          'answer_id'=>'require'
        ],
        [
          'supplement_key.require'=>'补充的键名必须存在！',
          'answer_id'=>'答案的id必须存在！'
        ]
      );
      if(!$validate->check($condition)){
          abort(-0015,$validate->getError());
      }
      $where = ['supplement_key'=>$condition['supplement_key'],'answer_id'=>$condition['answer_id']];
      return self::where($where)->count();
  }


  /**
   * [setInputData 输入问题的内容]
   * @param array $input [输入的表单内容]
   */
  public static function setInputData($input=[]){
      self::$inputData = $input;
  }

  /**
   * [getInputData 输入键值的名称获取返回值]
   * @param  [string] $key [输入键值的名称]
   * @return [anything]      [可能会返回任何的值]
   */
  public static function getInputData($key=null){
      $inputData = self::$inputData;
      $publicTool = self::$publicTool;
      return $publicTool::getInputData($inputData,$key);
  }
}
