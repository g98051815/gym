<?php
namespace app\questionnaire\model;
use think\Validate;
use app\common\model\Base;
use app\questionnaire\model\Question;
// CREATE TABLE `gym_questionnaire_answer` (
//   `id` int(15) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增长的id主键',
//   `question_id` int(15) unsigned NOT NULL COMMENT '问题的id',
//   `answer_id` varchar(100) NOT NULL COMMENT '答案的id',
//   `supplement_id` int(15) unsigned NOT NULL DEFAULT '0' COMMENT '补充答案的id',
//   `supplement_value` varchar(255) NOT NULL DEFAULT '' COMMENT '补充答案的内容',
//   `gym_id` char(64) NOT NULL COMMENT '查询对应健身房的id',
//   `create_time` int(15) unsigned NOT NULL COMMENT '创建的时间',
//   `health_id` int(15) unsigned NOT NULL COMMENT '当前对应的健康状况的内容',
//   PRIMARY KEY (`id`),
//   KEY `NewTable_id_IDX` (`id`,`health_id`) USING BTREE
// ) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='问卷回答表'
class QuestionnaireAnswer extends Base{
  protected $table = 'gym_questionnaire_answer';

  /**
   * [push 会员插入问卷的内容]
   * @return [type] [返回布尔类型的数据]
   */
  public static function push(){
    $validate = new Validate(
      [
        'question_id'=>'require',
        'answer_id'=>'require',
        'health_id'=>'require',
        'supplement_id'=>'require',
        'supplement_value'=>'require',
        'gym_id'=>'require'
      ],
      [
        'question_id.require'=>'问题的id必须存在！',
        'answer_id.require'=>'答案的id不必须存在！',
        'health_id.require'=>'会员当前健康状况的id必须存在！'
      ]
    );
      $inputData = self::getInputData();
      //获取对象
      $inputDataObject = file_get_contents('php://input');

      $objectDecode = json_decode($inputDataObject,true);

      for($i=0; $i<count($objectDecode); $i++){
         $objectDecode[$i]['health_id'] = $inputData['health_id'];
         $objectDecode[$i]['create_time'] = time();
         if(!$validate->check($objectDecode[$i])){
            abort(-15,$validate->getError());
         }
      }

      $allowField = ['question_id','answer_id','supplement_id','supplement_value','gym_id','health_id','create_time'];
      $dataObject = new self();
      $save = $dataObject->allowField($allowField)->saveAll($objectDecode);
      if(!$save){
          abort(-0015,'问卷添加失败！');
      }
      return [];
  }

  /**
   * [info 查询的内容]
   * @param  array  $condition [搜索的条件]
   * @return [array]            [返回的内容]
   */
  public static function info(array $condition){

      if(array_key_exists('health_id',$condition)){
          $questionList = Question::info(['types_id'=>$condition['types_id']]);
          $where = ['health_id'=>$condition['health_id']];
          $result = self::where($where);
          $questionAnsWerList = self::infoOfCache($result);
          foreach($questionList as $key => &$val){
              if($val['is_choose'] == 1){
                $answersId = array_column($val['answers_val'],'id');
                foreach($questionAnsWerList as $questionAnswerIndex => $questionAnswerItem){
                      if($questionAnswerItem['question_id'] == $val['id']){
                          $answerIdIndex = array_search($questionAnswerItem['answer_id'],$answersId);
                          $merge = array_merge($questionAnswerItem,$val['answers_val'][$answerIdIndex]);
                          $val['answers_val'] = $merge;
                      }
                }
              }else{
                foreach($questionAnsWerList as $questionAnswerIndex => $questionAnswerItem){
                    if($questionAnswerItem['question_id'] == $val['id']){

                        $merge = array_merge($questionAnswerItem,$val);
                        $val = $merge;
                    }
                }

              }

          }
          return $questionList;
      }else{
        return[];
      }
  }

}
