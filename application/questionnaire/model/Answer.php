<?php
namespace app\questionnaire\model;
use think\Model;
use think\Loader;
use think\Validate;
/**
 * CREATE TABLE `gym_answer` (
 *`id` int(15) unsigned NOT NULL COMMENT '自动增长id',
 *  `answer_value` varchar(100) NOT NULL COMMENT '答案的内容',
 *  `question_id` int(15) unsigned NOT NULL COMMENT '问题的id表',
 *  `create_time` int(15) unsigned NOT NULL COMMENT '创建的时间',
 *  `supplement` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否有补充 1:是0:否,问题的补充默认是不存在的'
 *) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4
 */


/**
 * [Answer 问题的答案内容]
 */
class Answer extends Model{
  protected $table = 'gym_answer';
  protected static $inputData = [];
  protected static $publicTool=[];


  protected static function init(){
      self::$publicTool = Loader::controller('\\app\\publictool\\controller\\PublicTool');
  }
  /**
   * [push 添加答案的数据内容]
   * @return [boolean] [返回布尔类型的数据]
   */
  public static function push(){
    //直接获取所有Input的内容
    $inputData = self::getInputData();
    $allowField = ['answer_value','question_id','supplement','create_time'];
    $validate = new Validate(
      [
        'answer_value'=>'require',//答案的内容不正确
        'question_id'=>'require', //答案对应的问题id
        'supplement'=>'require', //答案是否存在补充的内容
      ],
      [
        'answer_value.require'=>'答案的内容不能是空的！',
        'question_id.require'=>'问题的id绝对不能是空的！',
        'supplement.require'=>'请告诉我这个问题是否需要补充的内容！',
      ]
    );
    if(!$validate->check($inputData)){
          abort(-00010,$validate->getError());
    }

    if(self::answerExists($inputData)){
        abort(-0012,'您已经添加过相同的答案无需重复添加！');
    }
    $inputData['create_time'] = time(); //创建时间
    $dataObject = new self();
    $save = $dataObject->data($inputData)->allowField($allowField)->save();
    if(!$save){
      abort(-0014,'添加失败请重试！');
    }

    return true;

  }

  /**
   * [info description]
   * @return [type] [description]
   */
  public static function info(array $condition=[]){
      $validate = new Validate(
        [
          'question_id'=>'require',
        ],
        [
          'question_id.require'=>'问题的编码是必须的存在的否则无法搜索！',
        ]
      );
      if(!$validate->check($condition)){
          abort(-0014,$validate->getError());
      }
      $where = ['question_id'=>$condition['question_id']];
      return collection(self::where($where)->field(true)->select())->toArray();
  }

  /**
   * [answerExists 查询答案是否存在！]
   * @param  array  $conditon [查询的条件]
   * @return [type]           [查询的类型]
   */
  public static function answerExists(array $conditon=[]){
     if(self::dataSum($conditon) > 0){
        return true;
     }
     return false;
  }


  /**
   * [dataSum 数据统计的内容]
   * @param  [array] $conditon [查询的内容必须是数组类型的！]
   * @return [integer]           [一定会返回数字类型的内容哪怕是0]
   */
  public static function dataSum(array $conditon=[]){
    $validate = new Validate(
      [
        'answer_value'=>'require',
        'question_id'=>'require',
      ],
      [
        'answer_value.require'=>'答案的内容必须存在！',
        'question_id.require'=>'问题的id必须存在！'
      ]
    );

    if(!$validate->check($condition)){
        abort(-00013,$validate->getError());
    }
    $answerValue = $conditon['answer_value'];
    $questionId = $conditon['question_id'];
    $where = ['anwser_value'=>$answerValue,'question_id'=>$questionId];
    return self::where($where)->count();
  }


  /**
   * [setInputData 输入问题的内容]
   * @param array $input [输入的表单内容]
   */
  public static function setInputData($input=[]){
      self::$inputData = $input;
  }

  /**
   * [getInputData 输入键值的名称获取返回值]
   * @param  [string] $key [输入键值的名称]
   * @return [anything]      [可能会返回任何的值]
   */
  public static function getInputData($key=null){
      $inputData = self::$inputData;
      $publicTool = self::$publicTool;
      return $publicTool::getInputData($inputData,$key);
  }
}
