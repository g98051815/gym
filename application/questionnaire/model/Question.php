<?php
namespace app\questionnaire\model;
use think\Loader;
use think\Validate;
use app\common\model\Base;

/**
 * CREATE TABLE `gym_question` (
 *`id` int(15) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增长id主键',
 *  `question_value` varchar(100) NOT NULL COMMENT '问题的内容',
 *  `multiple_choice` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否为多选：1:是多选，0:不是多选',
 *  `create_time` int(15) unsigned NOT NULL COMMENT '创建的时间',
 *  `types_id` int(15) unsigned NOT NULL COMMENT '分类的id',
 *  PRIMARY KEY (`id`),
 *  KEY `gym_question_id_IDX` (`id`,`question_value`,`types_id`) USING BTREE
 *) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='问卷调查的问题表'
 */

/**
 * [Question description]
 */
class Question extends Base{
  protected $table = 'gym_question';
  protected static $inputData = [];
  protected static $cacheTag='question';
  protected static $publicTool = null;
  /**
   * [init 初始化的字段]
   * @return [type] [初始化的字段内容]
   */
  public static function init(){
    parent::init();
    //调用公共工具类
  }

  /**
   * [push 添加push的内容]
   * @return [bool] [返回布尔类型的值]
   */
  public static function push(){
      $inputData = self::$inputData;
      $allowField = [];
      $validate = new Validate(
        [
          'question_value'=>'require', //判断问题的内容是否为空！
          'multiple_choice'=>'require', //判断多选是否为空值
          'types_id'=>'require',
        ],
        [
          'question_value.require'=>'问题的标题不能为空！',
          'multiple_choice.require'=>'请告诉我是否要将这个问题标记为多选内容!',
          'types_id'=>'问题的内容不能为空！',
        ]
      );
      if(!$validate->check($inputData)){
          abort(-0012,$validate->getError());
      }
      //查询是否存在重复的内容字段
      if(!self::questionExists($inputData)){
          abort(-0004,'已经存在重复的问题标题！');
      }

      $dataObject = new self();
      $save = $dataObject->data($inputData)->allowField($allowField)->save();
      if(!$save){
        abort(-00010,'保存失败请重试！');
      }
      return true;
  }

  /**
   * [info description]
   * @param  array  $condition [查询的条件]
   * @return [type]            [查询后的返回值]
   */
  public static function info(array $condition=[]){
      $validate = new Validate(
         [
           'types_id'=>'require',
         ]
        ,[
          'types_id.require'=>'类型的id必须存在！'
        ]
    );
    if(!$validate->check($condition)){
        abort(-35,$validate->getError());
    }
    //通过问题的类型调用答案在进行层层调用
    $where = ['types_id'=>$condition['types_id']];
    $count  = self::where($where)->count();
    $result = $count > 0 ? self::infoOfCache(self::where($where)) : [];
    if($count > 0){
      //问题的答案表
      foreach($result as &$val){
          if($val['is_choose'] == 1){
              $answers = db('question_preset')->where(['question_id'=>$val['id']])->select();
              //循环答案
              foreach($answers as &$answerVal){
                    //选择后补充
                    if($answerVal['supplement_after_selection'] == 1){

                          $supplement = db('answer_supplement')->where(['answer_id'=>$answerVal['id']])->select();
                          $answerVal['supplement_after_selection'] = $supplement;
                    }

              }
              $val['answers_val'] = $answers;
          }
      }
    }

    return $result;
  }

  /**
   * [questionExists 查询当前的问题是否存在]
   * @param  array  $conditon [查询的条件]
   * @return [bool]           [返回布尔类型的值]
   */
  protected static function questionExists(array $conditon=[]){
        if(self::dataSum($condition) > 0){
            return true;
        }
        return false;
  }

  /**
   * [dataSum 查询当前的问题是否存在]
   * @return [integer] [返回查询问题的数量]
   */
  public static function dataSum(array $condition=[]){
      $validate = new Validate(
        ['question_value'=>'require','types_id'=>'require'],
        ['question_value.require'=>'问题的标题必须存在！','types_id'=>'您必须告诉我实在那个分类下的内容！']
      );
      $questionValue = $condtion['question_value'];
      $typesId = $conditon['types_id'];
      $where = ['question_value'=>$questionValue,'type_id'=>$typesId];
      if(!$validate->check($conditon)){
         abort(-0013,$validate->getError());
      }
      return self::where($condition)->count();
  }


}
