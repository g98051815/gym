<?php
namespace app\statistical\model;
use app\gym\model\MemberSigning as membersing; //会员与教练签约的类
use app\common\model\Base;
/**
 * [Statistical 统计类]
 */
class Statistical extends Base{
  protected $table = '';

  /**
   * [allFittenAndTheClassHoursMap 店铺下所有教练的列表以及他剩余的课时]
   * @return [type] [description]
   */
  public static function allFittenAndTheClassHoursMap(array $info=[]){

  }

  /**
   * [allGymMap 老板下面所有健身房的列表]
   * @return [type] [description]
   */
  public static function allGymMap(){

  }

  /**
   * [fitenssSalesCourseRankings 教练销售课程的排行,筛选方式使用周排行和月排行]
   * @return [array] [教练销售课程的排行列表，使用数组形式进行排列]
   */
  public static function fitenssSalesCourseRankings(){

  }

  /**
   * [FittenAndTheClassHoursMap 教练被约课的统计]
   * @param $info [搜索的条件]
   * @return array 返回数组形式的内容
   */
  public static function FittenAndTheClassHoursMap(array $info){

  }

  /**
   * [memberClassHoursMap 会员剩余课时的排行榜单]
   * @param array $info 搜索的信息
   * @return [type] [description]
   */
  public static function memberClassHoursMap(array $info=[]){
        
  }

  /**
   * [memberMakeHoursCourseMap 会员预约课程剩余的排行榜单]
   * @param  array  $info [description]
   * @return [type]       [description]
   */
  public static function memberMakeHoursCourseMap(array $info=[]){

  }

}
