<?php
namespace app\circle\controller;
use app\common\controller\Base;
use app\user\model\Auth;
class Index extends Base{
  //点赞接口
  public function like(){
    self::sendResponse(
      'get:*',
      function($input,$publicTool){
      $like = $this->loader->model('\\app\\circle\\model\\ThumbUp');
      $like::setInputData($input);
      $response = $like::thumbUp($input);
      return $publicTool::msg('success','request success',1,$response);
    });
 }

 //查看上课反馈意见
 public function opinionFeedback(){
   self::sendResponse(
     'get:*',
     function($input,$publicTool){
     $like = $this->loader->model('\\app\\circle\\model\\OpinionFeedBack');
     $response = $like->info($input);
     return $publicTool::msg('success','request success',1,$response);
   });
 }

    //点赞接口
    public function likeInfo(){
        self::sendResponse(
            'get:*',
            function($input,$publicTool){
                $like = $this->loader->model('\\app\\circle\\model\\ThumbUp');
                $response = $like::info(
                    (array_key_exists('limit',$input)?$input['limit']:10),
                    (array_key_exists('page',$input)?$input['page']:1),
                    (array_key_exists('circle_id',$input)?$input['circle_id']:null)
                );
                return $publicTool::msg('success','request success',1,$response);
            });
    }




    //评论的接口
    public function pushComment(){
        self::sendResponse(
            'get:*',
            function($input,$publicTool){
                $comment = $this->loader->model('\\app\\circle\\model\\Comment');
                $comment::setInputData($input);
                $response = $comment::push();
                return $publicTool::msg('success','request success',1,$response);
            });
    }

    /**
     * 调用健身房的评论接口
    */
    public function infoOfComment(){
        self::sendResponse(
            'get:*',
            function($input,$publicTool){
                $comment = $this->loader->model('\\app\\circle\\model\\Comment');
                $response = $comment::info($input);
                return $publicTool::msg('success','request success',1,$response);
            });
    }


 //教练成绩单接口
 public function inputTranscript(){
   self::sendResponse('get:*',function($input,$publicTool){
     $inputTranscript = $this->loader->model('\\app\\circle\\model\\Transcript');
     $inputTranscript::setInputData($input);
     $response = $inputTranscript::inputTranscript($input);
     return $publicTool::msg('success','request success',1,[]);
   });
 }

 /**
  * S
  * [circleInfo 获取朋友圈的信息]
  * @return [type] [获取朋友圈的信息]
  */
 public function circleInfo(){
   self::sendResponse('get:*',function($input,$publicTool){
     $circle = $this->loader->model('\\app\\circle\\model\\Circle');
     $input['uuid'] = array_key_exists('uid',$input)? $input['uid'] : $input['uuid'];
     $role = Auth::getRoleInfo($input['uuid']);
     $input['role'] = $role;
     $response = $circle::info($input);
     return $publicTool::msg('success','request success',1,$response);
   });
 }


    /**
     * S
     * [circleInfo 获取朋友圈的信息]
     * @return [type] [获取朋友圈的信息]
     */
    public function CourseDynamicInfo(){
        self::sendResponse('get:*',function($input,$publicTool){
            $circle = $this->loader->model('\\app\\gym\\model\\CourseDynamic');
            $response = $circle::info($input);
            return $publicTool::msg('success','request success',1,$response);
        });
    }


}
