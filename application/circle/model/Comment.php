<?php
namespace app\circle\model;
use app\common\model\Base;
use app\publictool\controller\MsgTpl;
class Comment extends Base{

    protected $table = 'gym_circle_comment';

    protected static  $autoPushCreateTime = true; //自动添加时间

    protected static $publicSphere = [1=>'公开',2=>'仅老板可见'];
    /**
     * 插入评论
    */
    public static function push(){
        self::couSave([
            ['circle_id','require','朋友圈的列表id不能为空！'],
            ['content','require','评论的内容不能为空！'],
            ['public_sphere','require','公开范围不能为空！']
        ],function($input,$self){
              if($input['public_sphere'] == 2){
                $circle = db('circle')->where('cl.id','=',$input['circle_id'])->alias('cl')->join('__GYM_INFO__ gi','cl.gym_id = gi.unique_id')->field(['cl.*','gi.boss'])->find();
                  //添加意见
                db('opinion_feedback')->insert(
                  [
                    'circle_id'=>$circle['id'],
                    'uid'=>$input['uuid'],
                    'gym_id'=>$circle['gym_id'],
                    'comment'=>$input['content'],
                    'boss_id'=>$circle['boss'],
                    'create_time'=>time(),
                  ]
                );
                return true;
              }
                $allowField = true;
                $dataObj = new $self();
                $input['uid'] = $input['uuid'];
                return $dataObj->data($input)->allowField($allowField)->isUpdate(false)->save();
        });

        return [];
    }

    /**
     * 搜索评论的接口
    */
    public static function info( $info = [] ){

        $sqlQuery = self::sqlParams($info,['limit'=>1,'page'=>1,'order'=>'cc.create_time desc','field'=>['cc.id','cc.circle_id','cc.content','cc.create_time','usi.uuid','usi.name','usi.nick_name','usi.head_figure']]);
        $where = ['cc.circle_id'=>$info['circle_id']];
        $sqlResult =function($where,$sqlQuery){
                $result = self::where($where)->alias('cc');
                $result->limit($sqlQuery['limit']);
                $result->page($sqlQuery['page']);
                $result->order($sqlQuery['order']);
                $result->field($sqlQuery['field']);
                $result->join('__USER_INFO__ usi','cc.uid = usi.uuid','INNER');
                return $result;
        };
        $count = $sqlResult($where,$sqlQuery)->count();
        $list = self::infoOfCache($sqlResult($where,$sqlQuery));
        $page = $sqlQuery['page'];
        $limit = $sqlQuery['limit'];
        return MsgTpl::createListTpl($list,$count,$limit,$page);
    }
}
