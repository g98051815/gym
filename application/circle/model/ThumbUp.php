<?php
namespace app\circle\model;
use app\common\model\Base;
use think\Loader;
use think\Validate;

/**
 * [ThumbUp 点赞功能模块]
 */
class ThumbUp extends Base{
    protected $tablename = '';
    protected static $inputData;
    protected static $msgTpl; //信息数据的模板
    protected static $publicTool; //公共工具的内容

    //初始化的类
    protected static function init(){
        //初始化信息类 createListTpl
        self::$publicTool = Loader::controller('\\app\\publictool\\controller\\PublicTool');
        self::$msgTpl = Loader::controller('\\app\\publictool\\controller\\MsgTpl');
    }

    /**
     * [thumbUp 给用户进行点赞操作]
     * @return [type] [boolean|true\false]
     */
    public static function thumbUp(){
      $inputData = self::$inputData;
      $dataObject = new self();
      $publicToolObject = self::$publicTool;
      $publicToolObject::checkInputData($inputData);
       $validate = new Validate(
         [
           'circle_id'=>'require',
           'uuid'=>'require'
       ],
         [
           'circle_id.require'=>'动态编号必须！',
           'uuid.require'=>'用户编号必须！'
         ]
       );
       if(!$validate->check($inputData)){
          abort(-0002,$validate->getError());
       }
        $exists = self::where(['uuid'=>$inputData['uuid'],'circle_id'=>$inputData['circle_id']])->count();
        if($exists < 1){
            $inputData['create_time'] = time(); //当前的创建时间
            $allowField = true;
            $save = $dataObject->data($inputData)->allowField($allowField)->isUpdate(false)->save();
            if(!$save) {
                abort(-0003, '操作失败请重试！');
            }
        }
        //证明操作成功
        return [];
    }

    /**
     * [info 查询当前文章点赞的操作]
     * @param  integer $limit  [当前文章限制的数量]
     * @param  integer $page   [当前的页面]
     * @param  string  $circle_id  [动态,的编号]
     * @param  string  $filter [筛选的内容]
     * @param  string  $order  [排序的规则]
     * @return [array]          [返回的类型]
     */
    public static function info($limit=10,$page=1,$circle_id='',$filter='',$order='create_time'){
        $response = ''; //获取数据的返回值
        $result = '' ; //获取数据的详细内容
        $count = ''; //获取数据的总数
        $where = []; //搜索的条件    示例： ['id'=>15] 等同于 thinkphp3.23的 $arr['id'] = array('eq',15);
        /**
         * 搜索的方式全部采用静态的方法进行搜素
         * 示例代码，此处涉及到oop思想
         * 注释： ( | ) 的意思代表或者的意思
         * self::where(搜索条件)->limit(限制条数)->page(当前页面的数量)->order(排序规则)->select()|->find()|->count();
         * 并且在tp5中返回的内容是对象形式的内容需要使用 collection(objcet)->toArray(); //进行获取内容的操作，但是这种情况
         * 单单只是select的情况，在find的情况下直接就可以 object()->toArray(),前面的object代表的是 find()链式操作
         */
        $msgTplObject = self::$msgTpl;
        if(empty($circle_id)){abort(-0001,'动态编码必须存在！');};
        $where = [];
        //--> 填写搜索的内容
        //<--
        $result = self::where('tup.circle_id','=',$circle_id)->alias('tup')->join('__USER_INFO__ usi','tup.uuid = usi.uuid')->limit($limit)->page($page)->order('tup.'.$order)->select();
        $count = self::where('circle_id','=',$circle_id)->count();
        $result = collection($result)->toArray();
        //$$limit 获取到的限制条数
        //$presendPage 获取到的页面数量
        //$list 数据
        //$count 获取到的数据总数
        $response =  $msgTplObject::createListTpl($result,$count,$limit,$page);
        return  $response;
    }
}
