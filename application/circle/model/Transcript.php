<?php
namespace app\circle\model;
use app\common\model\Base;
use think\Loader;
use think\Validate;
use app\gym\model\FitnessTrainerSigning;
use app\common\model\Observice;
/**
 * [Transcript 成绩单的添加模型以及成绩单的内容]
 */
class Transcript extends Base{
    protected $table = 'gym_transcript';
    protected static  $autoPushCreateTime = true; //自动添加时间
    protected static  $autoPushUpdateTime = true; //自动添加修改时间
    protected static $cacheTag = 'transcript';
    protected static $transcriptContent = ''; //成绩单的内容表
    /**
     * [funciton 默认的初始化值]
     * @var [type]
     */
    public static function init(){
       parent::init();
       self::$transcriptContent = Loader::model('\\app\\circle\\model\\TranscriptContent');
    }

    /**
     * [inputTranscript 插入成绩单]
     * 如果成绩单已经存在的就进行修改操作，如果成绩单不存在的话就进行增加操作
     * @return [type]        [插入的内容]
     */
    public static function inputTranscript(){

          return self::push();

    }

    /**
     * [push 添加的方法,添加用户的方法]
     * 成绩单的方法需要连接健康管理的内容
     * @return [boolean] [true|false,返回布尔类型的值非真即假]
     */
    public static function push(){
        self::couSave(
          [
            ['member_id','require','会员:的编号不能为空！'],
          ],
          function($input,$self){

              $allowField = ['member_id','last_update_time','create_time','fitness_id'];
              $input['fitness_id'] = $input['uuid'];
              $gymInfo = FitnessTrainerSigning::gymFitnessSigningInfo($input);
              $gymId = array_combine(['gym_id'],array_column($gymInfo,'gym_id'));
              $input = array_merge($input,$gymId);
              $dataObject  = new $self();
              $result = $dataObject->data($input)->allowField($allowField)->isUpdate(false)->save();
              $lastInsId = $dataObject->getLastInsID();
              $transcriptContent = self::$transcriptContent;
              foreach(json_decode($input['content'],true) as $key => $val){
                $val['transcript_id'] = $lastInsId;
                $transcriptContent::setInputData($val);
                $transcriptContent::push();
              }
              Observice::addObserivce('\\app\\circle\\model\\Circle',[
                'type'=>3,
                'gym_id'=>$input['gym_id'],
                'uuid'=>$input['member_id'],
                'fitness_id'=>$input['fitness_id'],
                'index_id'=>$lastInsId
              ]);
              //通知朋友圈发送消息
              Observice::notify();
              return true;
          }
        );
        //返回的内容
        return [];
    }

    /**
     * [monity 修改的内容]
     * @param  array  $input [修改的内容传入]
     * @return [array]        [true|false,修改的内容方法！]
     */
    public static function monity(array $input=[]){
      $inputData =self::getInputData();
      $inputData['last_update_time'] = time();
      $allowField = ['id','uuid','status','last_update_time'];
      $tranCotnent =self::$transcriptContent;
      $publicToolObject = self::$publicTool;
      $dataObject = new self();
      $validate = new Validate(
        [
          'transcript_id'=>'require|number',
          'photo'=>'require',
          'weight'=>'require|number',
          'three_dimension'=>'require|number',
          'bmi'=>'require|number'
        ],
        [
          'transcript_id.require'=>'成绩单编号必须！',
          'photo.require'=>'用户的照片不能为空！',
          'three_dimension.require'=>'三维不能为空！',
          'bmi.require'=>'bmi指数不能为空！',
          'three_dimension.number'=>'三位只能是数字！',
          'bmi.number'=>'三维只能是数字！',
          'weight.number'=>'体重只能是数字！'
        ]
      );
      if(!$validate->check($inputData)){
          abort(-0003,$validate->getError());
      }
      $tranCotnent::setInputData($inputData);
      if($tranCotnent::push()){
        $where = ['id'=>$inputData['transcript_id']];
        $dataObject->save(['status'=>1],$where); //修改查询的状态
        Observice::addObserivce('\\app\\circle\\model\\Circle',['type'=>'3','index_id'=>self::getInputData('transcript_id'),'uuid'=>self::getInputData('uuid')]);
        Observice::notify();
        return true;
        //通知到了
      }
      return true;
    }

    /**
     * [info 查询成绩单的信息]
     * @param  [type]  $uuid  [description]
     * @param  integer $limit [description]
     * @param  integer $page  [description]
     * @param  string  $order [description]
     * @return [type]         [description]
     */
    public static function info($condition=null,$limit=1,$page=1,$order='tc.create_time desc'){
        $result = '';
        $field = ['tc.id','tc.member_id','usi.head_figure'];
        $where = [];
        if(empty($condition)){
            abort(-0005,'用户的编号不能为空！');
        }
        $where = ['tc.id'=>$condition['id'],'tc.status'=>1];
        $result = self::where($where)->alias('tc')->field($field)->join('__USER_INFO__ usi','tc.member_id  = usi.uuid','INNER')->order($order)->limit($limit)->page($page)->select();
        // if(array_key_exists('id',$condition) && !array_key_exists('uuid',$condition)){
        //     $where = ['tc.id'=>$condition['id'],'tc.status'=>1];
        //     $result = self::where($where)->alias('tc')->field($field)->join('__USER_INFO__ usi','tc.uuid  = usi.uuid','INNER')->order($order)->limit($limit)->page($page)->select();
        // }
        //
        // if(array_key_exists('uuid',$condition) && !array_key_exists('id',$condition)){
        //     if(empty($result)){
        //     $where = ['tc.uuid'=>$condition['uuid'],'tc.status'=>1];
        //     $result = self::where($where)->alias('tc')->field($field)->join('__USER_INFO__ usi','tc.uuid  = usi.uuid','INNER')->order($order)->limit($limit)->page($page)->select();
        //     }
        //
        // }
        //
        // if(array_key_exists('id',$condition) && array_key_exists('uuid',$condition)){
        //     $where = ['tc.id'=>$condition['id'],'tc.status'=>1,'tc.uuid'=>$condition['uuid']];
        //     $result = self::where($where)->alias('tc')->field($field)->join('__USER_INFO__ usi','tc.uuid  = usi.uuid','INNER')->order($order)->limit($limit)->page($page)->select();
        // }

       if(!empty($result = collection($result)->toArray())){
          array_walk($result,['self','starategy']);
       }

       return empty($result)? [] : $result[0];
    }

    /**
     * [starategy 内容的调用策略]
     * @param  [type] $val [内容的详情]
     * @return [void]      [没有返回值]
     */
    public static function starategy(&$val){
        $transcriptId = $val['id'];
        $transcriptContentObject =  Loader::model('\\app\\circle\\model\\TranscriptContent');
        $val['content'] = $transcriptContentObject::info(2,1,$transcriptId);
    }


    /**
     * [info 查询接口]
     * @param  array  $input [description]
     * @return [type]        [description]
     */
    // public static function info(array $input = []){
    // }


}
