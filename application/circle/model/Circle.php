<?php
namespace app\circle\model;
use app\common\model\ObserviceReceiveInterface;
use app\common\model\Base;
use think\Loader;
use app\publictool\controller\MsgTpl as msgtpl;
use think\Validate;
/**
 * 圈子模型,同时也是一个观察者的接口方法
 * [circle description]
 */
class Circle extends Base implements ObserviceReceiveInterface{
  protected $tablename = 'gym_circle';
  //信息的模板
  protected static $msgTpl = null;
  //缓存列表
  protected static $cacheTag = 'gym_circle';
  //公共方法
  protected static $publicTool = null;
  //信息队列
  protected static $contentQueue = [
  ];

  /**
   * [init 这个方法是做神码设那么的]
   * @param $init [真个参数是做什么]
   * @param $init [xxxxx]
   * @return void
   */
  protected static function init(){
      //初始化信息类 createListTpl
      parent::init();
      self::$msgTpl = Loader::controller('\\app\\publictool\\controller\\MsgTpl');
      self::$publicTool = Loader::controller('\\app\\publictool\\controller\\PublicTool');
      self::$contentQueue = [
          1=>'\\app\\gym\\model\\CourseDynamic',//课程动态
          2=>'\\app\\gym\\model\\Reward', //奖励表
          3=>'\\app\\circle\\model\\Transcript', //成绩单
          4=>'\\app\\gym\\model\\Activity'//活动搜索
      ];
  }


  /**
   * [receive 接口规定的内容]
   * @param  [anything] $message [规定的内容信息，可以是任意格式的]
   * @return [void]          [此方法没有返回值]
   */
  public static function receive($message){
      //添加推送内容
      self::setInputData($message);
      return self::push();
  }

  /**
   * [push 发送推送的数据]
   * @return [type] [description]
   */
  protected static function push(){
      //<--- 判断inputData
      $result = '';
      $response = '';
      $save = '';
      $inputData = self::$inputData;
      $allowField = []; //设置筛选的字段
      $dataObject = new self();
      $publicToolObject = self::$publicTool;
      $publicToolObject::checkInputData($inputData);
      $validate = new Validate(
        [
          'type'=>'require',
          'index_id'=>'require',
          'uuid'=>'require'
        ],
        [
          'type.require'=>'属性不能为空！',
          'index_id'=>'索引id不能为空！',
          'uuid.require'=>'用户的编号不能为空！'
        ]
    );
      if(!$validate->check($inputData)){
          abort(-0002,$validate->getError());
      }
      $inputData['create_time'] = time(); //当前的创建时间
      $inputData['last_update_time'] = time(); //最后的修改时间
      $save = $dataObject->data($inputData)->allowField($allowField)->isUpdate(false)->save();
      if(!$save){
          return false;
      }
      return true;
  }

  /**
   * [info description]
   * @param  integer $limit  [限制的数量]
   * @param  integer $page   [现在要发现第几
   * @param  string  $order  [排序的内容]
   * @return [type]          [array]
   */
  public static function info($info=[]){
    //$list,$count,$limit,$presendPage $msgTpl 的参数 <——
      $validate = new Validate(
        [
          'type'=>'require',
        ],
        [
          'type.require'=>'类别不能为空！',
        ]
      );
      if(!$validate->check($info)){
          abort(-58,'获取的类别不能为空！');
      }
      //检测用户的角色
      if($info['role'] == 'fitness'){
          $result = self::fitnessInfo($info);
      }
      if($info['role'] == 'member'){
          $result = self::memberInfo($info);
      }
      if(array_key_exists('gym',$info)){
          //查询健身房的信息
          $result = self::gymCircleInfo($info);
      }
      if(array_key_exists('id',$info)){
          $result = self::circleInfo($info);
      }
      $response = [];
      $sqlParams = ['limit'=>10,'page'=>1];
      $count = 0;
      if(!empty($result) && isset($result)){
          list($response,$count,$sqlParams) = $result;
      }
      //如果数据不是空的就进行提取索引操作
      if(!empty($response) && count($response)>0){
        array_walk($response,['self','starategy']);
      }else{
          $response = [];
      }
      return msgtpl::createListTpl($response,$count,$sqlParams['limit'],$sqlParams['page']);
  }

  //通过id获得内容
  protected static function circleInfo( $info ){
      //设置id的编号
      $where = ['ci.id'=>$info['id']];
      $sqlQuery = self::sqlParams($info,['field'=>['ci.id','ci.type','ci.index_id','ci.uuid','usi.head_figure','usi.nick_name'],'limit'=>10,'page'=>1,'order'=>'ci.create_time desc']);
      $resultQuery =function($where , $sqlQuery){
        $result = self::where($where)->field($sqlQuery['field'])->alias('ci');
        $result->join('__USER_INFO__ usi','ci.uuid = usi.uuid','LEFT')->order($sqlQuery['order'])->limit($sqlQuery['limit'])->page($sqlQuery['page']);
        return $result;
      };
      //统计数量的信息
      $count = $resultQuery($where,$sqlQuery)->count();
      //统计查询的列表
      $response = self::infoOfCache($resultQuery($where,$sqlQuery));
      //返回统计过后的数组信息
      return [$response,$count,$sqlQuery];
  }

  /**
   * 健身房的朋友圈信息列表
   * @param  $info array
   * @return array
  */
  protected static function gymCircleInfo($info = []){
        if(array_key_exists('id',$info)){
          return [[],0,0];
        }
        //检索健身房的信息
        $validate = new Validate(
        [
            'gym_id'=>'require',
        ],
        [
            'gym_id.require'=>'健身房的编号不能为空'
        ]
        );
      if(!$validate->check($info)){
          //提示报错信息
          abort(-70,$validate->getError());
      }

      //设置健身房的编号
      $where = ['ci.gym_id'=>$info['gym_id']];
      $type = $info['type'];
      $queue = self::$contentQueue; //获取的队列信息
      if($type !=='all' && array_key_exists($type,$queue)){
          $where['ci.type'] = $type;
      }
      $sqlQuery = self::sqlParams($info,['field'=>['ci.id','ci.type','ci.index_id','ci.uuid','usi.head_figure','usi.nick_name'],'limit'=>10,'page'=>1,'order'=>'ci.create_time desc']);
      $resultQuery =function($where , $sqlQuery){
          $result = self::where($where)->field($sqlQuery['field'])->alias('ci');
          $result->join('__USER_INFO__ usi','ci.uuid = usi.uuid','LEFT')->order($sqlQuery['order'])->limit($sqlQuery['limit'])->page($sqlQuery['page']);
          return $result;
      };
      //统计数量的信息
      $count = $resultQuery($where,$sqlQuery)->count();
      //统计查询的列表
      $response = self::infoOfCache($resultQuery($where,$sqlQuery));
      //返回统计过后的数组信息
      return [$response,$count,$sqlQuery];
  }

  /**
   * 教练的健身房朋友圈信息
   * @param $info array
   * @return  array
  */
  protected static function fitnessInfo($info = []){
      if(array_key_exists('gym',$info) || array_key_exists('id',$info)){
        return [[],0,0];
      }
      $type = $info['type'];
      $queue = self::$contentQueue; //获取的队列信息
      $where = ['ci.fitness_id'=>$info['uuid']];
      if($type !=='all' && array_key_exists($type,$queue)){
          $where['ci.type'] = $type;
      }
      $sqlQuery = self::sqlParams($info,['field'=>['ci.id','ci.type','ci.index_id','ci.uuid','usi.head_figure','usi.nick_name'],'limit'=>10,'page'=>1,'order'=>'ci.create_time desc']);
      $resultQuery =function($where , $sqlQuery){
          return self::where($where)->alias('ci')->field($sqlQuery['field'])->join('__USER_INFO__ usi','ci.uuid = usi.uuid')->order($sqlQuery['order'])->limit($sqlQuery['limit'])->page($sqlQuery['page']);
      };
      $count = $resultQuery($where,$sqlQuery)->count();
      $response = self::infoOfCache($resultQuery($where,$sqlQuery));
      return [$response,$count,$sqlQuery];
  }

  /**
   * 会员的健身房信息
   * @param  $info array
   * @return array
  */
  protected static function memberInfo($info = []){
    if(array_key_exists('gym',$info) || array_key_exists('id',$info)){
      return [[],0,0];
    }
      //筛选类别
      $type = $info['type'];
      $queue = self::$contentQueue; //获取的队列信息
      $where = ['ci.uuid'=>$info['uuid']];
      if($type !=='all' && array_key_exists($type,$queue)){
          $where['ci.type'] = $type;
      }
      $sqlQuery = self::sqlParams($info,['field'=>['ci.id','ci.type','ci.index_id','ci.uuid','usi.head_figure','usi.nick_name'],'limit'=>10,'page'=>1,'order'=>'ci.create_time desc']);
      $resultQuery =function($where , $sqlQuery){
          return self::where($where)->alias('ci')->field($sqlQuery['field'])
              ->join('__USER_INFO__ usi','ci.uuid = usi.uuid')->order($sqlQuery['order'])->limit($sqlQuery['limit'])->page($sqlQuery['page']);
      };
      $count = $resultQuery($where,$sqlQuery)->count();
      $response = self::infoOfCache($resultQuery($where,$sqlQuery));
      return [$response,$count,$sqlQuery];
  }

  /**
   * [starategy 提取圈子模型中的详细内容不论这个内容是什么]
   * @param  [type] $val [description]
   * @return [type]      [description]
   */
  protected static function starategy(&$val){
     //提取圈子中的点赞内容
     $thumbUp = Loader::model('\\app\\circle\\model\\ThumbUp');
     $comment = Loader::model('\\app\\circle\\model\\Comment');
     $circleId = $val['id'];
     $typeId = $val['type'];
     $indexId = $val['index_id'];
     $uuid = $val['uuid'];
     $queue =   self::$contentQueue;
     $object = $queue[$typeId];
     $val['details'] = $object::info(['id'=>$indexId]);
     $val['thumb_up_queue'] = $thumbUp::info(10,1,$circleId);
     $val['comment'] = $comment::info(['circle_id'=>$circleId,'limit'=>3,'order'=>'cc.create_time asc']);
  }
}
