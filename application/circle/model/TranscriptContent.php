<?php
namespace app\circle\model;
use app\common\model\Base;
/**
 * [TranscriptContent 成绩单内容]
 */
class TranscriptContent extends Base{

  protected $table = 'gym_transcript_content';
  protected static  $cacheTag='transcript_content'; //缓存的名称,关闭使用缓存直接使用false
  protected static  $autoRefreshCache=true; //增加，修改，删除，自动更新缓存
  protected static  $autoPushCreateTime = true; //自动添加时间
  protected static  $autoPushUpdateTime = true; //自动添加修改时间
  protected static $inputData = ''; //插入的数据

  /**
   * [push 插入内容]
   * @return [void] [void]
   */
  public static function push(){
      self::couSave(
        [
          ['weight','require|number','体重:不能为空！|只能为数字！'],
          ['three_dimension','require','三维:不能为空!'],
          ['bmi','require|number','体脂率:不能为空！|只能为数字！'],
          ['photo','require','照片:不能为空！'],
          ['transcript_id','require|number','编号不能为空！']
        ],
        function($input,$self){
            $allowField = ['weight','three_dimension','bmi','transcript_id','photo','create_time','last_update_time'];
            $dataObject = new $self();
            return $dataObject->data($input)->allowField($allowField)->isUpdate(false)->save();
        }
      );
      return true;
  }
  /**
   * [info 查询接口]
   * @param  array  input [查询的接口]
   * @return [type]       [description]
   */
  public static function info($limit=2,$page=1,$transcriptId,$order='create_time asc'){
      $where = ['transcript_id'=>$transcriptId];
      return collection(self::where($where)->order($order)->order('create_time')->limit(2)->select())->toArray();
  }

  /**
   * [countSum 返回成绩单中记录的条数]
   * @param  [type] $transcriptId [description]
   * @return [type]               [description]
   */
  public static function countSum($transcriptId = null){
        if(is_null($transcriptId)){
            $transcriptId = self::getInputData('transcript_id');
        }
        $where = ['transcript_id'=>$transcriptId];
        return self::where($where)->count();
  }


}
