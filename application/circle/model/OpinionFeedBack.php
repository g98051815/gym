<?php
namespace app\circle\model;
use app\common\model\Base;
use app\publictool\controller\MsgTpl;

class OpinionFeedBack extends Base{

    protected $table = 'gym_opinion_feedback';

    /**
     * [info description]
     * @return [type] [description]
     */
    public static function info($info = []){

          $sqlQuery = self::sqlParams($info,[
            'field'=>[
                'fd.*',
                'usi.head_figure'
            ],
            'limit'=>10,
            'page'=>1,
            'order'=>'create_time desc'
          ]);

          $where = ['gym_id'=>$info['gym_id']];
          $sqlResult = function($where,$sqlQuery){
              return self::where($where)->alias('fd')->join('__USER_INFO__ usi','fd.uid = usi.uuid','INNER');
          };
          $count = $sqlResult($where,$sqlQuery)->count();
          $result =  $count > 0 ? self::infoOfCache($sqlResult($where,$sqlQuery)->limit($sqlQuery['limit'])->page($sqlQuery['page'])->field($sqlQuery['field'])->order($sqlQuery['order'])):[];
          return MsgTpl::createListTpl($result,$count,$sqlQuery['limit'],$sqlQuery['page']);
    }


}
