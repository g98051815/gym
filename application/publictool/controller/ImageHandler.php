<?php
/**
 * php的图片处理类
 */
namespace app\publictool\controller;
class ImageHandler{
  /**
   * [__construct 默认填入图片的地址]
   * @param [type] $imageUrl [默认填入图片的地址]
   */
  public function __construct($imageUrl=null){


  }


  public function image($imageUrl=null){
    if(is_null($imageUrl)){throw new \Exception('图片不能为空！');}
    $image = imagecreatefrompng($imageUrl);
    $black = imagecolorallocate($image, 0, 0, 0);
    imagettftext($image,14,0,610,490,$black,'/usr/share/fonts/truetype/freefont','2017');
    ob_start();
    imagepng($image);
    $img = ob_get_clean();
    imagedestroy($image);
    return response($img, 200, ['Content-Length' => strlen($img)])->contentType('image/png');
  }


}
