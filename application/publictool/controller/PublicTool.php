<?php
namespace app\publictool\controller;
use app\publictool\controller\UniqueId;
use app\publictool\controller\PublicTool;
use think\Request;
use app\publictool\controller\CoCaptcha;
use think\Response;
use think\Session;
use think\Url;
class PublicTool{
    /**
    *密码加密的方法
    *@param $express 等待加密的明文
    *@return String of hash_sha1
    */
    public static function passwordEncryption($express=null){
        if(empty($express)){
            throw new \Exception('待加密的明文不能为空！');
        }
        return sha1($express);
    }

    //生成表单令牌
    public static function token($name='__token__',$type='md5'){
        return Request::instance()->token($name,$type);
    }

    /**
    *生成唯一的id不论是什么东西
    */
    public static function createUniqueId(){
        return UniqueId::createUniqueId();
    }

    /**
    *生成信息模版
    */
    public static function msg($type=null,$msg='',$code=0000,array $response=[],$option=[]){

        return MsgTpl::msg($type,$msg,$code,$response,$option);

    }

    public function captcha(){
         $captchaUniqueCode = self::createUniqueId();
         $captchaUrl = Url::build('/captchaShow?' . $captchaUniqueCode);
         $msg = self::msg('success','验证码获取成功！',0001,['verify_code_unique_id'=>$captchaUniqueCode,'captchaUrl'=>$captchaUrl]);
         Response::create($msg,config('show_msg'),0001)->send();
         exit;
    }

    //验证验证码
    public function checkCaptcha(){
          $captcha = new CoCaptcha((array)config('captcha'));
          $check = $captcha->check(input('get.verify_code'),input('get.verify_code_unique_id'));
          dump($check);
    }

    //显示验证码
    public function captchaShow(){
        $captcha = new CoCaptcha((array)config('captcha'));
        return $captcha->entry(key(input('get.')));
    }

    //获取模型的内容
    public static function getInputData($inputData=[],$key=null){
         if(empty($inputData)){
            return [];
         }
         //返回所有数据
         if(is_null($key)){
            return $inputData;
         }
         if(!array_key_exists($key,$inputData)){
            return null;
         }
         return $inputData[$key];
    }


    //替换输入的内容
    public static function replaceInputData($dbColumn,$replaceColumn){
        $result = []; //返回值
        foreach($dbColumn as $key => $val){
            if(!array_key_exists($val,$replaceColumn)){
                continue;
            }
            $inputValue =$replaceColumn[$val];unset($replaceColumn[$val]);
            $result[$key] = $inputValue;
        }
        return array_merge_recursive($result,$replaceColumn);
    }

    //检查inputData是否为空
    public static function checkInputData($inputData,$msg='关键字段不能为空！',$code=-1000){
            if(empty($inputData)){
                abort($code,$msg);
            }
    }


}
