<?php
namespace app\publictool\controller;
/**
*反馈信息模版
*/
class MsgTpl{
    /**
    *拼接信息模版
    *@param $type [模板的类型]
    *@param $msg  [模板的信息]
    *@param $response [返回的数据信息]
    *@param $code [返回的编码信息]
    *@param $option [返回的操作信息]
    */

    public static function msg($type=null,$msg='',$code=0000,$response=[],$option=[]){
        //目前的错误信息有成功与失败两种方式
        if(is_null($type) || empty($type) || !array_key_exists($type,static::closureMsg())){
            throw new \Exception('信息类型不正确');
        }

        $closure = static::closureMsg()[$type];
        return $closure($msg,$code,$response);
    }

    private static function closureMsg(){
        $closureStock =  [
            //返回成功的信息
            'success'=>function($msg,$code,$response){
                return static::msgTpl(true,$msg,$code,$response);
            },
            //返回失败的信息
            'error'=>function($msg,$code,$response){
                return static::msgTpl(false,$msg,$code,$response);
            }
        ];
        return $closureStock;
    }


   /**
    * [createListTpl 生成列表页面的条数]
    * @param  [type] $list        [列表数据返回的标准类型]
    * @param  [type] $count       [当前的总数据条数]
    * @param  [type] $limit       [当前的筛选数量]
    * @param  [type] $presendPage [当前的页面]
    * @return [array]              [返回拼接号的数据]
    */
    public static function createListTpl($list,$count,$limit,$presendPage){
        //返回拼装好的数组
        return [
            'list'=>$list,
            'count'=>$count,
            'presend_page'=>$presendPage,
            'limit'=>$limit,
            'page_sum'=>(int)($count/$limit) //计算页面需要的内容
        ];
    }

    /**
    *短信模板
    *
    */
    private static function msgTpl($success=null,$msg='',$code,array $response=[]){
         return [
            'code'=>$code,
            'success'=>$success,
            'time_stamp'=>time(),
            'msg'=>$msg,
            'response'=>$response,
        ];
    }




}
