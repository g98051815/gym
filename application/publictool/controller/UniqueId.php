<?php
namespace app\publictool\controller;

class UniqueId{

    /**
    *生成唯一的id
    */
    public static function createUniqueId(){
        $microTime = microtime(); //毫秒
        list($a_dec,$a_sec)=explode(' ',$microTime);
        $dec_hex = dechex($a_dec*10000000);
        $asc_hex = dechex($a_sec);
        self::ensureLength($dec_hex,5);
        self::ensureLength($asc_hex,6);
        $guid = '';
        $guid.= $dec_hex;
        $guid.=self::createGuidSection(3);
        $guid.=self::createGuidSection(4);
        $guid.=self::createGuidSection(4);
        $guid.=self::createGuidSection(4);
        $guid.=$asc_hex;
        $guid.=self::createGuidSection(6);
        return $guid;
    }


    /**
    *保证用户唯一标识符的长度
    */
    private static function ensureLength(&$string,$length){
        $strlen = strlen($string);
        if($strlen < $length){
            $string = str_pad($string,$length,'0');
        }else if($strlen > $length){
            $string = substr($string,0,$length);
        }
    }


    /**
    *创建guid
    */
    private static  function createGuidSection($charActers){
        $result = '';
        for($i=0; $i<$charActers; $i++){
            $result.=dechex(mt_rand(0,15));
        }
        return $result;
    }

}