<?php
namespace app\publictool\controller;
use think\exception\HttpException;
use app\publictool\controller\PublicTool;
use think\Loader;
class CheckAuth{
    //检查访问权限
    public function run(&$params){
        $auth = Loader::model('\app\user\model\Auth');
        $response = Loader::model('\think\Response');
        try{
            $auth::check(input('get.hashcode'));
        }catch(HttpException $e){
            $response->create(PublicTool::msg('error',$e->getMessage(),-0002),config('show_msg'),200)->send();
            exit;
        }
    }

}