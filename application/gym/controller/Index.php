<?php
namespace app\gym\controller;
use think\Loader;
use app\common\controller\Base; //控制器基础模块
use app\gym\model\Activity as activity; //活动添加模块
use app\gym\model\GymCircle as gymcircle; //健身房朋友圈
/**
*健身房模块
*8f8571f7071cc356587d597eeea9bdea  //新注册的会员编号
*/
class Index extends Base{
    //访问参数
    //http://localhost/gym/public/index.php/gym/input?
    //boss=2db04240496c5f1bb6f759537f8d3b33&
    //store_title='这个是门店的名称'&
    //phone_num=18513853817&
    //tel_num=0433112322&
    //address='这个是门店的地址信息怎么填写无所谓'&
    //profile=这个是门店的简介&
    //pictures={"http://www.xxx.com"}&
    //建立的第一个方法
    public function index(){
      self::sendResponse(
        'get:*',
        function($input,$publicTool){
        $index = $this->loader->model('\\app\\gym\\model\\GymInfo');
        $index::setInputData($input);
        $response = $index::input();
        return $publicTool::msg('success','request success',1,$response);
      });
    }

    /**
     * [ActivityMonity 活动修改的接口]
     */
    public function ActivityMonity(){
      self::sendResponse(
        'get:*',
        function($input,$publicTool){
        $obj = $this->loader->model('\\app\\gym\\model\\Activity');
        $obj::setInputData($input);
        $response = $obj::monity();
        return $publicTool::msg('success','request success',1,[]);
      });
    }

    /**
     * [pushReward 添加奖状]
     * @return [type] [description]
     */
    public function pushReward(){
      self::sendResponse(
        'get:*',
        function($input,$publicTool){
        $index = $this->loader->model('\\app\\gym\\model\\Reward');
        $index::setInputData($input);
        $response = $index::push();
        return $publicTool::msg('success','request success',1,$response);
      });
    }

    /**
     * [memberTransfer 会员转移]
     * @return [type] [description]
     */
    public function memberTransfer(){
      self::sendResponse(
        'get:*',
        function($input,$publicTool){
        $index = $this->loader->model('\\app\\gym\\model\\MemberSigning');
        $input = file_get_contents('php://input');
        $index::setInputData(json_decode($input,true));
        $response = $index::memberTransfer();
        return $publicTool::msg('success','request success',1,$response);
      });
    }

    /**
    *查询课程动态信息的内容
    */
    public function fitnessCountInfo(){
      self::sendResponse('get:*',function($input,$publicTool){
          $info = $this->loader->model('\\app\\gym\\model\\MemberSigning');
          return $publicTool::msg('success','request success',1,$info::fitnessCountInfo($input));
      });
    }

    /**
     * [memberRestOfClassCount 会员的课程时间统计列表]
     * @return [type] [description]
     */
    public function memberRestOfClassCount(){
      self::sendResponse('get:*',function($input,$publicTool){
          $info = $this->loader->model('\\app\\gym\\model\\MemberSigning');
          return $publicTool::msg('success','request success',1,$info::remainingClassHour($input));
      });
    }

    /**
     * [memberRestOfClassCount 会员的课程时间统计列表]
     * @return [type] [description]
     */
    public function memberRestContentOfClassCount(){
      self::sendResponse('get:*',function($input,$publicTool){
          $info = $this->loader->model('\\app\\gym\\model\\MemberSigning');
          return $publicTool::msg('success','request success',1,$info::remainingClassHourContent($input));
      });
    }

    /**
     * [gymCourseHourCount 健身房今日约克]
     * @return [type] [description]
     */
    public function gymCourseHourCount(){
      self::sendResponse('get:*',function($input,$publicTool){
          $info = $this->loader->model('\\app\\gym\\model\\GymInfo');
          return $publicTool::msg('success','request success',1,$info::courseCountInfo($input));
      });
    }


    /**
    *查询课程动态信息的内容
    */
    public function courseInfo(){
      self::sendResponse('get:*',function($input,$publicTool){
          $info = $this->loader->model('\\app\\reservation\\model\\ArrangeCourse');
          return $publicTool::msg('success','request success',1,$info::courseInfo($input));
      });
    }

    /**
     * 用户参加活动的接口
    */
    public function activityJoin(){
        self::sendResponse(
            'get:*',
            function($input,$publicTool){
                $index = $this->loader->model('\\app\\gym\\model\\ActivityJoin');
                $index::setInputData($input);
                $response = $index::joinActivity();
                return $publicTool::msg('success','request success',1,$response);
            });
    }


    public function activityAdd(){
      self::sendResponse(
          'get:*',
          function($input,$publicTool){
              $index = $this->loader->model('\\app\\gym\\model\\Activity');
              $index::setInputData($input);
              $response = $index::newAdd();
              return $publicTool::msg('success','request success',1,$response);
          });
    }

    /**
     * 查询活动的相关信息
    */
    public function activityJoinInfo(){
        self::sendResponse(
            'get:*',
            function($input,$publicTool){
                $index = $this->loader->model('\\app\\gym\\model\\ActivityJoin');
                $response = $index::info($input);
                return $publicTool::msg('success','request success',1,$response);
            });
    }


    /**
     * 活动的信息接口
    */
    public function activityInfo(){
        self::sendResponse(
            'get:*',
            function($input,$publicTool){
                $activity = $this->loader->model('\\app\\gym\\model\\Activity');
                $response = $activity::info($input);
                return $publicTool::msg('success','request success',1,$response);
            });
    }

    /**
     * 今天的营养餐
    */
    public function mealTodayInfo(){
        self::sendResponse(
            'get:*',
            function($input,$publicTool){
                $meal = $this->loader->model('\\app\\gym\\model\\NutritiousMeal');
                $response = $meal::mealToday($input);
                return $publicTool::msg('success','request success',1,$response);
            });
    }

    /**
     * 查询营养餐的信息周一到周五
    */
    public function mealInfo(){
        self::sendResponse(
            'get:*',
            function($input,$publicTool){
                $meal = $this->loader->model('\\app\\gym\\model\\NutritiousMeal');
                $response = $meal::info($input);
                return $publicTool::msg('success','request success',1,$response);
            });
    }

    /**
     * 健身房的营养餐添加
    */
    public function mealPush(){
        self::sendResponse(
            'post:*',
            function($input,$publicTool){
                $meal = $this->loader->model('\\app\\gym\\model\\NutritiousMeal');
                $meal::setInputData($input);
                $response = $meal::push();
                return $publicTool::msg('success','request success',1,$response);
        });
    }

    //课程动态图片的信息
    public function courseDynamicImgInfo(){
        self::sendResponse(
            'get:*',
            function($input,$publicTool){
                $meal = $this->loader->model('\\app\\gym\\model\\GymCourseDynamicImg');
                $response = $meal::info($input);
                return $publicTool::msg('success','request success',1,$response);
            });
    }


    //课程动态图片的添加
    public function courseDynamicImgPush(){
        self::sendResponse(
            'post:*',
            function($input,$publicTool){
                $meal = $this->loader->model('\\app\\gym\\model\\GymCourseDynamicImg');
                $meal::setInputData($input);
                $response = $meal::initial();
                return $publicTool::msg('success','request success',1,$response);
            });
    }

    //课程动态图片的修改
    public function courseDynamicImgMonity(){
        self::sendResponse(
            'post:*',
            function($input,$publicTool){
                $meal = $this->loader->model('\\app\\gym\\model\\GymCourseDynamicImg');
                $meal::setInputData($input);
                $response = $meal::push();
                return $publicTool::msg('success','request success',1,$response);
            });
    }

    /**
     * [健身房的查询方法]
     * @return void
     */
    public function gymInfo(){
        self::sendResponse(
            'post:*',
            function($input,$publicTool){
                $meal = $this->loader->model('\\app\\gym\\model\\GymInfo');
                $response = $meal::info($input);
                return $publicTool::msg('success','request success',1,$response);
            });
    }


    /**
     * [健身房的查询方法]
     * @return void
     */
    public function gymMonity(){
        self::sendResponse(
            'post:*',
            function($input,$publicTool){
                $meal = $this->loader->model('\\app\\gym\\model\\GymInfo');
                if(!array_key_exists('boss',$input)){
                    $input['boss'] = $input['uuid'];
                }
                $meal::setInputData($input);
                $response = $meal::modify();
                return $publicTool::msg('success','request success',1,$response);
            });
    }


    //老板主页下的店铺查询接口
    public function bossShop(){
       self::sendResponse('get:*',function($input,$publicTool){
         $bossShop = $this->loader->model('\\app\\gym\\model\\GymSumInfo');
         $response = $bossShop::query($input);
         return $publicTool::msg('success','request success',1,$response);
       });
    }

 //老板店铺下面的会员列表
   public function bossMember(){
     self::sendResponse('get:*',function($input,$publicTool){
       $bossMember = $this->loader->model('\\app\\gym\\model\\GymMembers');
       $response = $bossMember::member($input);
       return $publicTool::msg('success','request success',1,$response);
     });
   }

   //老板店铺下面的教练列表
     public function bossCoach(){
       self::sendResponse('get:*',function($input,$publicTool){
         $bossCoach = $this->loader->model('\\app\\gym\\model\\GymCoach');
         $response = $bossCoach::coach($input);
         return $publicTool::msg('success','request success',1,$response);
       });
     }

     /**
      * [MemberFitnessList 会员健身排行榜的信息]
      */
     public function MemberFitnessList(){
        self::sendResponse('get:*',function($input,$publicTool){
            $arrangeCourse = $this->loader->model('\\app\\reservation\\model\\ArrangeCourse');
            $input = $input['gym_id'] !== 'all'? $input : $this->loader->model('\\app\\gym\\gymInfo')->getAllGyms('老板的编号，通过sessionId识别程序获得');
            $response = $arrangeCourse->memberFitnessList($input);
            return $publicTool::msg('success','request success',1,$response);
        });
     }

     public function fitnessListRankings(){
       self::sendResponse('get:*',function($input,$publicTool){
           $arrangeCourse = $this->loader->model('\\app\\reservation\\model\\ArrangeCourse');
           $input = $input['gym_id'] !== 'all'? $input : $this->loader->model('\\app\\gym\\gymInfo')->getAllGyms('老板的编号，通过sessionId识别程序获得');
           $response = $arrangeCourse->fitnessListRankings($input);
           return $publicTool::msg('success','request success',1,$response);
       });
     }

     /**
      * 老板下面的所有健身房
     */
     public function allGymByBoos(){
         self::sendResponse('get:*',function($input,$publicTool){
             $bossMember = $this->loader->model('\\app\\gym\\model\\GymInfo');
             $response = $bossMember::getAllGyms($input);
             return $publicTool::msg('success','request success',1,$response);
         });
     }

     /**
      * [FitnessSigningGym 教练签约健身房]
      */
     public function fitnessSigningGym(){
         self::sendResponse('get:*',function($input,$publicTool){
             $sigin ='\\app\\gym\\model\\FitnessTrainerSigning';
             $sigin::setInputData($input);
             $response = $sigin::signing();
             return $publicTool::msg('success','request success',1,$response);
         });
     }

     /**
      * 教练和健身房的解约
     */
     public function fitnessUnSigningGym(){
         self::sendResponse('get:*',function($input,$publicTool){
             $sigin ='\\app\\gym\\model\\FitnessTrainerSigning';
             $sigin::setInputData($input);
             $response = $sigin::unFitnessByGmySign();
             return $publicTool::msg('success','request success',1,$response);
         });
     }


     /**
      * [memberSigningFitness 会员签约教练的申请接口]
      * @return [type] [array]
      */
     public function memberSigningFitness(){
       self::sendResponse('get:*',function($input,$publicTool){
         $sign = $this->loader->model('\\app\\gym\\model\\MemberSigning');
         $input['member_id'] = $input['uuid'];
         $response = $sign::info($input);
         return $publicTool::msg('success','request success',1,$response);
       });
     }


     /**
      * 查询会员签约的教练信息
     */
     public function memberOfFitness(){
         self::sendResponse('get:*',function($input,$publicTool){
             $sign = $this->loader->model('\\app\\user\\model\\MemberSigningApplication');
             $response = $sign::setInputData($input);
             //会员对教练发送签约申请
             $response = $sign::memberSignApplication();
             return $publicTool::msg('success','request success',1,$response);
         });
     }


     /**
      * [fitnessAgree 确认教练和会员的签约状态]
      * @return [type] [description]
      */
     public function fitnessAgreeSign(){
        self::sendResponse('get:*',function($input,$publicTool){
            $agree = $this->loader->model('\\app\\user\\model\\MemberSigningApplication');
          //教练同意签约
            $sign = $this->loader->model('\\app\\gym\\model\\MemberSigning');
            $agree::setInputData($input);
            if(!array_key_exists('agree', $input)){abort(-20,'缺少参数:agree！');}
            switch($input['agree']){
                //教练同意签约
                case 1:
                //教练开始签约
                $sign::setInputData($input);
                $signing = $sign::signing();
                //通知接口
                $agree::agreeSign();
                break;
                //教练不同意签约
                case 0:
                  $agree::refuseSign();
                  break;
                default:
                abort(-15,'agree:参数错误请重试！');
            }
            return $publicTool::msg('success','request success',1,[]);
        });
     }

    /**
     * [gymMemberList 健身房下的会员列表]
     * @return [type] [description]
     */
    public function gymMemberList(){
      self::sendResponse('get:*',function($input,$publicTool){
        $memberList = $this->loader->model('\\app\\gym\\model\\MemberSigning');
        $response = $memberList::gymMemberList($input);
        return $publicTool::msg('success','request success',1,$response);
      });
    }

    /**
     * [memberList 教练下的会员列表]
     * @return [type] [description]
     */
    public function memberList(){
      self::sendResponse('get:*',function($input,$publicTool){
        $memberList = $this->loader->model('\\app\\gym\\model\\MemberSigning');
        $response = $memberList::memberList($input);
        return $publicTool::msg('success','request success',1,$response);
      });
    }

    /**
    *会员的课时操作
    */
    public function memberHoursOption(){
      self::sendResponse('get:*',function($input,$public){
          $hourCourse = $this->loader->model('\\app\\gym\\model\\HourCourse');
          $input['fitness_id'] = $input['uuid'];
          $hourCourse->setInputData($input);
          $response = $hourCourse->push();
          return $public::msg('success','request success',1,$response);
      });
    }

    /**
    *会员的课时信息查询，列表形式展现
    */
    public function memberHourseInfo(){
      self::sendResponse('get:*',function($input,$public){
          $hourCourse = $this->loader->model('\\app\\gym\\model\\HourCourse');
          $input['fitness_id'] = $input['uuid'];
          $response = $hourCourse->info($input);
          return $public::msg('success','request success',1,$response);
      });
    }

    public function memberHourseInfoItem(){
      self::sendResponse('get:*',function($input,$public){
          $hourCourse = $this->loader->model('\\app\\gym\\model\\HourCourse');
          $input['fitness_id'] = $input['uuid'];
          $response = $hourCourse->infoItem($input);
          return $public::msg('success','request success',1,$response);
      });
    }


    /**
    *教练解约会员
    */
    public function fitnessUnsignMember(){
        self::sendResponse('post:*',function($input,$public){
            $signing = $this->loader->model('\\app\\gym\\model\\MemberSigning');
            $signing->setInputData($input);
            //教练和会员的解约
            $response = $signing->unsigning();
            return $public::msg('success','request success',1,$response);
        });
    }

    /**
     * [membersigningInfo ]
     * @return [type] [description]
     */
    public function membersigningInfo(){

    }

    /**
     *教练解约会员
     */
    public function memberUnsignitness(){
        self::sendResponse('post:*',function($input,$public){
            $signing = $this->loader->model('\\app\\gym\\model\\MemberSigning');
            $input['member_id'] = $input['uuid'];
            $signing->setInputData($input);
            //教练和会员的解约
            $response = $signing->unsigning();
            return $public::msg('success','request success',1,$response);
        });
    }



    /**
     *晴空数据库
     */
    public function clearGymReservationCourse(){
        self::sendResponse('post:*',function($input,$public){
            $rel = db()->query('TRUNCATE TABLE `gym_reservation_course`');
            dump('获取清空成功，你可以尝试以下');exit;
            return $public::msg('success','request success',1,[]);
        });
    }
}
