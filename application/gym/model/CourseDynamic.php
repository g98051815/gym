<?php
namespace app\gym\model;
use think\Validate;
use app\common\model\Base;
use app\reservation\model\ArrangeCourse;
class CourseDynamic extends Base{

  protected $table = 'gym_course_dynamic';

  protected static  $cacheTag='course_dynamic'; //缓存的名称,关闭使用缓存直接使用false

  protected static  $autoRefreshCache=true; //增加，修改，删除，自动更新缓存

  protected static  $autoPushCreateTime = true; //自动添加时间

  /**
   * [info 创建信息]
   * @return [type] [description]
   */
  public static function info($info){
    $validate = new Validate(
      [
        'id'=>'require'
      ],
      [
        'id.require'=>'IndexId不能是空的！'
      ]
    );
    if(!$validate->check($info)){
        abort(-25,$validate->getError());
    }
    $field = [
        'cd.id',
        'cd.course_content_id',
        'cd.coach_reviews',
        'cd.coach_id',
        'cd.gym_id',
        'cd.member_id',
        'cd.create_time',
        'cd.status',
        'usi.head_figure',
        'usi.nick_name',
        'usi.name',
        'gi.store_title'
    ];
    $where = ['cd.id'=>$info['id'],'cd.status'=>1];
    $result = self::where($where)->alias('cd');
    $result->join('__USER_INFO__ usi','cd.coach_id = usi.uuid','INNER')->field($field);
    $result->join('__GYM_INFO__ gi','cd.gym_id = gi.unique_id','INNER')->order('cd.create_time desc');
    $response = self::infoOfCache($result,true)[0];
    $imgList= db('course_dynamic_img')->where(['course_dynamic_id'=>$response['id']])->field(['img_url'])->limit(4)->order('create_time desc')->select();
    $response['img_url'] =$imgList[0]['img_url'];
    if(array_key_exists('detail',$info) || in_array('detail',$info)){
          $response['course'] = ArrangeCourse::courseInfo(['id'=>$response['course_content_id']]);
    }
      return $response;
  }

  /**
   * [detailed 课程动态]
   * @return [type] [description]
   */
  public static function detailed($info){

      $where = ['cd.id'=>$info['id'],'cd.status'=>1];
      $sqlQuery = self::sqlParams($info,
      [
        'field'=>true,
        'limit'=>10,
        'page'=>1,
      ]);
      $sqlResult = function($where,$sqlQuery){
          return self::where($where)->field($sqlQuery['field']);
      };
      $count = $sqlResult($where)->count();
  }

}
