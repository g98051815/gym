<?php
namespace app\gym\model;
use think\exception\HttpException;
use app\common\model\Base;
use think\Loader;
use think\Validate;

class GymMembers extends Base{
    protected $table = 'gym_signing_fitness_instructor';
    protected static $inputData = [];
  /**
   *查询老板店铺里的会员列表
   * `member_id` char(64) NOT NULL COMMENT '会员的id',
   * `gym_id` char(64) NOT NULL COMMENT '健身房的id',
   *@param  $status 查询状态值
   *@param $memberId 会员id
   *@param $gymId  健身房的id
  */
  public static function member($info=[]){
    $where = [];
    $params = self::sqlParams($info,['order'=>'create_time desc','limit'=>10,'page'=>1,'field'=>false]);
    $validate = new Validate(
      [
        'gym_id'    => 'require',
      ],
      [
        'gym_id.require' => '会员编号不能为空！',
      ]
    );
    //统一验证
     if(!$validate->check($info)){
         abort(-0001,$validate->getError());
     }
     $gymid    = $info['gym_id'];
     $where = ['gym_id'=>$gymid,'status'=>1];
     $sql = self::where($where)->field($params['field'])->order($params['order'])->limit($params['limit'])->page($params['page']);
     $result = self::infoOfCache($sql);
     return $result;
  }

  

}
