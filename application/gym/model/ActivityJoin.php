<?php
namespace app\gym\model;
use app\common\model\Base;
use app\common\model\Observice;
use app\user\model\Auth;

class ActivityJoin extends Base{
  //参加活动
  protected $table = 'gym_see_activity';

  protected static $cacheTag = 'see_activity';

  protected static  $autoPushCreateTime = true; //自动添加时间


  /**
   * 参加活动
   * [joinActivity 参加活动的方法]
   * @return [type] [参加活动的方法]
   */
  public static function joinActivity(){
    self::couSave(
        [
            ['activity_id','require','活动的编号不能为空！'],
        ],
        function($input,$self){
            $dataObj = new $self();
            $allowField = ['activity_id','uuid','create_time'];
            $where = ['activity_id'=>$input['activity_id'],'uuid'=>$input['uuid']];
            //查询同样的id的情况下用户的参加情况
            if(self::where($where)->count() > 0){
                abort(-20,'用户已经成功的参加活动了！');
            }
            $input['create_time'] = time();
            $obj =  $dataObj->data($input)->allowField($allowField)->isUpdate(false)->save()?:abort(-15,'参加失败');
            //添加要通知的观察者,通知朋友圈的观察者
            $role = Auth::getRoleInfo($input['uuid']);
            switch($role){
                case 'member':
                $notifyMessage = [
                    'index_id'=>$input['activity_id'],
                    'type'=>4,
                    'uuid'=>$input['uuid'],
                    'gym_id'=>'none',
                    'fitness_id'=>'none',
                  ];
                  break;
                case 'fitness':
                $notifyMessage = [
                    'index_id'=>$input['activity_id'],
                    'type'=>4,
                    'uuid'=>$input['uuid'],
                    'gym_id'=>'none',
                    'fitness_id'=>$input['uuid'],
                  ];
                break;
                case 'boss':
                $notifyMessage = [
                    'index_id'=>$input['activity_id'],
                    'type'=>4,
                    'uuid'=>$input['uuid'],
                    'gym_id'=>'none',
                    'fitness_id'=>$input['uuid'],
                  ];
                }
              Observice::addObserivce('\\app\\circle\\model\\Circle',$notifyMessage);
              Observice::notify();
              return true;
        });
    return [];
  }

  /**
   * [info 查询参加活动的明细]
   * @return [type] [description]
   */
  public static function info($info=[]){
      if(!array_key_exists('activity_id', $info)){
          return [];
      }
      $activityId = $info['activity_id'];
      //通过单个进行查询
      $where = ['activity_id'=>$activityId];
      $result = self::where($where)->alias('see')->join('__USER_INFO__ usi','see.uuid = usi.uuid','INNER');
      $response = self::infoOfCache($result);
      return $response;
  }

}
