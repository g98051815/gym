<?php
// +----------------------------------------------------------------------
// | 会员转移
// +----------------------------------------------------------------------
// | Author ： 才华横溢
// +----------------------------------------------------------------------
namespace app\gym\model;
use think\Validate;
use think\Loader;
use app\common\model\Base;
class MemberTransfer extends Base{
    protected $table = ´´;
    protected static $inputData  = null;
    protected static $publicTool = null;

    /**
     * 会员转移，需要的参数
     * 原有教练的编号和现有教练的编号，然后控制教练转移重新签约，并且与将原有的课程时间进行转移
    */
    public static function transfer(){
        self::couSave(
            [],
            function($input,$self){

            }
        );

    }

    /**
    *sing fitness member now fetch
    */
    protected static function singMember(array $condition=[]){
	$field = ['fitness_instructor_id','member_id','gym_id','id','bought_class','rest_of_class'];
	$validate = new Validate(
	[
	  'fitness_id'=>'require', //check fitness_id is exists if is not exists buz throw error the show
	],
	[
	  'fitness_id.require'=>'fitness id is not exists',
	]
	);
	if(!$validate->check($condition)){
	  abort(-0012,$validate->getError());
	}
	$where = ['fitness_instructor_id'=>$condition['fitness_id'],'status'=>1];
	$dbObject = new self();
	$result = $dbObject->table('gym_signing_fitness_instructor')->where($where)->field($field)->select();
	$response = collection($result)->toArray();
	return $response;

   }
  /**
  *验证会员和教练的签约
  */
  protected static function checkMemberSignFitness(array $memberList=[],$fitness_id=null){
	if(empty($memberList)){
	  abort(-0013,'memberList is cant\'t exists , has one min');
	}

	if(empty($fitness_id)){
	  abort(-0014,'fitness id is cant\'t exists');
	}
	if(!is_numeric($fitness_id)){
	  abort(-0015,'fitness id toggle is not true , must be type is int');
	}
	$signingMemberList = self::signMember(['fitness_id'=>$fitness_id]);

  }

}
