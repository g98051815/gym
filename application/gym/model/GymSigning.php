<?php
namespace app\gym\model;
use app\publictool\controller\MsgTpl;
use app\common\model\Base;
use think\Loader;
/**
*abort
*健身房的签约信息
*健身房和教练的签约暂时没有这个信息
*/
/**
*表字段
*CREATE TABLE `gym_sign_gym` (
* `fitness_instructor_id` char(64) NOT NULL COMMENT '教练的标识',
* `gym_id` char(64) NOT NULL COMMENT '健身房的标识',
* `create_time` varchar(100) NOT NULL DEFAULT '0' COMMENT '签约的时间',
* `last_update_time` int(15) unsigned NOT NULL DEFAULT '0' COMMENT '最后修改的时间可以理解为解约的时间',
* `status` int(1) unsigned NOT NULL DEFAULT '1' COMMENT '健身房与教练的签约状态1:已经签约,0:已经解约',
* KEY `gym_sign_gym_fitness_instructor_id_IDX` (`fitness_instructor_id`,`gym_id`,`status`) USING BTREE
*)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='教练签约健身房'
*/
class GymSigning extends Base{
    protected $table = 'gym_sign_gym';
    protected static $cacheTag = 'gym_sing_gym'; //健身房的签约信息
    /**
    *查询健身房签约信息
    *在此表中create_time 代表签约时间
    *返回当前健身房与教练签约的信息
    *@param $gymId
    */
    public static function gymSigningInfo($gymId=null,$page=1,$limit=1,$orderBy='create_time desc'){
        if(is_null($gymId)){
           exception('没有健身房唯一编码',-0001);
        }
        $where['where']=['gym_id'=>$gymId,'status'=>1];
        $where['page'] = $page;
        $where['limit'] = $limit;
        $where['order'] = $orderBy;
        $response = self::infoOfCache($where);
        return $response;
    }

}
