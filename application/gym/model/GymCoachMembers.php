<?php
namespace app\gym\model;

use think\exception\HttpException;
use app\common\model\Base;
use think\Loader;
use think\Validate;
/**
 * [GymCoachMembers 教练会员模型]
 */
class GymCoachMembers extends Base{
  protected $table = 'gym_signing_fitness_instructor';
  protected static $inputData = [];

  /**
   *查询教练总课时和剩余课时
   *`bought_class` int(4) NOT NULL COMMENT '当前会员已经购买的课时',
   *@param  $status 查询状态值
   *@param $bought_class 总课时
  */
   public static function memberNumber($info=[]){
     $where = [];
     $params = self::sqlParams($info,['order'=>'create_time desc','limit'=>10,'page'=>1,'field'=>false]);
     $validate = new Validate(
       [
         'member_id'    => 'require',
       ],
       [
         'member_id.require' => '会员编号不能为空！',
       ]
     );
     //统一验证
      if(!$validate->check($info)){
          abort(-0001,$validate->getError());
      }
      $fitnessinstructorid    = $info['fitness_instructor_id'];
      $where = ['fitness_instructor_id'=>$fitnessinstructorid ,'status'=>1];
      $sql = self::where($where)->field($params['field'])->order($params['order'])->limit($params['limit'])->page($params['page']);
      $result = self::infoOfCache($sql);
      return $result;
   }
}
