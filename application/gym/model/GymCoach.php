<?php
namespace app\gym\model;

use think\exception\HttpException;
use app\common\model\Base;
use think\Loader;
use app\publictool\controller\MsgTpl;
use think\Validate;

class GymCoach extends Base{
   protected $table = 'gym_sign_gym';
   protected static $inputData = [];
  /**
   *查询老板店铺里的教练列表
   * `fitness_id` char(64) NOT NULL COMMENT '教练的id',
   *@param  $status 查询状态值
   *@param $fitness_id 教练的id
  */
   public static function coach($info=[]){
     $where = [];
     $validate = new Validate(
       [
         'gym_id'    => 'require',
       ],
       [
         'gym_id.require' => '教练编号不能为空！',
       ]
     );
     //统一验证
      if(!$validate->check($info)){
          abort(-0001,$validate->getError());
      }
      $sqlQuery = self::sqlParams($info,[
        'order'=>'member_count desc',
        'limit'=>10,
        'page'=>1,
        'field'=>[
          'gig.*',
          'count(gsfi.member_id) as member_count',
          'count(gsfi.rest_of_class) as rest_of_class_count',
          'usi.nick_name',
          'usi.head_figure',
          'usi.name',
          'usi.uuid',
        ],
      ]);
      $where = ['gig.gym_id'=>$info['gym_id'] ,'gig.status'=>1];
      $sqlResult = function($where,$sqlQuery){
        $result = self::where($where)->alias('gig');
        $result->join('__USER_INFO__ usi','gig.fitness_instructor_id = usi.uuid','INNER');
        //gym_signing_fitness_instructor
        $result->join('__SIGNING_FITNESS_INSTRUCTOR__ gsfi','gig.fitness_instructor_id = gsfi.fitness_instructor_id AND gsfi.status = 1','LEFT');
        return $result;
      };
      $count = $sqlResult($where,$sqlQuery)->count();
      $result = $count > 0 ? self::infoOfCache($sqlResult($where,$sqlQuery)->field($sqlQuery['field'])->limit($sqlQuery['limit'])->page($sqlQuery['page'])->order($sqlQuery['order'])->group('gig.fitness_instructor_id')) : [];
      return MsgTpl::createListTpl($result,$count,$sqlQuery['limit'],$sqlQuery['page']);
   }
}
