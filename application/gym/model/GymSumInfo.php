<?php
namespace app\gym\model;
use think\exception\HttpException;
use app\common\model\Base;
use think\Loader;
use think\Validate;
/*
老板主页下的会员管理
CREATE TABLE IF NOT EXISTS `gym_gym_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自动增长id仅用于计数',
  `store_title` varchar(80) NOT NULL COMMENT '门店名称',
  `phone_num` char(11) NOT NULL COMMENT '门店手机号码',
  `tel_num` varchar(13) NOT NULL COMMENT '门店电话',
  `address` tinytext NOT NULL COMMENT '门店地址',
  `profile` varchar(255) NOT NULL COMMENT '门店简介(最长不能超过30个字符)',
  `pictures` varchar(100) NOT NULL COMMENT '门店图片(可以使用上传多张图片)',
  `location` varchar(150) NOT NULL DEFAULT '''''' COMMENT '门店坐标信息(预留的lbs)',
  `boss` char(64) NOT NULL COMMENT '用户的唯一标识符',
  `create_time` int(20) NOT NULL COMMENT '创建的时间',
  `last_update_time` int(20) NOT NULL COMMENT '最后修改的时间',
  `unique_id` char(64) NOT NULL COMMENT '门店的唯一标识符(由系统生成后存入数据库)',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '店铺当前的状态(1:启用,2:禁用)',
  PRIMARY KEY (`id`),
  KEY `boss` (`boss`),
  KEY `id` (`id`),
  KEY `unique_id` (`unique_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COMMENT='健身房信息';
*/
class GymSumInfo extends Base{
   protected $table = 'gym_gym_info';
   protected static $inputData = [];

   /**
   *查询老板主页下店铺信息
   *@param $status 查询状态值
   *@param $storetitle 店名称
   *@param $boss   用户唯一标识
   *@param $phone     门店手机号
   */
 public static function query($info=[]){
       $where = [];
       $params = self::sqlParams($info,['order'=>'create_time desc','limit'=>10,'page'=>1,'field'=>false]);
       $validateConfig = config('gym_sum_info_query');
       $validate = new Validate(
         $validateConfig['validate'],
         $validateConfig['message']
      );
      //统一验证
       if(!$validate->check($info)){
           abort(-0001,$validate->getError());
       }
       $boss = $info['boss'];
       $where = ['boss'=>$boss,'status'=>1];
       $sql = self::where($where)->field($params['field'])->order($params['order'])->limit($params['limit'])->page($params['page']);
       $result = self::infoOfCache($sql);
       return $result;
 }

}
