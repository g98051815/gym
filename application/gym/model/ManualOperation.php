<?php
namespace app\gym\model;
use app\common\model\Base;
/**
 * [ManualOperation 手动减少或者增加课程]
 */
class ManualOperation extends Base{

  protected $table = '';

  // `id` int(15) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  // `number_of` varchar(100) NOT NULL COMMENT '数量',
  // `member_id` char(64) NOT NULL COMMENT '减少数量的会员的id',
  // `gym_id` char(64) NOT NULL COMMENT '健身房的编号',
  // `fitness_id` char(64) NOT NULL COMMENT '教练的id',
  // `number_of_pre_changes` int(15) unsigned NOT NULL COMMENT '修改前的数量',
  // `create_time` int(15) unsigned NOT NULL COMMENT '创建的时间',
  // `last_update_time` int(15) unsigned NOT NULL COMMENT '手动减少课时的数量',
  // `operation` tinyint(1) unsigned NOT NULL COMMENT '操作的方式,1:为较少,2:为增加',

  /**
   * [push 操作添加记录的流程]
   * @return [bool] [返回布尔值]
   */
  public static function push(){
    //数量验证
    return self::couSave(
      [
        ['number_of','require','修改的数量不能为空！'],
        ['member_id','require','会员的编号不能为空！'],
        ['gym_id','require','健身房的编号不能为空！'],
        ['fitness_id','require','健身教练的编号不能为空！'],
        ['number_of_pre_changes','require','修改前的数量不能为空！']
      ],
      function($inputData,$self){
          $dataObject = new self();
          $allowField = true;
          return $dataObject->data($inputData)->allowField($allowField)->save();
      }
    );
  }

  /**
   * [monity 修改]
   * @return [type] [返回Boolean内容]
   */
  public static function monity(){
    
  }


}
