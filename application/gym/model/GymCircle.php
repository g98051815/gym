<?php
namespace app\gym\model;
use app\common\model\Base;
use app\gym\model\GymSigning as gymsigning; //健身房与教练签约的内容
use app\gym\model\MemberSigning as membersing; //会员与教练签约的类
use app\circle\model\Circle;
use app\gym\model\Activity as activity;
use think\Validate;
use app\gym\model\ActivityJoin as activityjoin;
/**
 * [GymCircle 查询健身房下面所有用户的信息]
 */
class GymCircle extends Base{
  protected $cacheTa = 'gym_circle';
  /**
   * [info 查询健身房下面包括健身房自己的和健身房的下面会员的所有内容]
   * @return [type] [返回数组形式的内容]
   */
  public static function info($info=[]){

  }

  /**
   * [infoAllOfGym 查询健身房的朋友圈信息]
   * @param  array  $info [查询的条件]
   * @return [type]       [查询的内容]
   */
  public static function infoAllOfGym($gymId=null,$field='*'){
     if(empty($gymId)){abort('健身房的编号是必须的！');}
      //查询健身房下面的活动信息
      $activity = activity::info(['gym_id'=>$gymId,'limit'=>1,'order'=>'create_time desc']);
      //查询参加活动的人员
      foreach($activity as $key => &$val){
         $val['join_item'] = activityjoin::info(['activity_id'=>$val['id']]);
      }
      //查询健身房签约的教练
      $fitnessSign = GymSigning::gymSigningInfo($gymId);
      //查询教练会员下的列表
      $memberSing = membersing::fitnessMembers($gymId);
      //查询教练的列表
      $fitnessQueue = array_column($fitnessSign,'fitness_instructor_id');
      //查询教练的列表
      $memberQueue = array_column($memberSing,'member_id');
      //朋友圈的队列
      $circleQueue = array_merge($fitnessQueue,$memberQueue);
      //取出，当前健身房下面所有关联的内容
      $circle['circle_item']= Circle::info(1,1,$circleQueue,3);
      $circle['activity_item'] = $activity;
      return $circle;
  }


}
