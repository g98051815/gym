<?php
namespace app\gym\model;
use app\common\model\Base;
use think\Loader;
use think\Validate;
use think\exception\HttpException;
use think\Cache;
use app\common\model\Observice;
/**
*健身房的活动功能
*/

/**
*健身房活动的表结构
*id 活动的编号
*place 活动的地点
*date_time 活动的时间
*explain 活动的说明
*picture 活动的图片
*create_time 活动的图片
*last_update_time 活动的最后更新时间
*status 活动的状态
*name 活动的名称
*/
class Activity extends Base{
    //健身房的活动
    protected   $table = 'gym_gym_activity';

    protected  static $cacheTag = 'activity'; //缓存的名字

    protected static  $autoRefreshCache=true; //增加，修改，删除，自动更新缓存

    protected static  $autoPushCreateTime = true; //自动添加时间

    protected static  $autoPushUpdateTime = true; //自动添加修改时间

    /**
    *健身房活动的表结构
    *id 活动的编号
    *place 活动的地点
    *date_time 活动的时间
    *explain 活动的说明
    *pictures 活动的图片
    *create_time 活动的图片
    *last_update_time 活动的最后更新时间
    *status 活动的状态
    *name 活动的名称
    */
    //活动添加
    public  static function newAdd(array $data=[]){
      $inputData = self::getInputData(); //获取当前的所有字段
  // CREATE TABLE `gym_gym_activity` (
//   `id` int(15) unsigned NOT NULL AUTO_INCREMENT COMMENT '活动的序列id',
//   `locaton` varchar(100) NOT NULL DEFAULT '' COMMENT '活动的地点',
//   `location_code` varchar(100) NOT NULL DEFAULT '' COMMENT '活动的位置代码，主要用于地图使用'
//   `date_time` int(15) unsigned NOT NULL DEFAULT '0' COMMENT '活动的开始时间',
//   `over_time` int(15) unsigned NOT NULL COMMENT '活动结束时间',
//   `gym_id` varchar(64) NOT NULL COMMENT '健身房的编号,可以选择是一个健身房还是多个健身房,或者是全部的健身房,使用all进行代表',
//   `explain` text NOT NULL COMMENT '活动的说明',
//   `uuid` varchar(64) NOT NULL COMMENT '发布人',
//   `pictures` varchar(200) NOT NULL DEFAULT '' COMMENT '活动的图片',
//   `create_time` int(15) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
//   `last_update_time` int(15) unsigned NOT NULL DEFAULT '0' COMMENT '最后修改的时间',
//   `status` int(1) unsigned NOT NULL DEFAULT '1' COMMENT '活动的状态1:为开启0为关闭',
//   `title` varchar(100) NOT NULL DEFAULT '' COMMENT '活动名称',
//   `time_signing_up` int(15) unsigned NOT NULL COMMENT '活动开始报名的时间',
//   `registration_time` int(15) unsigned NOT NULL COMMENT '活动报名结束的时间',
//   PRIMARY KEY (`id`),
//   KEY `gym_gym_activity_id_IDX` (`id`,`title`,`date_time`) USING BTREE,
//   KEY `gym_gym_activity_uuid_IDX` (`uuid`) USING BTREE
// ) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COMMENT='健身房活动表'
//必须填写的字段  date_time 活动的开始时间 ,
//必须填写的字段  over_time 活动的结束时间
//必须填写的字段  gym_id 活动是属于哪一个健身房下的
//必须填写的字段  uuid 活动的发布人
//必须填写的字段  title 活动的名称
//必须填写的字段  location 活动的位置
//必须填写的字段  location_code 活动的位置代码
//必须填写的字段  explain 活动的说明
//必须填写的字段  time_signing_up 活动开始报名的时间
//必须填写的字段  registration_time 活动结束报名的时间
        $allowField = [];
        //验证内容
        $validate = new Validate(config('activity_add'));
        if(!$validate->check($inputData)){
            abort(-0002,$validate->getError());
        }
        $data['create_time'] = time(); //创建的时间
        $data['last_update_time'] = time(); //最后修改的时间
        $dataObject =new self();
        $add = $dataObject->data($inputData)->allowField(true)->isUpdate(false)->save();
        if(!$add){
           abort(-3,'添加失败请重试！');
           return;
        }
        //添加要通知的观察者,通知朋友圈的观察者
        $notifyMessage = [
            'index_id'=>$dataObject->getLastInsID(),
            'type'=>4,
            'uuid'=>'none',
            'gym_id'=>$inputData['gym_id'],
            'fitness_id'=>'none',
          ];
          Observice::addObserivce('\\app\\circle\\model\\Circle',$notifyMessage);
          Observice::notify();
        return [];
    }

    /**
     * [addActivityPicture 添加活动结束后的图片]
     * 活动如果没有开始是不可以填入活动的图片的，
     * 活动开始之后可以填入活动的图片，和活动结束后的图片
     */
    public function addActivityPicture(){



    }



    /**
     * 获取活动的开始时间
     */
    public static function getActivityDateTime(){
        return self::getInputData('date_time');
    }



    /**
    *活动检索
    *几种搜索情况
    *1.使用id进行搜索
    *3.使用创建的用户进行搜索
    *4.使用加入活动的用户进行搜索
    *@param $info 信息
    **/
    public static function info(array $info=[]){
        $where = [];
        $response =''; //检索出的返回值
        $key = null;
        //是否为健身房的id
        if(!empty($info)){

            if(array_key_exists('id',$info)){
                $where['where'] = ['id'=>$info['id']];
                $response = self::infoOfCache($where);
                //查询当前参加活动的人数
                if(!array_key_exists('details',$info)){
                    //是否需要详情列表
                    $response['join_count'] = db('see_activity')->where(['activity_id'=>$info['id']])->count();
                }else{
                    $response['join_list'] = db('see_activity')->where(['activity_id'=>$info['id']])->alias('sa')->join('__USER_INFO__ usi','sa.uuid = usi.uuid','INNER')->select();
                }
            }
            //判断是否为发布人进行搜索列表
            if(array_key_exists('uid',$info)){
                $where['where'] =['uuid'=>$info['uid'],'status'=>1];
                $response = self::infoOfCache($where);
            }
            //使用加入活动的用户进行搜索 获取的是列表信息,根据加入的用户进行反差活动列表，这样可以获得用户的内容
            if(array_key_exists('join_activice_uuid',$info)){
                $where = ['sa.uuid'=>$info['join_activice_uuid']];
                $response = self::infoOfCache(self::table('gym_see_activity')->alias('sa')->where($where)->join('__GYM_ACTIVITY__ ga','sa.activity_id = ga.id','INNER')->order('sa.create_time desc'));
            }
            //使用健身房的id搜索活动的详情
            if(array_key_exists('gym_id',$info)){
                $sqlQuery = self::sqlParams($info,[
                  'field'=>true,
                  'limit'=>10,
                  'page'=>1,
                  'order'=>'create_time desc'
                ]);
                $where = ['gym_id'=>$info['gym_id'],'status'=>1];
                $sql = self::where($where)->limit($sqlQuery['limit'])->order($sqlQuery['order'])->field($sqlQuery['field']);
                $response = self::infoOfCache($sql);
                foreach($response as $key => &$val){
                    $val['join_count'] = db('see_activity')->where(['activity_id'=>$val['id']])->count();
                }
            }else if(!empty($info) && is_numeric(key($info))){
                $where ['where']= ['id'=>key($info)];
                $response = self::infoOfCache($where);
           }
        }else{
            abort(-0004,'参数不正确请检查！');
        }
        return $response;
    }


//    /**
//     * [cachePolicy 缓存策略]
//     * @param  [array] $response [查询到的缓存内容]
//     * @return [void]           [true|false]
//     */
//    protected static function cachePolicy(&$response){
//        //数组递归
//        $dataObject = new self();
//        $return = false;
//        if(count($response) < count($response,true)){
//              $saveAll = [];
//              foreach($response as $key => $val){
//                  if($val['over_time'] < time()){
//                      //批量修改为已经关闭
//                      $dataObject->data(['id'=>$val['id'],'status'=>0],true)->isUpdate(true)->save();
//                      unset($response[$key]);
//                      if(false === $return){$return = true; }
//                  }
//              }
//        }else if($response['over_time'] < time()){
//            $dataObject->data(['id'=>$response['id'],'status'=>0],true)->isUpdate(true)->save();
//            $response = [];
//            $return = true;
//        }
//
//        return $return;
//    }





    //活动修改
    public static function monity(){
        $data = self::getInputData();
        //允许修改的值
        $allowField = [];
        //dump($data);exit;
        //验证内容
        $validate = new Validate(
        [
            'id'=>'require',
        ],
        [
            'id.require'=>'活动编号必须存在！',
        ]
        );
        if(!$validate->check($data)){
            //弹出错误窗口！
            throw new HttpException(-0006,$validate->getError());
        }
        $where = ['id'=>$data['id']];
        $dataObject = new self();
        $monity = $dataObject->allowField(true)->isUpdate(true)->save($data,$where);
        if(!$monity){
            abort(-0005,'信息操作失败请重试！');
        }
        return true;
    }

    //活动删除，默认为软删除
    public function falseDelete(array $data=[]){
        $data['status'] = 0;
        $result = $this->monity($data);
        return true;
    }

}
