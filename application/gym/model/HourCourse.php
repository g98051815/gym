<?php
namespace app\gym\model;
use app\common\model\Base;
use app\gym\model\GymInfo;
use app\common\model\Observice;
/**
 * 教练对于课程的操作
 */
class HourCourse extends Base{

    protected $table = 'gym_hour_course';
    //信息模板
    protected static $msgTpl;

    protected static  $autoPushCreateTime = true; //自动添加时间

    protected static  $autoPushUpdateTime = true; //自动添加修改时间

    protected static $memberSigning;

    protected function __initialize(){
      self::init();
    }
    /**
     * [push 添加数据的方法]
     * @return [bool] [返回布尔类型的值]
     */
    protected static function init(){
        parent::init();
        self::$msgTpl = new \app\publictool\controller\MsgTpl();
        self::$memberSigning = new \app\gym\model\MemberSigning();
    }
    //  `id` int(15) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
    //   `fitness_id` char(64) NOT NULL COMMENT '教练的id',
    //   `member_id` char(64) NOT NULL COMMENT '会员的id',
    //   `hours` int(15) NOT NULL COMMENT '购买的课时数量,或减少的课程时间',
    //   `gym_id` char(64) NOT NULL COMMENT '健身房的编号',
    //   `create_time` int(15) unsigned NOT NULL COMMENT '创建的时间',
    //   `note` varchar(255) NOT NULL COMMENT '增加或者减少的原因',
    //   `option` tinyint(1) unsigned NOT NULL COMMENT '操作：1:增加,2:减少',
    //   `option_before_hours` int(5) NOT NULL COMMENT '操作前的课程数量',
    //   `last_update_time` int(15) unsigned NOT NULL COMMENT '最后的修改时间
    public static function push(){
        //添加的数据
        self::couSave(
          [
          ['member_id','require','会员:的编号不能是空的！'],
          ['hours','require|number',':操作课程的时间不能为空！|操作课程的时间不能为空！'],
          ['gym_id','require',':健身房的编号不能为空！'],
          ['option','require|number',':必须填写操作的原因！| option必须是数字'],
          ['option_before_hours','require','必须填写操作前的数量'],
        ],
        function($input,$self){
            $allowField = true;
            $dataObject = new $self();
            $rel = $dataObject->data($input)->allowField($allowField)->isUpdate(false)->save();
            $lastId = $dataObject->getLastInsID();
            self::$memberSigning->setInputData($input);
            self::$memberSigning->memberHoursOption();
              //通知会员
              Observice::addObserivce('\\app\\user\\model\\AlertsMsg',
              ['addressee'=>$input['member_id'], //收信人
              'addresser'=>$input['fitness_id'], //发信人
              'type'=>5, //通知类型
              'title'=>'课时修改', //内容
              'index_id'=>$lastId]); //索引id
              //通知教练
              Observice::addObserivce('\\app\\user\\model\\AlertsMsg',
              ['addressee'=>$input['fitness_id'], //收信人
              'addresser'=>$input['fitness_id'], //发信人
              'type'=>5, //通知类型
              'title'=>'课时修改', //内容
              'index_id'=>$lastId]); //索引id
              //通知老板
              Observice::addObserivce('\\app\\user\\model\\AlertsMsg',
              ['addressee'=>GymInfo::info(['unique_id'=>$input['gym_id']])['boss'], //收信人
              'addresser'=>$input['fitness_id'], //发信人
              'type'=>5, //通知类型
              'title'=>'课时修改', //内容
              'index_id'=>$lastId]); //索引id
              //教练与学员进行约课需要通知教练一声
              //教练与学员进行约课需要通知教练一声
              Observice::notify(); //通知教练
            return true;
        }
      );
      return [];
    }


    public static function info($condition=[]){

        if(!array_key_exists('option',$condition)){

              abort(-30,'缺少必要的参数！');

        }

        $sqlParams = self::sqlParams($condition,['field'=>true,'limit'=>10,'page'=>1,'order'=>'create_time desc']);

        $sqlQuery = function($condition,$sqlParams){

          switch($condition['option']){
                case 1:
                //查询增加的操作
                  $where['option'] = 1;
                  break;
                case 2:
                //查询减少的操作
                  $where['option'] = 2;
                  break;
          }
          //会员的编号
          $where['member_id'] = $condition['member_id'];
          $where['fitness_id'] = $condition['fitness_id'];
          $where['type_status'] = 1;
          $result = self::where($where)->field($sqlParams['field']);
          $result->order($sqlParams['order']);
          $result->limit($sqlParams['limit']);
          $result->page($sqlParams['page']);
          return $result;

        };
        //统计总数
        $count = $sqlQuery($condition,$sqlParams)->count();
        $response = self::infoOfCache($sqlQuery($condition,$sqlParams));
        return self::$msgTpl->createListTpl($response,$count,$sqlParams['limit'],$sqlParams['page']);
    }


    public static function infoItem($condition=[]){

        $condition = ['id'=>$condition['id']];
        $sqlQuery = function($condition){
          $result = self::where($condition);
          return $result;
        };
        $response = self::infoOfCache($sqlQuery($condition));
        $responseItem = &$response[0];
        $responseItem["fitness_info"] = db('user_info')->field(['nick_name','name'])->where(["uuid"=>$responseItem['fitness_id']])->find();
        $responseItem["member_info"] = db('user_info')->field(['nick_name','name'])->where(["uuid"=>$responseItem['member_id']])->find();
        return $responseItem;
    }

}
