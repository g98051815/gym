<?php
namespace app\gym\model;
use app\common\model\Base;
use app\publictool\controller\MsgTpl;
use think\exception\HttpException;
use think\Validate;
use app\publictool\controller\PublicTool;
use app\gym\model\GymSigning;
use think\Loader;
/**
*如下是建设放的字段信息
*
*/
class GymInfo extends Base{
    protected $table = 'gym_gym_info';
    protected static  $cacheTag='gym_info'; //缓存的名称,关闭使用缓存直接使用false
    protected static  $autoPushCreateTime = true; //自动添加时间
    protected static  $autoPushUpdateTime = true; //自动添加修改时间
    protected $searchParam = ['phone_num','boss','tel_phone','store_title'];
    /**
    *获取当前健身房的信息
    *此方法具有两种功能，直接获取健身房的对象信息，第二个是获取健身房的列表信息，第三个是获取健身房的单个信息
    *此方法的三种接口方式使用策略模式进行开发最为适宜
    *1.使用健身房门店的手机号码信息可以获取关键的信息，信息格式为 ['phone_num'=>'手机号码']
    *2.默认情况下直接填写unique_id的查询健身房的信息
    *3.也可以使用老板的名称对健身房的信息进行筛选 信息格式为 ['boos'=>'老板的名称']
    *4.可以使用门店的座机电话对健身房的信息进行筛选 ['tel_phone'=>'门店的座机号码']
    *5.可以使用健身房门店的名称对健身房进行筛选 ['store_title'=>'门店的名称']
    *@param $info 健身房的信息
    *@return array|(array[array])表示二维数组
    */
    public static function info($info=null){
        $where='';
        $response = '';
        $page = 0;//赋值page删除page的页面
        if(!is_array($info)){abort(-0020,'您传入的信息不正确必须填写数组信息！');}
        if(!empty($info['page'])){$page = $info['page'];   unset($info['page']);}
        $searchKey = ['unique_id','phone_num','boss','tel_phone','store_title','signing','fitness_id'];
        //判断是否传入参数
        if(empty($info) || empty(array_intersect($searchKey,array_keys($info)))){
             abort(-0025,'请填写您要查询的信息');
        }
        //判断是否为多个参数传入
        if(is_array($info)){
                 //判断传入的是否是门店的id
            if(array_key_exists('unique_id',$info)){
                $where = ['unique_id' => $info['unique_id'],'status'=>1];
                if(!in_array('list',$info)){
                  $response =  self::where($where)->find();
                  if(!empty($response)){
                     return $response = $response->toArray();

                   }
                }
            }

                //永手机号码的方式进行查询['返回一维数组的对象信息']
            if(array_key_exists('phone_num',$info)){
                 $where = ['phone_num'=>$info['phone_num'],'status'=>1];
            }

            //用老板的名称进行查询['返回二维数组的对象列表信息因为一个老板可能游多个门店']
            if(array_key_exists('boss',$info)){
                 $where = ['boss'=>$info['boss'],'status'=>1];
            }

            //使用门店的电话号码进行查询['返回一维数组的对象列表信息']
            if(array_key_exists('tel_phone',$info)){
                $where = ['tel_phone'=>$info['tel_phone'],'status'=>1];
            }

            //使用门店的名称进行查询['返回二维数组的对象列表信息']
            if(array_key_exists('store_title',$info)){
                $where = ['store_title'=>['like','%'.$info['store_title'].'%']];
            }

            //使用过滤object
              //查询健身房当前签约的教练信息
            if(array_key_exists('signing',$info)){
                    //引入健身房的签约查询
                    $loader = Loader::model('\\app\\gym\\model\\GymSigning');
                    GymSigning::setGymId(1);
                    $result = GymSigning::gymSigningInfo();
                    exit;
            }
            //通过教练查询健身房
            if(array_key_exists('fitness_id',$info)){
              // `fitness_instructor_id` char(64) NOT NULL COMMENT '教练的标识',
              //   `gym_id` char(64) NOT NULL COMMENT '健身房的标识',
              //通过教练查询健身房
              $where = ['fitness_instructor_id'=>$info['fitness_id'],'status'=>1];
              $field = true;
              $response  = self::table('gym_sign_gym')->where($where)->select();
              return $response;
            }
        }
        $sqlQuery = self::sqlParams($info,['field'=>true,'limit'=>10,'page'=>1]);
        $sqlResult = function($where,$query){
            return self::where($where);
        };
        $count = $sqlResult($where,$sqlQuery)->count();
        $response = $count > 0?collection($sqlResult($where,$sqlQuery)->limit($sqlQuery['limit'])->field($sqlQuery['field'])->page($sqlQuery['page'])->select())->toArray():[];
        //返回查询的数据
        return  MsgTpl::createListTpl($response,$count,$sqlQuery['limit'],$sqlQuery['page']);

    }


    //查询健身房和健身房的过期时间
    public static function gymAllExport( $info=[] ){

      $where = true;

      if(array_key_exists('store_title',$info)){

          $where = ['gi.store_title'=>$info['store_title']];

      }

      if(array_key_exists('phone_num',$info)){

          $where = ['gi.phone_num'=>$info['phone_num']];

      }

      if(array_key_exists('be_overdue',$info)){
          if($info['be_overdue']!== "0"){
            //设置的过期时间没有缴纳费用的
            $overide = time()-($info['be_overdue']*(60*60*24));
            if (is_bool($where)){
                $where= ['be_overdue'=>["<=",$overide]];
            }else{
              $where['be_overdue'] = ["<=",$overide];
            }
          }
      }

      $sqlQuery = self::sqlParams($info,[
        'field'=>[
          'gi.store_title',
          'gi.unique_id',
          'cr.create_time',
          'cr.beoverdue',
          'gi.boos'
        ],
        'limit'=>10,
        'page'=>1,
        'order'=>'cr.create_time desc',
      ]);

      $sqlResult = function($where){

          return self::where($where)->alias('gi')->distinct(false)->join('__CONTRIBUTION_RECORD__ cr','gi.unique_id = cr.gym_id','LEFT');

      };

      $count = $sqlResult($where)->count();
      $result = $count > 0? collection($sqlResult($where)->limit($sqlQuery['limit'])->page($sqlQuery['page'])->order($sqlQuery['order'])->group("gi.unique_id")->select())->toArray():[];

      return  MsgTpl::createListTpl($result,$count,$sqlQuery['limit'],$sqlQuery['page']);

    }


    /**
     * [getAllGyms 获取老板下面所有的健身房信息]
     * @param  [type] $boosId [老板的id]
     * @return [type]         [获取老板下面所有的健身房信息]
     */
    public static function getAllGyms($info=[]){
      $where = ['boss'=>$info['boss']];
      $params = self::sqlParams($info,['field'=>'unique_id','limit'=>1000,'page'=>1,'order'=>'create_time desc']);
      $result = self::where($where);
      $result = false === $params['limit']? $result : $result->limit($params['limit']);
      $result = false === $params['page']? $result : $result->page($params['page']);
      $result = $result->order($params['order']);
      $result = $result->field($params['field']);
      $response = self::infoOfCache($result);
      return $response;
    }

    /**
     * [courseInfo 搜索所有店面的约克信息]
     * @return [type] [description]
     */
    public static function courseCountInfo($info){
        //通过老板的编号查询
        if(array_key_exists('boss',$info)){

            $where = ['gi.boss'=>$info['boss']];

        }
        //通过健身房的编号修改
        if(array_key_exists('unique_id',$info)){

            $where = ['gi.unique_id'=>$info['unique_id']];

        }

        $sqlQuery =self::sqlParams($info,
        [
          'field'=>[
              'gi.store_title',
              'gi.unique_id',
              'usi.nick_name',
              'usi.name',
              'co.fitness_id',
              'usi.head_figure',
              'count(co.member_id) as course_hour_count'
          ],
          'limit'=>10,
          'page'=>1,
          'order'=>'count(co.member_id) desc'
        ]);
        $sqlResult = function($where,$sqlQuery,$info){
            $result = self::where($where)->alias('gi');
            $result->join('__COURSE__ co','gi.unique_id = co.gym_id','LEFT');
            $result->join('__USER_INFO__ usi','co.fitness_id = usi.uuid','LEFT');
            if(array_key_exists('today',$info)){
                switch ($info['today']) {
                  case 1:
                    $result->whereTime('co.begin_time','d');
                    break;
                  case 2:
                  $result->whereTime('co.begin_time','+1d');
                    break;
                }

              if(array_key_exists('gym',$info)){
                  $result->group('co.gym_id');
              }
            }
            return $result;
        };
        $count = $sqlResult($where,$sqlQuery,$info)->count();
        $result = $count > 0? self::infoOfCache($sqlResult($where,$sqlQuery,$info)->limit($sqlQuery['limit'])->field($sqlQuery['field'])->page($sqlQuery['page'])->order($sqlQuery['order'])) : [];
        return MsgTpl::createListTpl($result,$count,$sqlQuery['limit'],$sqlQuery['page']);
    }

    /**
     * [input   输入健身房的信息]
    *@param $info 健身房的信息
    *@param $field 要查询用户的什么数据
    *@param $page 要查询用户的第几页数据[只有在列表的情况下才会返回]
    * @return $info;
    */
    public static function input(){
        $info = self::getInputData();
        //允许添加的数据
        $allowField = ['store_title','phone_num','tel_num','address','profile','picture','boss','create_time','last_update_time'];
        $validate = new Validate(
            [
                'store_title'=>'require',//|token
                'phone_num'=>'require',
                'tel_num'=>'require',
                'address'=>'require',
                'profile'=>'require',
                'pictures'=>'require',
            ],
            [
                'store_title.require'=>'门店名称必须填写',
                'phone_num.require'=>'手机号码必须填写',
                'tel_num.require'=>'门店联系方式必须填写',
                'address.require'=>'门店的联系地址必须填写',
                'profile.require'=>'门店的简介必须填写',
                'pictures.require'=>'门店的图片必须上传',
            ]
        );
        $checkInfo = $validate->check($info);
        if(!$checkInfo){throw new HttpException(-0010,$validate->getError());}
        $time = time();
        $info['create_time'] = $time;
        $info['last_update_time']=$time;
        $info['boss'] = $info['uuid'];
        $info['unique_id'] = PublicTool::createUniqueId();
        $data = new self();
        $allowField = true;
        $save = $data->data($info)->allowField($allowField)->isUpdate(false)->save();
        if(!$save){
            throw new HttpException(-1002,'新增门店失败请重试！');
        }
        return $info;
    }

    /**
    *修改健身房的信息
    *可以修改的内容信息
    *1.数据的状态不能是禁用的状态
    *2.数据不能是不存在的值
    *3.用户必须具备修改的权限
    *@param $info 修改健身房的信息
    */
    public static function modify(){
        $info = self::getInputData();
        $allowField = []; //允许修改的数据库内容
        $validate = new Validate(
        [
            'boss'=>'require',
            'unique_id'=>'require'
        ],
        [
            'boss.require'=>'老板的id是必须的！',
            'unique_id.require'=>'健身房的id是必要信息！'
            ]
        );//验证数据内容

        if(!$validate->check($info)){
            throw new HttpException(-0005,$validate->getError());
        }
       if(empty($info)){throw new HttpException(-0002,'参数错误请重新尝试！');}
        $where['unique_id']=$info['unique_id'];//健身房的唯一标识符
        $gymInfo = self::where($where)->field(false)->find();
       if(empty($gymInfo)){
           throw new HttpException(-0006,'健身房不存在！');
       }
       if(!in_array($info['boss'],config('gym.modfiy_rule_out_group'))){
            if(!config('gym.gym_modify_state_on')){
                if($gymInfo->getAttr('status') !== 1){
                    throw new HttpException(-0001,'用户属于禁用状态不能修改');
                }
            }
            if(strcmp($gymInfo->getAttr('boss'),$info['boss']) !== 0){
                 throw new HttpException(-0003,'此店铺不再您的名下所以您不能修改');
            }
       }

       //修改店铺的内容
       $dataObj = new self();
       $motify = $dataObj->allowField(true)->save($info,['unique_id'=>$info['unique_id']]);
       if(!$motify){
            throw new HttpException(-0004,'修改失败！');
       }
            return [];
    }

    /**
    *删除健身房的信息
    *此删除中使用的
    *@param $softDelete 是否使用软删除健身房的信息
    */
    public static function undate(array $data=[]){
        $data['status'] = 0;
        self::modify($data);
    }

}
