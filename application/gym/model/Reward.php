<?php
namespace app\gym\model;
use think\Validate;
use app\common\model\Observice;
use app\common\model\Base;
use app\user\model\Auth;

class Reward extends Base{

  protected $table = 'gym_certificate_of_merit';

  protected static  $cacheTag='certificate_of_merit'; //缓存的名称,关闭使用缓存直接使用false

  protected static  $autoRefreshCache=true; //增加，修改，删除，自动更新缓存

  protected static  $autoPushCreateTime = true; //自动添加时间

  /**
   * [info description]
   * @param  [type] $info [description]
   * @return [type]       [description]
   */
  public static function info($info=[]){

      $validate = new Validate(
        [
          'id'=>'require'
        ],
        [
          'id.require'=>'IndexId不能是空的！'
        ]
      );
      if(!$validate->check($info)){
          abort(-25,$validate->getError());
      }
      $where = ['re.id'=>$info['id']];
      $result = self::where($where)->field(['re.*','gi.store_title'])->alias('re')->join('__GYM_INFO__ gi','re.gym_id = gi.unique_id','INNER');
      $response = self::infoOfCache($result);
      return $response;
  }


  /**
   *奖励添加
   */
  public static function push(){
      self::couSave([
        ['picture','require','奖状的图片不能为空！'],
        ['sender_id','require','发奖人不能为空！'],
        ['recipient_id','require','收奖人不能为空！'],
        ['gym_id','require','健身房的编号不能为空！'],
        ['full_name','require','获奖人的全名不能为空！'],
        ['award_speech','require','颁奖词不能为空！'],
        ['more_rawrads','require','更多的奖励内容！'],
      ],function($input,$self){
          $role = Auth::getRoleInfo($input['recipient_id']);
          $dataObj = new $self();
          $result = $dataObj->data($input)->allowField(true)->isUpdate(false)->save();
          if($result){
            switch($role){
                case 'member':
                $notifyMessage = [
                    'index_id'=>$dataObj->getLastInsID(),
                    'type'=>2,
                    'uuid'=>$input['recipient_id'],
                    'gym_id'=>$input['gym_id'],
                    'fitness_id'=>db('signing_fitness_instructor')->where(['member_id'=>$input['recipient_id']])->field(['fitness_instructor_id'])->find()['fitness_instructor_id'],
                  ];
                  break;
                case 'fitness':
                $notifyMessage = [
                    'index_id'=>$dataObj->getLastInsID(),
                    'type'=>2,
                    'uuid'=>$input['recipient_id'],
                    'gym_id'=>$input['gym_id'],
                    'fitness_id'=>$input['recipient_id'],
                  ];
                break;
                case 'boss':
                $notifyMessage = [
                    'index_id'=>$dataObj->getLastInsID(),
                    'type'=>2,
                    'uuid'=>$input['recipient_id'],
                    'gym_id'=>$input['gym_id'],
                    'fitness_id'=>$input['recipient_id'],
                  ];
                }
                Observice::addObserivce('\\app\\circle\\model\\Circle',$notifyMessage);
                Observice::notify();
          }
          return $result;
      });
      return [];
  }

}
