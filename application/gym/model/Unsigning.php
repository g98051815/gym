<?php
namespace app\gym\model;
use app\common\model\Base;
use app\common\model\Observice;
class Unsigning extends Base{

  protected $table = 'gym_unsiging';

  public static function push(){
    self::couSave(
      [],
      function($input,$self){
          //添加用户的申请界面
          $allowField = true;
          $dataObject = new $self();
          //找到会员是否曾经和教练进行过签约
          $response = $dataObject->data($input)->allowField($allowField)->isUpdate(false)->save();
          $indexId = $dataObject->getLastInsID();
          $notifyFitness=[
           'addressee'=>self::getInputData('fitness_id'),
           'title'=>'解约通知',
           'type'=>4,
           'index_id'=>$indexId,
           'addresser'=>'5b4bbdfc19b57837fa05661c67a0ce765c3d277f',//系统通知用户
          ];

          $notifyMember=[
           'addressee'=>self::getInputData('member_id'),
           'title'=>'解约通知',
           'type'=>4,
           'index_id'=>$indexId,
           'addresser'=>'5b4bbdfc19b57837fa05661c67a0ce765c3d277f',//系统通知用户
          ];

          Observice::addObserivce('\\app\\user\\model\\AlertsMsg',$notifyFitness);
          Observice::addObserivce('\\app\\user\\model\\AlertsMsg',$notifyMember);
          Observice::notify();
          return $response;
      }
    );

  }



  /**
   * [info 使用id查询申请的信息]
   * 一般是使用内部的通知接口进行访问的
   * @return [type] [返回的信息]
   */
  public static function info($info=[]){
      if(!array_key_exists('id',$info)){
          return [];
      }
      $where = ['id'=>$info['id']];
      $sqlQuery = self::sqlParams($info,[
        'field'=>true,
      ]);
      $sqlResult = function($where){
          return self::where($where);
      };
      $count = $sqlResult($where)->count();
      if($count > 0){
        $result = $sqlResult($where)->field($sqlQuery['field'])->find()->toArray();
        $result['fitness_info'] = db("user_info")->where(['uuid'=>$result['fitness_id']])->field(['head_figure','name','nick_name'])->find();
        $result['member_info'] = db("user_info")->where(['uuid'=>$result['member_id']])->field(['head_figure','name','nick_name'])->find();
        $result['oper_info'] = db("user_info")->where(['uuid'=>$result['oper_id']])->field(['head_figure','name','nick_name'])->find();
      }else{
        $result = [];
      }

      return $result;
  }


}
