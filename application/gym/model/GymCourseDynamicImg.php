<?php
namespace app\gym\model;
use app\common\model\Base;
use think\Validate;
class GymCourseDynamicImg extends Base{

  protected $table = 'gym_course_dynamic_img';

  protected static  $autoRefreshCache=true; //增加，修改，删除，自动更新缓存

  protected static  $autoPushCreateTime = true; //自动添加时间

  protected static  $autoPushUpdateTime = true; //自动添加修改时间

  protected static  $cacheTag = 'gym_course_dynamic_img_'; //缓存的标签

  /**
   * `id` int(15) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
   *`course_dynamic_id` int(15) unsigned NOT NULL COMMENT '课程动态编号',
   *`img_url` blob NOT NULL COMMENT '图片地址',
   *`create_time` int(15) unsigned NOT NULL COMMENT '创建时间',
   *`last_update_time` int(15) unsigned NOT NULL COMMENT '最后修改时间',
   * @return [type] [description]
   */

   /**
    * [initial 如果存在那么就进行更改]
    * @return [type] [bool]
    */
  public static function initial(){
    $inputData = self::getInputData();
	if(array_key_exists('id',$inputData)){
		return self::monity();
	}

	if(array_key_exists('course_dynamic_id',$inputData)){
		return self::push();
	}
  }

  public static function push(){
    self::couSave(
        [
          ['course_dynamic_id','require','课程动态不能为空！'],
          ['img_url','require','图片地址不能为空！']
        ],
        function($inputData,$self){
          $dataObject = new $self();
          return $save = $dataObject->data($inputData)->isUpdate(false)->allowField(true)->save();
        });
    return [];
  }

  /**
   * [monity 修改内容模型]
   * @return [type] [修改内容模型]
   */
   public  static function monity(){
      self::couSave(
        [
          ['id','require','课程动态不能为空！'],
          ['img_url','require','图片地址不能为空！']
        ],
      function($inputData,$self){
        $dataObject = new $self();
        $where = ['id'=>$inputData['id']];
        $allowField = ['img_url'];
        return $save = $dataObject->allowField($allowField)->save($inputData,$where);
      });
       return [];
  }


  /**
   * [info 信息检索]
   * @param  [array] $info [检索信息]
   * @return [array]       [返回一个数组]
   */
  public static function info(array $info=[]){
     if(array_key_exists('course_dynamic_id', $info)){
        $where = ['course_dynamic_id'=>$info['course_dynamic_id']];
        $sql = self::where($where)->field(true)->order('create_time desc');
     }else{
       abort(-7,'参数不足无法索引！');
     }
      $response = self::infoOfCache($sql);
      return $response;
  }
}
