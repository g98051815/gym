<?php
/**
*设置当前用户组的角色
*系统根据这个文件区分组别
*/
return [
    'boos'=>1, //老板组的id
    'member'=>2,//会员组的id
    'fitness_trainer'=>3,//教练组的id
];