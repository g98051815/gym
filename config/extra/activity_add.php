<?php
/**
 * [return 活动添加验证信息]
 * @var [type]
 */
return   [
      'time_signing_up'=>function($val,$rule){
        $now = time();
        $dateTime = !empty($rule['date_time'])?$rule['date_time']:'';
        if(empty($val)){
          return '报名开始时间必须填写！';
        }
        if(!\app\common\model\Validate::timeStamp($val)){
            return '报名开始时间的格式不正确';
        }
        if($val >= $dateTime && !empty($dateTime)){
            return '报名开始时间必须小于活动开始时间！';
        }
        //目前不会检测选中时间是否具有同样的活动
        return true;
      },
      'registration_time'=>function($val,$rule){
        $now = time();
        $timeSigningUp = $rule['time_signing_up']; //报名开始时间
        $dateTime = !empty($rule['date_time'])?$rule['date_time']:'';

        if(empty($val)){
          return '报名结束时间必须填写！';
        }
        if(!\app\common\model\Validate::timeStamp($val)){
            return '报名结束时间的格式不正确';
        }
        if($val >= $dateTime && !empty($dateTime)){
            return '报名结束时间必须小于活动开始时间！';
        }
        if($val <= $timeSigningUp && !empty($timeSigningUp)){
           return '报名结束时间必须大于报名开始时间！';
        }
        //目前不会检测选中时间是否具有同样的活动
        return true;
      },
      'date_time'=>function($val,$rule){
          //活动的时间不能小于当前的时间
          $now = time();
          if(empty($val)){
            return '活动开始时间必须填写！';
          }
          if(!\app\common\model\Validate::timeStamp($val)){
              return '活动开始的时间的格式不正确';
          }
          if($val < $now){
              return '活动开始时间必须大于当前的时间';
          }
          //目前不会检测选中时间是否具有同样的活动
          return true;
      },
      'over_time'=>function($val,$rule){
          $now = time();
          $dateTime = !empty($rule['date_time'])?$rule['date_time']:'';

          if(empty($val)){
              return '活动结束时间必须填写！';
          }
          if(!is_numeric($val)){
              return '活动结束时间的格式不正确！';
          }
          if($val <= $dateTime && !empty($dateTime)){
              return '活动结束的时间必须大于活动开始的时间！';
          }
          return true;
      },
      'location'=>function($val,$rule){
        if(empty($val)){
          return '活动地点必须添加！';
        }
        return true;
      },
      'location_code'=>function($val,$rule){
            //目前只考虑单个经度的问题
            if(empty($val)){
                return '坐标必须填写！';
            }
            //经度坐标正则表达式
            //$locationCodePatten = '/^[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?)$/';
            //暂时不考虑经纬度的验证
            return true;

      },
      'gym_id'=>function($val,$rule){
        if(empty($val)){
            return '健身房的编号不能为空！';
        }
        return true;
      },
      'explain'=>function($val,$rule){
          if(empty($val)){
             return '活动说明必须填写！';
          }
          if(!is_string($val)){
            return '活动说明格式不正确！';
          }
          return true;
      },
      'uuid'=>function($val,$rule){
          if(empty($val)){
              return '发布人的编号不能为空！';
          }
          if(!is_string($val)){
             return '发布人编码格式错误！';
          }
          return true;
      },
      'title'=>function($val,$rule){
          $activityNoRepeat = true;
          if(empty($val)){
            return '活动的名称必须填写！';
          }
          //验证活动的名称是否重复
          if(!is_string($val)){

            return '活动名称的格式不正确！';

          }
          //活动的名号呢个不能重复
          if($activityNoRepeat){

              $where = ['title'=>$val];

              $count = \app\gym\model\Activity::where($where)->count();

              if($count>0){
                  return '已经添加名称相同的活动，不能重复添加！';
              }

          }
          return true;
      },
  ];
