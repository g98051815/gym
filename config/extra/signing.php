<?php
return [
    'max_member_signing'=>1, //一个会员最多可以签约几个教练
    'max_fitness_trainer_signing'=>1,//一个教练最多可以签约多少健身房
    'default_gym_id'=>'73afd98fb6d8f78d9cc75955cb79c29b',//系统默认健身房的id
    'member_signing_validate'=>[ //会员签约验证
         'rules'=>
          [
            'fitness_instructor_id'=>'require',
            'gym_id'=>'require',
            'member_id'=>'require',
            'bought_class'=>'require',
            'rest_of_class'=>'require'
          ],
         'message'=>
          [
            'fitness_instructor_id.require'=>'教练的编号不能为空:fitness_id',
            'gym_id.require'=>'健身房的编号不能为空',
            'member_id.require'=>'会员的编号必须存在',
            'bought_class.require|number'=>'全部课程必须存在！',
            'rest_of_class.require|number'=>'剩余的课程必须存在！',
          ]
    ],
    'member_unsigning_validate'=>[ //会员解约的验证
            'rules'=>[
                'fitness_id'=>'require',
                'member_id'=>'require'

            ],
            'message'=>[
                'member_id.require'=>'缺少会员id:fitness_id',
                'fitness_id.require'=>'缺少教练id:member_id'
            ]
    ],

];
