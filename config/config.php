<?php
return[
        'app_debug'=>true, //应用调试模式
        'url_route_on'=>true,//开启路由
        'url_route_must'=>true,//强制开启路由
        'url_param_type'=>1,//按照顺序解析变量
        'show_error_msg'=>true,//显示错误信息
        'app_trace'=>true,//开启trace
        'show_msg'=>'json',//显示json信息
        'auth_check'=>true,//权限认证开启或者关闭
        'inter_face_mode'=>true,//是否开启接口模式
        'trace'=>[
              'type'=>'console',//开启调试模式是以console为准的
        ],
        //缓存设置
        'cache'=>[
          'type'=>'file',
          'path'=>CACHE_PATH,
          'prefix'=>'gym_',
          'expire'=>1
        ],
        //session设置
        'session'=>[
          'var_session_id'=>'gym_', //解决跨域问题的session前戳
          'auto_start'=>true,
        ]

];
