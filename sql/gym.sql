-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 2017-10-13 10:24:04
-- 服务器版本： 10.1.26-MariaDB
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gym`
--
CREATE DATABASE IF NOT EXISTS `gym` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `gym`;

-- --------------------------------------------------------

--
-- 表的结构 `gym_answer_supplement`
--

DROP TABLE IF EXISTS `gym_answer_supplement`;
CREATE TABLE `gym_answer_supplement` (
  `id` int(15) UNSIGNED NOT NULL COMMENT '自动增长id主键',
  `supplement_key` varchar(100) NOT NULL COMMENT '补充的键名',
  `create_time` varchar(100) NOT NULL COMMENT '创建的时间',
  `answer_id` int(15) UNSIGNED NOT NULL COMMENT '答案的id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='问卷补充表';

--
-- 插入之前先把表清空（truncate） `gym_answer_supplement`
--

TRUNCATE TABLE `gym_answer_supplement`;
--
-- 转存表中的数据 `gym_answer_supplement`
--

INSERT INTO `gym_answer_supplement` (`id`, `supplement_key`, `create_time`, `answer_id`) VALUES
(1, '体检状况', '1507877888', 1),
(2, '检查结果', '1507877888', 3),
(3, '有何不适', '1507877888', 5),
(4, '部位拉伤', '1507877888', 7),
(5, '饮食爱好', '1507877888', 9),
(6, '吸烟量', '1507877888', 11);

-- --------------------------------------------------------

--
-- 表的结构 `gym_auth_group`
--

DROP TABLE IF EXISTS `gym_auth_group`;
CREATE TABLE `gym_auth_group` (
  `id` int(11) UNSIGNED NOT NULL COMMENT '自动增长的id内容',
  `title` char(100) NOT NULL DEFAULT '' COMMENT '用户组的名称不代表用户组真实的名称',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '用户组当前的状态',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '说明',
  `listorder` int(11) NOT NULL DEFAULT '0' COMMENT '用户组的排序'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='用户组表以此来区分用户是什么身份';

--
-- 插入之前先把表清空（truncate） `gym_auth_group`
--

TRUNCATE TABLE `gym_auth_group`;
--
-- 转存表中的数据 `gym_auth_group`
--

INSERT INTO `gym_auth_group` (`id`, `title`, `status`, `description`, `listorder`) VALUES
(1, '超级管理员', 1, '超级管理员用户组', 0),
(2, '保留用户组', 1, '保留用户组', 0),
(3, '匿名用户组', 1, '匿名用户组', 0);

-- --------------------------------------------------------

--
-- 表的结构 `gym_auth_group_relation`
--

DROP TABLE IF EXISTS `gym_auth_group_relation`;
CREATE TABLE `gym_auth_group_relation` (
  `uid` char(64) NOT NULL COMMENT '用户的id',
  `group_id` int(11) UNSIGNED NOT NULL COMMENT '用户组的id',
  `id` int(15) UNSIGNED NOT NULL COMMENT '主键'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='用户组明细表';

--
-- 插入之前先把表清空（truncate） `gym_auth_group_relation`
--

TRUNCATE TABLE `gym_auth_group_relation`;
--
-- 转存表中的数据 `gym_auth_group_relation`
--

INSERT INTO `gym_auth_group_relation` (`uid`, `group_id`, `id`) VALUES
('2998b48e2229ddd9a3e959537f7b0051', 3, 1),
('2db04240496c5f1bb6f759537f8d3b33', 1, 2),
('2db04240496c5f1bb6f759537f8d3b33', 2, 3),
('36ffd09ebd8dbd118960597ee8664923', 3, 4),
('528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 2, 5),
('8f8571f7071cc356587d597eeea9bdea', 3, 7),
('66b651a71a0114555886597e9fe6eed5', 1, 8);

-- --------------------------------------------------------

--
-- 表的结构 `gym_auth_rule`
--

DROP TABLE IF EXISTS `gym_auth_rule`;
CREATE TABLE `gym_auth_rule` (
  `id` int(11) UNSIGNED NOT NULL COMMENT '自动增长id',
  `pid` int(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '父级id',
  `title` char(20) NOT NULL DEFAULT '' COMMENT '规则中文名称',
  `module` varchar(45) NOT NULL DEFAULT '' COMMENT '模块',
  `controller` varchar(45) NOT NULL DEFAULT '' COMMENT '控制器',
  `action` varchar(45) NOT NULL DEFAULT '' COMMENT '行为',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态：1为正常，0为禁用',
  `ismenu` tinyint(1) UNSIGNED NOT NULL DEFAULT '1' COMMENT '菜单显示',
  `condition` char(100) NOT NULL DEFAULT '' COMMENT '附加的权限条件',
  `icon` varchar(45) NOT NULL DEFAULT '' COMMENT '图标',
  `listorder` int(11) NOT NULL DEFAULT '9999' COMMENT '排序',
  `group_id` varchar(255) NOT NULL DEFAULT '1' COMMENT '用户组的id'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='用户的规则表';

--
-- 插入之前先把表清空（truncate） `gym_auth_rule`
--

TRUNCATE TABLE `gym_auth_rule`;
--
-- 转存表中的数据 `gym_auth_rule`
--

INSERT INTO `gym_auth_rule` (`id`, `pid`, `title`, `module`, `controller`, `action`, `status`, `ismenu`, `condition`, `icon`, `listorder`, `group_id`) VALUES
(1, 0, '全部权限', 'user', 'Index', 'offline', 1, 0, '', '', 9999, '1,2,3'),
(2, 0, '另外一个模型的权限', 'user', 'Index', 'hello', 1, 0, '', '', 9999, '1,2,3'),
(3, 0, '', 'user', 'Index', 'aaa', 1, 0, '', '', 9999, '3'),
(4, 0, '用户信息', 'user', 'Index', 'info', 1, 0, '', '', 9999, '1,2,3'),
(5, 0, '', 'publictool', 'PublicTool', 'captcha', 1, 0, '', '', 9999, '1,2,3'),
(6, 0, '', 'publictool', 'PublicTool', 'checkCaptcha', 1, 0, '', '', 9999, '1,2,3'),
(7, 0, '', 'publictool', 'PublicTool', 'captchaShow', 1, 0, '', '', 9999, '1,2,3'),
(8, 0, '活动添加', 'gym', 'Index', 'activityAdd', 1, 1, '', '', 9999, '1,2,3');

-- --------------------------------------------------------

--
-- 表的结构 `gym_captcha_code`
--

DROP TABLE IF EXISTS `gym_captcha_code`;
CREATE TABLE `gym_captcha_code` (
  `code_key` varchar(70) NOT NULL DEFAULT '0' COMMENT '验证码的键',
  `code` varchar(64) NOT NULL DEFAULT '0' COMMENT '验证码',
  `expiration` int(15) NOT NULL DEFAULT '0' COMMENT '到期时间'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='验证码表';

--
-- 插入之前先把表清空（truncate） `gym_captcha_code`
--

TRUNCATE TABLE `gym_captcha_code`;
--
-- 转存表中的数据 `gym_captcha_code`
--

INSERT INTO `gym_captcha_code` (`code_key`, `code`, `expiration`) VALUES
('a8849192546b6f3efbc65ecbb239cb3812b2e1ba0900953fdccb595923e8b2f8', '0efaf53d17012a30d0d01566bb3550e3', 1499014200);

-- --------------------------------------------------------

--
-- 表的结构 `gym_certificate_of_merit`
--

DROP TABLE IF EXISTS `gym_certificate_of_merit`;
CREATE TABLE `gym_certificate_of_merit` (
  `id` int(15) UNSIGNED NOT NULL COMMENT '主键',
  `picture` text NOT NULL COMMENT '奖励的图片',
  `sender_id` char(64) NOT NULL COMMENT '发送人的id',
  `recipient_id` char(64) NOT NULL COMMENT '接收人的id',
  `gym_id` char(64) NOT NULL COMMENT '健身房的编号',
  `create_time` int(15) UNSIGNED NOT NULL COMMENT '创建时间',
  `last_update_time` int(15) UNSIGNED NOT NULL COMMENT '最后的修改时间',
  `full_name` varchar(100) NOT NULL COMMENT '姓名',
  `award_speech` text NOT NULL COMMENT '输入颁奖词',
  `more_rawrads` text NOT NULL COMMENT '更多的奖励'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='奖励表';

--
-- 插入之前先把表清空（truncate） `gym_certificate_of_merit`
--

TRUNCATE TABLE `gym_certificate_of_merit`;
--
-- 转存表中的数据 `gym_certificate_of_merit`
--

INSERT INTO `gym_certificate_of_merit` (`id`, `picture`, `sender_id`, `recipient_id`, `gym_id`, `create_time`, `last_update_time`, `full_name`, `award_speech`, `more_rawrads`) VALUES
(1, '11', '22', '8f8571f7071cc356587d597eeea9bdea', '73afd98fb6d8f78d9cc75955cb79c29b', 1506305816, 1506305816, '77', '88', '99');

-- --------------------------------------------------------

--
-- 表的结构 `gym_circle`
--

DROP TABLE IF EXISTS `gym_circle`;
CREATE TABLE `gym_circle` (
  `id` int(15) UNSIGNED NOT NULL COMMENT '自动增长类型',
  `type` int(15) UNSIGNED NOT NULL COMMENT '类型',
  `create_time` int(15) UNSIGNED NOT NULL COMMENT '创建时间',
  `last_update_time` int(15) UNSIGNED NOT NULL COMMENT '最后修改时间',
  `gym_id` char(64) NOT NULL COMMENT '健身房的编号(通过健身房和朋友圈子的信息可以检索内容)',
  `uuid` char(64) NOT NULL COMMENT '发送信息用户的uuid',
  `index_id` int(15) UNSIGNED NOT NULL COMMENT '索引id所在表的id',
  `fitness_id` char(64) NOT NULL COMMENT '教练的编号'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='朋友圈的功能';

--
-- 插入之前先把表清空（truncate） `gym_circle`
--

TRUNCATE TABLE `gym_circle`;
--
-- 转存表中的数据 `gym_circle`
--

INSERT INTO `gym_circle` (`id`, `type`, `create_time`, `last_update_time`, `gym_id`, `uuid`, `index_id`, `fitness_id`) VALUES
(18, 3, 1504858682, 1504858682, '21754deec45f9f55a2ce5955f364dd90', '8f8571f7071cc356587d597eeea9bdea', 59, '528a11f3378b0ee3bc40d8a3125b2668a8524cd8'),
(17, 3, 1504858675, 1504858675, '21754deec45f9f55a2ce5955f364dd90', '8f8571f7071cc356587d597eeea9bdea', 58, '528a11f3378b0ee3bc40d8a3125b2668a8524cd8'),
(16, 4, 150023132, 150023132, '73afd98fb6d8f78d9cc75955cb79c29b', 'none', 5, 'none'),
(19, 2, 1506305816, 1506305816, '21754deec45f9f55a2ce5955f364dd90', '8f8571f7071cc356587d597eeea9bdea', 1, '528a11f3378b0ee3bc40d8a3125b2668a8524cd8'),
(24, 3, 1507804312, 1507804312, '21754deec45f9f55a2ce5955f364dd90', '8f8571f7071cc356587d597eeea9bdea', 71, '528a11f3378b0ee3bc40d8a3125b2668a8524cd8'),
(23, 1, 1507717714, 1507717714, '21754deec45f9f55a2ce5955f364dd90', '8f8571f7071cc356587d597eeea9bdea', 1, '528a11f3378b0ee3bc40d8a3125b2668a8524cd8');

-- --------------------------------------------------------

--
-- 表的结构 `gym_circle_comment`
--

DROP TABLE IF EXISTS `gym_circle_comment`;
CREATE TABLE `gym_circle_comment` (
  `id` int(15) UNSIGNED NOT NULL COMMENT '主键',
  `circle_id` int(15) UNSIGNED NOT NULL COMMENT '朋友圈的id',
  `content` varchar(255) NOT NULL COMMENT '内容',
  `uid` char(64) NOT NULL,
  `create_time` int(15) UNSIGNED NOT NULL COMMENT '创建时间'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='评论表';

--
-- 插入之前先把表清空（truncate） `gym_circle_comment`
--

TRUNCATE TABLE `gym_circle_comment`;
--
-- 转存表中的数据 `gym_circle_comment`
--

INSERT INTO `gym_circle_comment` (`id`, `circle_id`, `content`, `uid`, `create_time`) VALUES
(1, 23, '添加', '8f8571f7071cc356587d597eeea9bdea', 1507717787),
(2, 18, '甜甜', '8f8571f7071cc356587d597eeea9bdea', 1507717874),
(3, 16, '评论活动', '8f8571f7071cc356587d597eeea9bdea', 1507734271),
(4, 16, 'jiaping', '8f8571f7071cc356587d597eeea9bdea', 1507735497),
(5, 23, 'v好怀念', '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 1507781420),
(6, 0, 'ddc', '8f8571f7071cc356587d597eeea9bdea', 1507803804),
(7, 1, 'rfff', '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 1507804043),
(8, 1, 'rfff', '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 1507804046),
(9, 1, 'rfff', '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 1507804047),
(10, 1, 'rfff', '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 1507804047),
(11, 1, '聊聊', '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 1507804074),
(12, 1, '聊聊', '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 1507804076),
(13, 1, '聊聊', '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 1507804077),
(14, 24, '来来来囧', '8f8571f7071cc356587d597eeea9bdea', 1507804364),
(15, 1, '电饭锅', '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 1507806323),
(16, 1, '电饭锅', '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 1507806324),
(17, 1, '电饭锅', '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 1507806324),
(18, 1, '电饭锅', '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 1507806324),
(19, 1, '电饭锅', '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 1507806325),
(20, 1, '电饭锅', '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 1507806325),
(21, 1, '电饭锅', '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 1507806325),
(22, 1, '电饭锅', '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 1507806325),
(23, 1, '电饭锅', '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 1507806325),
(24, 1, '电饭锅', '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 1507806325),
(25, 1, '电饭锅', '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 1507806326),
(26, 1, '电饭锅', '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 1507806326),
(27, 1, '电饭锅', '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 1507806326),
(28, 1, '电饭锅', '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 1507806326),
(29, 1, '电饭锅', '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 1507806326),
(30, 1, '电饭锅', '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 1507806327),
(31, 1, '电饭锅', '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 1507806328),
(32, 1, 'ddiaoghaohgoahgoahog', '8f8571f7071cc356587d597eeea9bdea', 1507817461),
(33, 1, 'ahgoahgohaoho', '8f8571f7071cc356587d597eeea9bdea', 1507817862),
(34, 1, 'ahgaohgoahgoahoig', '8f8571f7071cc356587d597eeea9bdea', 1507818319),
(35, 1, 'ahgaohgoahog', '8f8571f7071cc356587d597eeea9bdea', 1507818354),
(36, 23, 'ahgaohgoahgohaoghoa', '8f8571f7071cc356587d597eeea9bdea', 1507819553),
(37, 23, 'aaaaaaaaaaaaaa', '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 1507819784);

-- --------------------------------------------------------

--
-- 表的结构 `gym_course`
--

DROP TABLE IF EXISTS `gym_course`;
CREATE TABLE `gym_course` (
  `id` int(15) UNSIGNED NOT NULL COMMENT '作为主键使用',
  `begin_time` int(15) UNSIGNED NOT NULL COMMENT '开始时间',
  `end_time` int(15) UNSIGNED NOT NULL COMMENT '结束时间',
  `create_time` int(15) UNSIGNED NOT NULL COMMENT '创建时间',
  `feeding` varchar(200) NOT NULL DEFAULT '' COMMENT '是否存在营养餐',
  `target_section` varchar(200) NOT NULL COMMENT '本节目标',
  `member_id` char(64) NOT NULL COMMENT '用户的编号',
  `fitness_id` char(64) NOT NULL COMMENT '教练的id',
  `gym_id` char(64) NOT NULL COMMENT '健身房的编号'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='教练的约课信息';

--
-- 插入之前先把表清空（truncate） `gym_course`
--

TRUNCATE TABLE `gym_course`;
--
-- 转存表中的数据 `gym_course`
--

INSERT INTO `gym_course` (`id`, `begin_time`, `end_time`, `create_time`, `feeding`, `target_section`, `member_id`, `fitness_id`, `gym_id`) VALUES
(1, 1507701611, 1507708811, 1507717656, '4', '移民明明我恭送送心红鞋子', '8f8571f7071cc356587d597eeea9bdea', '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', '21754deec45f9f55a2ce5955f364dd90');

-- --------------------------------------------------------

--
-- 表的结构 `gym_course_content`
--

DROP TABLE IF EXISTS `gym_course_content`;
CREATE TABLE `gym_course_content` (
  `id` int(15) UNSIGNED NOT NULL COMMENT 'id做为主键使用',
  `course_id` int(15) UNSIGNED NOT NULL COMMENT '课程编号',
  `exercise_vent_id` int(15) UNSIGNED NOT NULL COMMENT '锻炼动作的编号',
  `length` int(15) UNSIGNED NOT NULL COMMENT '动作时间的长度\\或者动作的次数长度',
  `repeat` int(3) UNSIGNED NOT NULL COMMENT '重复多少次',
  `create_time` int(15) UNSIGNED NOT NULL COMMENT '创建时间'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='上课内容表';

--
-- 插入之前先把表清空（truncate） `gym_course_content`
--

TRUNCATE TABLE `gym_course_content`;
--
-- 转存表中的数据 `gym_course_content`
--

INSERT INTO `gym_course_content` (`id`, `course_id`, `exercise_vent_id`, `length`, `repeat`, `create_time`) VALUES
(1, 1, 1, 50, 50, 1507717656);

-- --------------------------------------------------------

--
-- 表的结构 `gym_course_dynamic`
--

DROP TABLE IF EXISTS `gym_course_dynamic`;
CREATE TABLE `gym_course_dynamic` (
  `id` int(15) UNSIGNED NOT NULL COMMENT '课程动态表,连接圈子功能',
  `course_content_id` int(15) UNSIGNED NOT NULL COMMENT '课程动态的id',
  `coach_reviews` varchar(255) NOT NULL COMMENT '教练的点评',
  `coach_id` char(64) NOT NULL COMMENT '教练的id',
  `gym_id` char(64) NOT NULL COMMENT '健身房的id',
  `member_id` char(64) NOT NULL COMMENT '会员的id',
  `create_time` int(64) NOT NULL COMMENT '创建时间',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT '1' COMMENT '状态信息',
  `last_update_time` int(15) NOT NULL COMMENT '最后修改时间'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='课程动态表';

--
-- 插入之前先把表清空（truncate） `gym_course_dynamic`
--

TRUNCATE TABLE `gym_course_dynamic`;
--
-- 转存表中的数据 `gym_course_dynamic`
--

INSERT INTO `gym_course_dynamic` (`id`, `course_content_id`, `coach_reviews`, `coach_id`, `gym_id`, `member_id`, `create_time`, `status`, `last_update_time`) VALUES
(1, 1, '打不动态', '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', '73afd98fb6d8f78d9cc75955cb79c29b', '8f8571f7071cc356587d597eeea9bdea', 1507717714, 1, 1507717714);

-- --------------------------------------------------------

--
-- 表的结构 `gym_course_dynamic_img`
--

DROP TABLE IF EXISTS `gym_course_dynamic_img`;
CREATE TABLE `gym_course_dynamic_img` (
  `id` int(15) UNSIGNED NOT NULL COMMENT '主键',
  `course_dynamic_id` int(15) UNSIGNED NOT NULL COMMENT '课程动态编号',
  `img_url` text NOT NULL COMMENT '图片地址',
  `create_time` int(15) UNSIGNED NOT NULL COMMENT '创建时间',
  `last_update_time` int(15) UNSIGNED NOT NULL COMMENT '最后修改时间'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='课程动态图片';

--
-- 插入之前先把表清空（truncate） `gym_course_dynamic_img`
--

TRUNCATE TABLE `gym_course_dynamic_img`;
--
-- 转存表中的数据 `gym_course_dynamic_img`
--

INSERT INTO `gym_course_dynamic_img` (`id`, `course_dynamic_id`, `img_url`, `create_time`, `last_update_time`) VALUES
(68, 1, 'tmp_1166517312o6zAJs9HkQKYUsVmUfoXtpxINqOc73e7a90cb8e68550b2543ce01c61a7ab.jpg', 1507789658, 1507789658),
(67, 1, 'tmp_1166517312o6zAJs9HkQKYUsVmUfoXtpxINqOcf940d79a7d8ce010673c007275d09c66.jpg', 1507789563, 1507789563),
(66, 1, 'tmp_1166517312o6zAJs9HkQKYUsVmUfoXtpxINqOcf6c6f93be834dfc27bd6b2f51c9808ee.jpg', 1507789493, 1507789493),
(21, 1, '2016-07-22_5791ea7c75e4c.jpg', 1504807078, 1504807078),
(22, 1, '2016-07-25_5795ca0f472a2.jpeg', 1504807100, 1504807100),
(23, 1, '2016-07-25_5795ca4693d65.jpeg', 1504861837, 1504861837),
(24, 1, '2016-07-25_5795ca75ddbf2.jpeg', 1504862864, 1504862864),
(25, 1, '2016-07-25_5795ca92cabe2.jpeg', 1504862869, 1504862869),
(26, 14, '1462243024.jpg', 1504863155, 1504863155),
(27, 14, '1462245303.jpg', 1504863189, 1504863189),
(28, 14, '1462256062.jpg', 1504864837, 1504864837),
(29, 14, '1462266373000213.jpg', 1504864840, 1504864840),
(30, 14, '	\r\n1462266475000515.jpg', 1504864868, 1504864868),
(31, 14, '1462267045000115.jpg', 1504866874, 1504866874),
(32, 14, '	\r\n1462267045000410.jpg', 1504866877, 1504866877),
(33, 14, '146226704500047.jpg', 1504866880, 1504866880),
(34, 14, 'tmp_0d277e079293b7e4956a3d69eb321f87b724a1257130a5df.jpg', 1504866882, 1504866882),
(35, 14, 'tmp_2b3ad89c5a36ada55cd27e6cf2af4dcb87b4e089a7fb63ff.jpg', 1504866891, 1504866891),
(36, 14, 'tmp_33cb8c0f28555107e9e6163fdf994f26cf05047c869689be.jpg', 1504866911, 1504866911),
(37, 14, 'tmp_c554b3677c6036ec3ba16978c5b9df29ba7fda033e40e679.jpg', 1504866930, 1504866930),
(38, 14, 'tmp_f6e06d8939cc86bec7494503c8dd59832ac2d6e3081b0821.jpg', 1505102781, 1505102781),
(39, 14, 'tmp_c402d4702e1013d22246f4097ae625f5c84d73b4612e9aee.jpg', 1505102785, 1505102785),
(40, 14, 'tmp_2289f4238e9dab519d556302e6ea2c8f3ec6745027a160d7.jpg', 1505102863, 1505102863),
(41, 14, 'tmp_9c51d2912e6e12698ce534ce9a3d32437c9558f379a7f58a.jpg', 1505103400, 1505103400),
(42, 14, 'tmp_083bab1e02a18ccdcc81f9eb5a8e335c2df6cc7f03f0c201.jpg', 1505106604, 1505106604),
(43, 14, 'tmp_5cf2cd139ff047db07417a296e4605ee8da4eb123f516bd7.jpg', 1505203397, 1505203397),
(44, 14, 'tmp_fd9fc65f84c167849967878e69fb52dc863a5c54fef5e4f6.jpg', 1505213460, 1505213460),
(45, 14, 'tmp_739b1faaafb17eb8f7895e1ae231a51c3bdeb712a08ff21d.jpg', 1505213467, 1505213467),
(46, 14, 'tmp_ddfbb5a262b04126ef1649262b71993705845cd96431841e.jpg', 1505213518, 1505213518),
(47, 14, 'tmp_fec9773251ee3bbf75544b785cd6f097753f1c49678b9c04.jpg', 1505213529, 1505213529),
(48, 14, 'tmp_7ff30f8536f1bb3e8329f07bbe3c51c071bcd0609d7907a9.jpg', 1505213578, 1505213578),
(49, 14, 'tmp_5bc5ad8fafd76530b0e7d7379aac15e863d4641841d3323d.jpg', 1505214213, 1505214213),
(50, 10032, 'tmp_c0103c6664241715592c8004d678bffd73daf985f7ed4044.jpg', 1505214662, 1505214662),
(51, 14, 'tmp_8974f4577a5140f50432b00030ae16647fedc8a5dcec4e6b.jpg', 1505215164, 1505215164),
(52, 14, 'tmp_b4b3d48baf9119c036591a162b19ce9aba0223de4df5e212.jpg', 1505215204, 1505215204),
(53, 14, 'tmp_d6e00464ba193225092db6e221430a3b040a6c46a170d95a.jpg', 1505215210, 1505215210),
(54, 10033, 'tmp_922035fd95a9ae34743f1a63f65aabc711546df6001ee766.jpg', 1505285887, 1505285887),
(55, 10033, 'tmp_2611869a9280da327edd6ed16fac413ee02a041c6b9be46c.jpg', 1505285904, 1505285904),
(56, 0, 'tmp_3cc9fb78811b00456af8421c667577c964b3ecb6dee7432c.jpg', 1506483376, 1506483376),
(57, 0, 'tmp_5eb1e644f3df05485391144b01d259907bcbfbb29f34f218.jpg', 1506483377, 1506483377),
(58, 0, 'tmp_f5f68e7124c0fa57bf5361f5e1a53f44a4c2df96effce760.jpg', 1506483385, 1506483385),
(59, 0, 'tmp_7cd10f664f73208ac95d8c8b7cc1e1da42f68fac0044685e.jpg', 1506483392, 1506483392),
(60, 0, 'tmp_1848354609o6zAJs9HkQKYUsVmUfoXtpxINqOc7554663ebee1d74c38670de7b53cd123.jpg', 1506501179, 1506501179),
(61, 0, 'tmp_1848354609o6zAJs9HkQKYUsVmUfoXtpxINqOcbb6c8ce5da785f6456b3880b62ec947f.jpg', 1506501229, 1506501229),
(65, 1, 'tmp_1166517312o6zAJs9HkQKYUsVmUfoXtpxINqOc55fcc00f4964912edf30c08bfb2b1c5d.jpg', 1507789339, 1507789339),
(69, 1, 'tmp_1166517312o6zAJs9HkQKYUsVmUfoXtpxINqOc708cec17887f3f1b824b2fea0c1c98ea.jpg', 1507789765, 1507789765),
(70, 1, 'tmp_1166517312o6zAJs9HkQKYUsVmUfoXtpxINqOc10e87e7d109e8de2834bae72cb7eccb5.jpg', 1507790921, 1507790921),
(71, 1, 'tmp_f0610f763ecdeff051d16bed30ff3324369c15b669d3d336.jpg', 1507803870, 1507803870);

-- --------------------------------------------------------

--
-- 表的结构 `gym_disable`
--

DROP TABLE IF EXISTS `gym_disable`;
CREATE TABLE `gym_disable` (
  `id` int(11) NOT NULL,
  `uuid` char(64) NOT NULL COMMENT '编号',
  `disabled` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否禁用'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='是否禁用';

--
-- 插入之前先把表清空（truncate） `gym_disable`
--

TRUNCATE TABLE `gym_disable`;
--
-- 转存表中的数据 `gym_disable`
--

INSERT INTO `gym_disable` (`id`, `uuid`, `disabled`) VALUES
(1, '66b651a71a0114555886597e9fe6eed5', 1);

-- --------------------------------------------------------

--
-- 表的结构 `gym_exercise_vent`
--

DROP TABLE IF EXISTS `gym_exercise_vent`;
CREATE TABLE `gym_exercise_vent` (
  `id` int(15) UNSIGNED NOT NULL COMMENT '自动增长id作为主见使用',
  `title` char(100) NOT NULL COMMENT '项目名称',
  `parent` int(15) UNSIGNED NOT NULL COMMENT '上级项目的编码',
  `create_time` int(15) UNSIGNED NOT NULL COMMENT '创建项目',
  `last_update_time` int(15) UNSIGNED NOT NULL COMMENT '最后修改时间',
  `amount` tinyint(3) UNSIGNED NOT NULL COMMENT '计量单位:1:量，2:秒,0无计量单位',
  `fitness_id` char(64) NOT NULL COMMENT '归属于哪一个教练'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='锻炼项目';

--
-- 插入之前先把表清空（truncate） `gym_exercise_vent`
--

TRUNCATE TABLE `gym_exercise_vent`;
--
-- 转存表中的数据 `gym_exercise_vent`
--

INSERT INTO `gym_exercise_vent` (`id`, `title`, `parent`, `create_time`, `last_update_time`, `amount`, `fitness_id`) VALUES
(1, '康复训练', 0, 1506586286, 1506586286, 0, '528a11f3378b0ee3bc40d8a3125b2668a8524cd8'),
(2, '第二个康复训练', 0, 1507529397, 1507529397, 2, '1'),
(3, '第三个康复训练', 0, 1507529397, 1507529397, 0, '2');

-- --------------------------------------------------------

--
-- 表的结构 `gym_gym_activity`
--

DROP TABLE IF EXISTS `gym_gym_activity`;
CREATE TABLE `gym_gym_activity` (
  `id` int(15) UNSIGNED NOT NULL COMMENT '活动的序列id',
  `location` varchar(100) NOT NULL DEFAULT '' COMMENT '活动的地点',
  `date_time` int(15) UNSIGNED NOT NULL DEFAULT '0' COMMENT '活动的开始时间',
  `over_time` int(15) UNSIGNED NOT NULL COMMENT '活动结束时间',
  `gym_id` varchar(64) NOT NULL COMMENT '健身房的编号,可以选择是一个健身房还是多个健身房,或者是全部的健身房,使用all进行代表',
  `explain` text NOT NULL COMMENT '活动的说明',
  `uuid` varchar(64) NOT NULL COMMENT '发布人',
  `pictures` varchar(200) NOT NULL DEFAULT '' COMMENT '活动的图片',
  `create_time` int(15) UNSIGNED NOT NULL DEFAULT '0' COMMENT '创建时间',
  `last_update_time` int(15) UNSIGNED NOT NULL DEFAULT '0' COMMENT '最后修改的时间',
  `status` int(1) UNSIGNED NOT NULL DEFAULT '1' COMMENT '活动的状态1:为开启0为关闭',
  `title` varchar(100) NOT NULL DEFAULT '' COMMENT '活动名称',
  `time_signing_up` int(15) UNSIGNED NOT NULL COMMENT '活动开始报名的时间',
  `registration_time` int(15) UNSIGNED NOT NULL COMMENT '活动报名结束的时间',
  `location_code` varchar(100) NOT NULL COMMENT '活动的位置'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='健身房活动表';

--
-- 插入之前先把表清空（truncate） `gym_gym_activity`
--

TRUNCATE TABLE `gym_gym_activity`;
--
-- 转存表中的数据 `gym_gym_activity`
--

INSERT INTO `gym_gym_activity` (`id`, `location`, `date_time`, `over_time`, `gym_id`, `explain`, `uuid`, `pictures`, `create_time`, `last_update_time`, `status`, `title`, `time_signing_up`, `registration_time`, `location_code`) VALUES
(5, '活动开水', 1500360234, 1500360235, '73afd98fb6d8f78d9cc75955cb79c29b', '这个是活动的说明', '66b651a71a0114555886597e9fe6eed5', '2016-04-14_1460645442.png', 1499072988, 1499072988, 1, '这个是活动的名称', 1500360219, 1500360231, '116.481488,39.990464');

-- --------------------------------------------------------

--
-- 表的结构 `gym_gym_info`
--

DROP TABLE IF EXISTS `gym_gym_info`;
CREATE TABLE `gym_gym_info` (
  `id` int(11) NOT NULL COMMENT '自动增长id仅用于计数',
  `store_title` varchar(80) NOT NULL COMMENT '门店名称',
  `phone_num` char(11) NOT NULL COMMENT '门店手机号码',
  `tel_num` varchar(13) NOT NULL COMMENT '门店电话',
  `address` tinytext NOT NULL COMMENT '门店地址',
  `profile` varchar(255) NOT NULL COMMENT '门店简介(最长不能超过30个字符)',
  `pictures` varchar(100) NOT NULL COMMENT '门店图片(可以使用上传多张图片)',
  `location` varchar(150) NOT NULL DEFAULT '''''' COMMENT '门店坐标信息(预留的lbs)',
  `boss` char(64) NOT NULL COMMENT '用户的唯一标识符',
  `create_time` int(20) NOT NULL COMMENT '创建的时间',
  `last_update_time` int(20) NOT NULL COMMENT '最后修改的时间',
  `unique_id` char(64) NOT NULL COMMENT '门店的唯一标识符(由系统生成后存入数据库)',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '店铺当前的状态(1:启用,2:禁用)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='健身房信息';

--
-- 插入之前先把表清空（truncate） `gym_gym_info`
--

TRUNCATE TABLE `gym_gym_info`;
--
-- 转存表中的数据 `gym_gym_info`
--

INSERT INTO `gym_gym_info` (`id`, `store_title`, `phone_num`, `tel_num`, `address`, `profile`, `pictures`, `location`, `boss`, `create_time`, `last_update_time`, `unique_id`, `status`) VALUES
(2, '门店的名称', '18513853817', '0433112322', '门店的地址', '门店的简介', '1,2,3', '\'\'', '66b651a71a0114555886597e9fe6eed5', 1498805047, 1498805047, '21754deec45f9f55a2ce5955f364dd90', 1),
(3, '门店名称', '18513853817', '0444', 'hello', '门店介绍', '111', '\'\'', '66b651a71a0114555886597e9fe6eed5', 1498810703, 1498810703, '6389c5f0a5b44a6feb2d59560943dc08', 1),
(4, '门店的名称', '18513853817', '0433112322', '门里的静的们', '这个是门店的简介', '111', '\'\'', '66b651a71a0114555886597e9fe6eed5', 1498810862, 1498810862, '7a7560ccdf1ce341a8f05956095ee546', 1),
(5, '门店的名称', '18513853817', '0433112322', '门里的静的们', '这个是门店的简介', '111', '\'\'', '66b651a71a0114555886597e9fe6eed5', 1498810946, 1498810946, '36091d1bf3d92f68f89459560ad34589', 1),
(6, '门店的名称', '18513853817', '0433112322', '门里的静的们', '这个是门店的简介', '111', '\'\'', '66b651a71a0114555886597e9fe6eed5', 1498811030, 1498811030, '59186e63aeee18f7e43759560a44f73a', 1),
(7, '11111', '13111111111', '010-12345678', '北京市海淀区中关村软件园23号楼', '啊还不错', 'http://img0.imgtn.bdimg.com/it/u=961130605,17877239&fm=26&gp=0.jpg', '\'\'', '66b651a71a0114555886597e9fe6eed5', 1498811149, 1498811149, '1855fd35c975f3fe11b059560bb697cc', 1),
(8, '系统默认的健身房', '00000000000', '0000000000', '系统默认的健身房', '系统默认的健身房功能', '2016-08-15_57b1a5cfd8726.jpg', '\'\'', '66b651a71a0114555886597e9fe6eed5', 1498794781, 1498794781, '73afd98fb6d8f78d9cc75955cb79c29b', 1),
(9, '第一个健身房的门店', '18513853817', '010112233', '地址', '简介', '相册', '\'\'', '66b651a71a0114555886597e9fe6eed5', 1504718836, 1504718836, '6ea8b825c2d813ffa4e259b02f0c8e10', 1),
(10, '第一个健身房的门店', '18513853817', '010112233', '地址', '简介', '相册', '\'\'', '66b651a71a0114555886597e9fe6eed5', 1504718888, 1504718888, '1b824271d5218a70ac6c59b0307b99e6', 1);

-- --------------------------------------------------------

--
-- 表的结构 `gym_hour_course`
--

DROP TABLE IF EXISTS `gym_hour_course`;
CREATE TABLE `gym_hour_course` (
  `id` int(15) UNSIGNED NOT NULL COMMENT '主键',
  `fitness_id` char(64) NOT NULL COMMENT '教练的id',
  `member_id` char(64) NOT NULL COMMENT '会员的id',
  `hours` int(15) NOT NULL COMMENT '购买的课时数量,或减少的课程时间',
  `gym_id` char(64) NOT NULL COMMENT '健身房的编号',
  `create_time` int(15) UNSIGNED NOT NULL COMMENT '创建的时间',
  `note` varchar(255) NOT NULL COMMENT '增加或者减少的原因',
  `option` tinyint(1) UNSIGNED NOT NULL COMMENT '操作：1:增加,2:减少',
  `type_status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '1,购买课程，2，修改课程',
  `option_before_hours` int(5) NOT NULL COMMENT '操作前的课程数量',
  `last_update_time` int(15) UNSIGNED NOT NULL COMMENT '最后的修改时间'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='课时购买表';

--
-- 插入之前先把表清空（truncate） `gym_hour_course`
--

TRUNCATE TABLE `gym_hour_course`;
--
-- 转存表中的数据 `gym_hour_course`
--

INSERT INTO `gym_hour_course` (`id`, `fitness_id`, `member_id`, `hours`, `gym_id`, `create_time`, `note`, `option`, `type_status`, `option_before_hours`, `last_update_time`) VALUES
(1, '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', '8f8571f7071cc356587d597eeea9bdea', 1, '21754deec45f9f55a2ce5955f364dd90', 1507803115, '', 1, 2, 56, 1507803115),
(2, '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', '8f8571f7071cc356587d597eeea9bdea', 3, '21754deec45f9f55a2ce5955f364dd90', 1507815787, '', 1, 1, 0, 1507815787);

-- --------------------------------------------------------

--
-- 表的结构 `gym_icinfo`
--

DROP TABLE IF EXISTS `gym_icinfo`;
CREATE TABLE `gym_icinfo` (
  `id` int(15) UNSIGNED NOT NULL COMMENT '主键',
  `legal_person_name` varchar(30) NOT NULL COMMENT '法人的姓名',
  `legal_person_card_id` varchar(19) NOT NULL COMMENT '法人的身份证号码',
  `legal_person_phone_num` varchar(13) NOT NULL COMMENT '法人的手机号码',
  `business_license` varchar(255) NOT NULL COMMENT '营业执照的地址',
  `corporate_name` varchar(200) NOT NULL COMMENT '公司名称',
  `create_time` int(15) UNSIGNED NOT NULL COMMENT '创建的时间',
  `last_update_time` int(15) UNSIGNED NOT NULL COMMENT '最后的修改时间',
  `uuid` char(64) NOT NULL COMMENT '用户的编号'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='工商信息表';

--
-- 插入之前先把表清空（truncate） `gym_icinfo`
--

TRUNCATE TABLE `gym_icinfo`;
--
-- 转存表中的数据 `gym_icinfo`
--

INSERT INTO `gym_icinfo` (`id`, `legal_person_name`, `legal_person_card_id`, `legal_person_phone_num`, `business_license`, `corporate_name`, `create_time`, `last_update_time`, `uuid`) VALUES
(1, '法人的姓名', '232303199506170549', '18513853817', '', '', 0, 0, '66b651a71a0114555886597e9fe6eed5'),
(2, '法人的姓名', '232303199506170549', '18513853817', '', '', 0, 0, '78'),
(3, '龚华涛', '612401199605268312', '18811382869', 'tmp_1166517312o6zAJs9HkQKYUsVmUfoXtpxINqOc6f6f52f8be1ab7ec37bd5220d6d0c9b9.png', '', 1507792920, 1507792920, '8f8571f7071cc356587d597eeea9bdea');

-- --------------------------------------------------------

--
-- 表的结构 `gym_indicators`
--

DROP TABLE IF EXISTS `gym_indicators`;
CREATE TABLE `gym_indicators` (
  `id` int(15) UNSIGNED NOT NULL COMMENT '主键',
  `uuid` char(64) NOT NULL COMMENT '用户的唯一标识符',
  `bust` double NOT NULL DEFAULT '0' COMMENT '胸围指数',
  `waist` double NOT NULL DEFAULT '0' COMMENT '腰围',
  `hip_dimension` double NOT NULL DEFAULT '0' COMMENT '臀维',
  `weight` double NOT NULL DEFAULT '0' COMMENT '体重',
  `bmi` double NOT NULL DEFAULT '0' COMMENT '体质指数(bmi)',
  `body_fat_rate` double NOT NULL DEFAULT '0' COMMENT '体脂率',
  `create_time` int(15) NOT NULL DEFAULT '0' COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='身体健康指标记录';

--
-- 插入之前先把表清空（truncate） `gym_indicators`
--

TRUNCATE TABLE `gym_indicators`;
--
-- 转存表中的数据 `gym_indicators`
--

INSERT INTO `gym_indicators` (`id`, `uuid`, `bust`, `waist`, `hip_dimension`, `weight`, `bmi`, `body_fat_rate`, `create_time`) VALUES
(2, '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 23, 20.3, 20.3, 20.3, 20.3, 20.3, 1501652503),
(3, '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 23, 20.3, 20.3, 20.4, 20.3, 20.3, 1501516800),
(4, '8f8571f7071cc356587d597eeea9bdea', 23, 20.3, 20.3, 20.4, 20.3, 20.3, 1501516800);

-- --------------------------------------------------------

--
-- 表的结构 `gym_member_sign_fitness_application`
--

DROP TABLE IF EXISTS `gym_member_sign_fitness_application`;
CREATE TABLE `gym_member_sign_fitness_application` (
  `id` int(15) UNSIGNED NOT NULL COMMENT '主键',
  `applicant_id` char(64) NOT NULL COMMENT '申请人的编号',
  `agree` tinyint(1) UNSIGNED NOT NULL DEFAULT '2' COMMENT '是否同意，如果同意是1，0,拒绝，2等待审核',
  `acceptance_id` char(64) NOT NULL COMMENT '受理人的编号，也就是教练的编号',
  `create_time` int(15) UNSIGNED NOT NULL COMMENT '创建的时间',
  `last_update_time` int(15) UNSIGNED NOT NULL COMMENT '最后受理的时间',
  `message` varchar(255) NOT NULL DEFAULT '' COMMENT '用户的留言'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='会员签约教练的申请表';

--
-- 插入之前先把表清空（truncate） `gym_member_sign_fitness_application`
--

TRUNCATE TABLE `gym_member_sign_fitness_application`;
--
-- 转存表中的数据 `gym_member_sign_fitness_application`
--

INSERT INTO `gym_member_sign_fitness_application` (`id`, `applicant_id`, `agree`, `acceptance_id`, `create_time`, `last_update_time`, `message`) VALUES
(1, '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 2, '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 1507720265, 1507720265, '');

-- --------------------------------------------------------

--
-- 表的结构 `gym_motto_tpl`
--

DROP TABLE IF EXISTS `gym_motto_tpl`;
CREATE TABLE `gym_motto_tpl` (
  `id` int(15) UNSIGNED NOT NULL COMMENT '自动增长id作为键',
  `content` varchar(200) NOT NULL DEFAULT '' COMMENT '格言的内容最多80个中文字符'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户的格言模板';

--
-- 插入之前先把表清空（truncate） `gym_motto_tpl`
--

TRUNCATE TABLE `gym_motto_tpl`;
-- --------------------------------------------------------

--
-- 表的结构 `gym_notifycation`
--

DROP TABLE IF EXISTS `gym_notifycation`;
CREATE TABLE `gym_notifycation` (
  `id` int(15) UNSIGNED NOT NULL,
  `addressee` char(64) NOT NULL COMMENT '收信人',
  `title` char(80) NOT NULL COMMENT '信息抬头',
  `type` tinyint(4) NOT NULL COMMENT '消息类型',
  `create_time` int(15) UNSIGNED NOT NULL DEFAULT '0' COMMENT '消息的创建时间',
  `reading` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '已读否;1:已读，0:未读',
  `addresser` char(64) NOT NULL COMMENT '发信人',
  `index_id` int(15) UNSIGNED NOT NULL COMMENT '索引id'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='消息通知表';

--
-- 插入之前先把表清空（truncate） `gym_notifycation`
--

TRUNCATE TABLE `gym_notifycation`;
--
-- 转存表中的数据 `gym_notifycation`
--

INSERT INTO `gym_notifycation` (`id`, `addressee`, `title`, `type`, `create_time`, `reading`, `addresser`, `index_id`) VALUES
(1, '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', '签约申请', 1, 1507720265, 1, '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 1),
(2, '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', '约课申请', 3, 1507741587, 1, '8f8571f7071cc356587d597eeea9bdea', 2);

-- --------------------------------------------------------

--
-- 表的结构 `gym_notifycation_content`
--

DROP TABLE IF EXISTS `gym_notifycation_content`;
CREATE TABLE `gym_notifycation_content` (
  `notify_id` int(15) UNSIGNED NOT NULL COMMENT '消息通知的id',
  `content` text NOT NULL COMMENT '消息通知的内容'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='消息通知的内容表';

--
-- 插入之前先把表清空（truncate） `gym_notifycation_content`
--

TRUNCATE TABLE `gym_notifycation_content`;
--
-- 转存表中的数据 `gym_notifycation_content`
--

INSERT INTO `gym_notifycation_content` (`notify_id`, `content`) VALUES
(1499280689, 'member_id,2'),
(10, 'member_id,2'),
(11, 'member_id,2,gym_id1,fitness_id,3');

-- --------------------------------------------------------

--
-- 表的结构 `gym_nutritious_meal`
--

DROP TABLE IF EXISTS `gym_nutritious_meal`;
CREATE TABLE `gym_nutritious_meal` (
  `id` int(15) UNSIGNED NOT NULL COMMENT '自动增长id作为键使用',
  `ingredients` varchar(200) NOT NULL DEFAULT '' COMMENT '营养餐的主要成分输入的内容以,号进行分割',
  `create_time` int(15) UNSIGNED NOT NULL COMMENT '创建时间',
  `last_update_time` int(15) UNSIGNED NOT NULL COMMENT '最后修改时间',
  `picture` varchar(100) NOT NULL COMMENT '营养餐的图片',
  `nutrition` varchar(200) NOT NULL COMMENT '营养成分',
  `gym_id` char(64) NOT NULL COMMENT '健身房的编号',
  `which_day` int(1) UNSIGNED NOT NULL COMMENT '哪一天（周几）'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='健身房营养餐明细';

--
-- 插入之前先把表清空（truncate） `gym_nutritious_meal`
--

TRUNCATE TABLE `gym_nutritious_meal`;
--
-- 转存表中的数据 `gym_nutritious_meal`
--

INSERT INTO `gym_nutritious_meal` (`id`, `ingredients`, `create_time`, `last_update_time`, `picture`, `nutrition`, `gym_id`, `which_day`) VALUES
(1, '更改后的营养成5分', 1504803601, 1504804055, '图片', '营养成分1', '1855fd35c975f3fe11b059560bb697cc', 5),
(2, '周一营养餐的主要成分', 1506423457, 1506423457, '2016-05-03_1462242784.jpg,2016-05-03_1462245900.jpg', '营养成分1', '73afd98fb6d8f78d9cc75955cb79c29b', 1),
(3, '周二营养餐的主要成分', 1506423457, 1506423457, '2016-05-03_1462246443.jpg,2016-05-03_1462246585.jpg', '营养成分1', '73afd98fb6d8f78d9cc75955cb79c29b', 2),
(4, '周三营养餐的主要成分', 1506423457, 1506423457, '2016-05-03_1462255997.jpg,2016-05-03_1462256576.jpg', '营养成分1', '73afd98fb6d8f78d9cc75955cb79c29b', 3),
(5, '周四营养餐的主要成分', 1506423457, 1506423457, '2016-07-22_5791e4c97ef6f.jpeg', '营养成分1', '73afd98fb6d8f78d9cc75955cb79c29b', 4),
(6, '周五营养餐的主要成分', 1506423457, 1506423457, '2016-05-03_1462267190.jpg', '营养成分1', '73afd98fb6d8f78d9cc75955cb79c29b', 5),
(7, '周六营养餐的主要成分', 1506423457, 1506423457, '2016-05-03_1462269929.jpg', '营养成分1', '73afd98fb6d8f78d9cc75955cb79c29b', 6),
(8, '周日营养餐的主要成分', 1506423457, 1506423457, '2016-07-21_5790c44c89dbb.jpg', '营养成分1', '73afd98fb6d8f78d9cc75955cb79c29b', 0);

-- --------------------------------------------------------

--
-- 表的结构 `gym_nutritious_meal_history`
--

DROP TABLE IF EXISTS `gym_nutritious_meal_history`;
CREATE TABLE `gym_nutritious_meal_history` (
  `id` int(15) UNSIGNED NOT NULL COMMENT '自动增长id作为键使用',
  `ingredients` varchar(200) NOT NULL DEFAULT '' COMMENT '营养餐的主要成分输入的内容以,号进行分割',
  `create_time` int(15) UNSIGNED NOT NULL COMMENT '创建时间',
  `last_update_time` int(15) UNSIGNED NOT NULL COMMENT '最后修改时间',
  `picture` varchar(100) NOT NULL COMMENT '营养餐的图片',
  `nutrition` varchar(200) NOT NULL COMMENT '营养成分',
  `gym_id` char(64) NOT NULL COMMENT '健身房的编号',
  `which_day` int(1) UNSIGNED NOT NULL COMMENT '哪一天（周几）'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='健身房营养餐明细';

--
-- 插入之前先把表清空（truncate） `gym_nutritious_meal_history`
--

TRUNCATE TABLE `gym_nutritious_meal_history`;
--
-- 转存表中的数据 `gym_nutritious_meal_history`
--

INSERT INTO `gym_nutritious_meal_history` (`id`, `ingredients`, `create_time`, `last_update_time`, `picture`, `nutrition`, `gym_id`, `which_day`) VALUES
(1, '更改后的营养成分', 0, 0, '图片', '营养成分1', '1855fd35c975f3fe11b059560bb697cc', 5),
(2, '更改后的营养成5分', 1504804055, 1504804055, '图片', '营养成分1', '1855fd35c975f3fe11b059560bb697cc', 5);

-- --------------------------------------------------------

--
-- 表的结构 `gym_other_account`
--

DROP TABLE IF EXISTS `gym_other_account`;
CREATE TABLE `gym_other_account` (
  `uuid` char(64) NOT NULL COMMENT '用户唯一标识(用户的唯一标识是可以重复的)',
  `openid` char(64) NOT NULL COMMENT '第三方账户的开发id',
  `platform_type` tinyint(3) NOT NULL COMMENT '最多可以兼容30个平台具体需要定义',
  `id` int(15) UNSIGNED NOT NULL COMMENT '主键'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='非本地手机号码注册的账户(其他平台注册账户)';

--
-- 插入之前先把表清空（truncate） `gym_other_account`
--

TRUNCATE TABLE `gym_other_account`;
--
-- 转存表中的数据 `gym_other_account`
--

INSERT INTO `gym_other_account` (`uuid`, `openid`, `platform_type`, `id`) VALUES
('7bbe2c16fee5e2569610597ee89e0fdc', '111', 3, 1),
('8f8571f7071cc356587d597eeea9bdea', 'oQgAY0U-bG5GFgSb2qukK2y4gr-M', 3, 2),
('688bf34ffaabcea1246059805d7fe7b2', 'oQgAY0bSv4srxHzQZ2bwl4d34JCo', 3, 3),
('6de608ad087e2a213a8f5997b023b349', 'oQgAY0QYCmwL2ZCu_pzRuQhW85ak', 3, 4),
('125deaf16d03065d026459b8c77472f1', 'oQgAY0aEcKPGONkVeaeRi5mM1wzs', 3, 5),
('36e8c28092dda60a169c59b8d1ea8161', 'oQgAY0T2QFwvPI8g0e26-01vJPpM', 3, 6),
('89585a834d3a4d165b0859dcac9809a2', 'oQgAY0TTjQmJ0wbSwMnNUdABPVS8', 3, 8);

-- --------------------------------------------------------

--
-- 表的结构 `gym_photo_album`
--

DROP TABLE IF EXISTS `gym_photo_album`;
CREATE TABLE `gym_photo_album` (
  `id` int(15) UNSIGNED NOT NULL COMMENT '主键',
  `category_id` int(15) UNSIGNED NOT NULL DEFAULT '1' COMMENT '分类的id',
  `img_url` text NOT NULL COMMENT '图片的地址',
  `uuid` char(64) NOT NULL COMMENT '用户的唯一标识',
  `title` varchar(255) NOT NULL DEFAULT 'photo' COMMENT '图片的名字',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '默认的删除键',
  `create_time` int(15) UNSIGNED NOT NULL COMMENT '创建时间',
  `last_update_time` int(15) UNSIGNED NOT NULL COMMENT '最后修改时间'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='用户的相册(目前是教练的相册)';

--
-- 插入之前先把表清空（truncate） `gym_photo_album`
--

TRUNCATE TABLE `gym_photo_album`;
--
-- 转存表中的数据 `gym_photo_album`
--

INSERT INTO `gym_photo_album` (`id`, `category_id`, `img_url`, `uuid`, `title`, `status`, `create_time`, `last_update_time`) VALUES
(1, 1, 'https://a2-q.mafengwo.net/s10/M00/B5/23/wKgBZ1j20d-Aa2XkADCLL5lB2DE92.jpeg?imageMogr2%2Fthumbnail%2F%21440x270r%2Fgravity%2FCenter%2Fcrop%2F%21440x270%2Fquality%2F90', '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 'photo', 1, 1501524144, 1501524144),
(2, 1, 'https://a2-q.mafengwo.net/s10/M00/B5/23/wKgBZ1j20d-Aa2XkADCLL5lB2DE92.jpeg?imageMogr2%2Fthumbnail%2F%21440x270r%2Fgravity%2FCenter%2Fcrop%2F%21440x270%2Fquality%2F90', '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 'photo', 1, 1501524144, 1501524144),
(3, 1, 'https://a2-q.mafengwo.net/s10/M00/B5/23/wKgBZ1j20d-Aa2XkADCLL5lB2DE92.jpeg?imageMogr2%2Fthumbnail%2F%21440x270r%2Fgravity%2FCenter%2Fcrop%2F%21440x270%2Fquality%2F90', '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 'photo', 1, 1501524144, 1501524144),
(4, 1, 'https://a2-q.mafengwo.net/s10/M00/B5/23/wKgBZ1j20d-Aa2XkADCLL5lB2DE92.jpeg?imageMogr2%2Fthumbnail%2F%21440x270r%2Fgravity%2FCenter%2Fcrop%2F%21440x270%2Fquality%2F90', '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 'photo', 1, 1501524144, 1501524144),
(5, 1, 'https://a2-q.mafengwo.net/s10/M00/B5/23/wKgBZ1j20d-Aa2XkADCLL5lB2DE92.jpeg?imageMogr2%2Fthumbnail%2F%21440x270r%2Fgravity%2FCenter%2Fcrop%2F%21440x270%2Fquality%2F90', '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 'photo', 1, 1501524144, 1501524144),
(6, 1, 'https://a2-q.mafengwo.net/s10/M00/B5/23/wKgBZ1j20d-Aa2XkADCLL5lB2DE92.jpeg?imageMogr2%2Fthumbnail%2F%21440x270r%2Fgravity%2FCenter%2Fcrop%2F%21440x270%2Fquality%2F90', '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 'photo', 1, 1501524144, 1501524144),
(7, 1, 'https://a2-q.mafengwo.net/s10/M00/B5/23/wKgBZ1j20d-Aa2XkADCLL5lB2DE92.jpeg?imageMogr2%2Fthumbnail%2F%21440x270r%2Fgravity%2FCenter%2Fcrop%2F%21440x270%2Fquality%2F90', '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 'photo', 1, 1501524144, 1501524144),
(8, 1, 'https://a2-q.mafengwo.net/s10/M00/B5/23/wKgBZ1j20d-Aa2XkADCLL5lB2DE92.jpeg?imageMogr2%2Fthumbnail%2F%21440x270r%2Fgravity%2FCenter%2Fcrop%2F%21440x270%2Fquality%2F90', '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 'photo', 1, 1501524144, 1501524144),
(9, 1, 'https://a2-q.mafengwo.net/s10/M00/B5/23/wKgBZ1j20d-Aa2XkADCLL5lB2DE92.jpeg?imageMogr2%2Fthumbnail%2F%21440x270r%2Fgravity%2FCenter%2Fcrop%2F%21440x270%2Fquality%2F90', '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 'photo', 1, 1501524144, 1501524144),
(10, 1, 'https://a2-q.mafengwo.net/s10/M00/B5/23/wKgBZ1j20d-Aa2XkADCLL5lB2DE92.jpeg?imageMogr2%2Fthumbnail%2F%21440x270r%2Fgravity%2FCenter%2Fcrop%2F%21440x270%2Fquality%2F90', '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 'photo', 1, 1501524144, 1501524144),
(11, 1, 'https://a2-q.mafengwo.net/s10/M00/B5/23/wKgBZ1j20d-Aa2XkADCLL5lB2DE92.jpeg?imageMogr2%2Fthumbnail%2F%21440x270r%2Fgravity%2FCenter%2Fcrop%2F%21440x270%2Fquality%2F90', '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 'photo', 0, 1501524144, 1501524144),
(12, 1, 'https://a2-q.mafengwo.net/s10/M00/B5/23/wKgBZ1j20d-Aa2XkADCLL5lB2DE92.jpeg?imageMogr2%2Fthumbnail%2F%21440x270r%2Fgravity%2FCenter%2Fcrop%2F%21440x270%2Fquality%2F90', '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 'photo', 1, 1501524144, 1501524144),
(13, 1, 'http://b1-q.mafengwo.net/s10/M00/B2/C9/wKgBZ1k6VayAWXdpAAJx9kBCIRM03.jpeg?imageMogr2/thumbnail/!220x150r/gravity/Center/crop/!220x150/quality/90', '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 'photo', 1, 1501525126, 1501525126),
(14, 1, 'tmp_1166517312o6zAJs9HkQKYUsVmUfoXtpxINqOc49c49ad7c4e1beb42ad974b7944f2d8a.jpg', '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 'photo', 1, 1507822624, 1507822624),
(15, 1, 'tmp_1166517312o6zAJs9HkQKYUsVmUfoXtpxINqOc44f677c3f9418e31cbf564b21d9aa732.jpg', '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 'photo', 1, 1507822871, 1507822871),
(16, 1, 'tmp_1166517312o6zAJs9HkQKYUsVmUfoXtpxINqOc7db457af790c472290d70101f6291753.jpg', '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 'photo', 1, 1507823015, 1507823015),
(17, 1, 'tmp_1166517312o6zAJs9HkQKYUsVmUfoXtpxINqOcb3b14df56426a0aad17c2ad36ecd91e6.jpg', '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 'photo', 1, 1507823182, 1507823182);

-- --------------------------------------------------------

--
-- 表的结构 `gym_question`
--

DROP TABLE IF EXISTS `gym_question`;
CREATE TABLE `gym_question` (
  `id` int(15) UNSIGNED NOT NULL COMMENT '自动增长id主键',
  `question_value` varchar(100) NOT NULL COMMENT '问题的内容',
  `is_choose` tinyint(2) NOT NULL DEFAULT '1' COMMENT '是否为选择',
  `multiple_choice` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '是否为多选：1:是多选，0:不是多选',
  `create_time` int(15) UNSIGNED NOT NULL COMMENT '创建的时间',
  `types_id` int(15) UNSIGNED NOT NULL COMMENT '分类的id'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='问卷调查的问题表';

--
-- 插入之前先把表清空（truncate） `gym_question`
--

TRUNCATE TABLE `gym_question`;
--
-- 转存表中的数据 `gym_question`
--

INSERT INTO `gym_question` (`id`, `question_value`, `is_choose`, `multiple_choice`, `create_time`, `types_id`) VALUES
(1, '定期体检', 1, 0, 1507877888, 1),
(2, '医院检查', 1, 0, 1507877888, 1),
(3, '身体觉知', 1, 0, 1507877888, 1),
(4, '运动损伤', 1, 0, 1507877888, 1),
(5, '医生医嘱训练建议', 0, 0, 1507877888, 1),
(6, '吸烟', 1, 0, 1507877888, 1);

-- --------------------------------------------------------

--
-- 表的结构 `gym_questionnaire_answer`
--

DROP TABLE IF EXISTS `gym_questionnaire_answer`;
CREATE TABLE `gym_questionnaire_answer` (
  `id` int(15) UNSIGNED NOT NULL COMMENT '自动增长的id主键',
  `question_id` int(15) UNSIGNED NOT NULL COMMENT '问题的id',
  `answer_id` varchar(100) NOT NULL COMMENT '答案的id',
  `supplement_id` int(15) UNSIGNED NOT NULL DEFAULT '0' COMMENT '补充答案的id',
  `supplement_value` varchar(255) NOT NULL DEFAULT '' COMMENT '补充答案的内容',
  `gym_id` char(64) NOT NULL COMMENT '查询对应健身房的id',
  `create_time` int(15) UNSIGNED NOT NULL COMMENT '创建的时间',
  `health_id` int(15) UNSIGNED NOT NULL COMMENT '当前对应的健康状况的内容'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='问卷回答表';

--
-- 插入之前先把表清空（truncate） `gym_questionnaire_answer`
--

TRUNCATE TABLE `gym_questionnaire_answer`;
-- --------------------------------------------------------

--
-- 表的结构 `gym_question_preset`
--

DROP TABLE IF EXISTS `gym_question_preset`;
CREATE TABLE `gym_question_preset` (
  `id` int(15) NOT NULL COMMENT '自动增长编号',
  `question_id` int(15) NOT NULL COMMENT '问题编号',
  `answer` varchar(50) NOT NULL COMMENT '答案',
  `supplement_after_selection` tinyint(2) NOT NULL COMMENT '选择后补充',
  `type_status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '答案的状态，目前还没有用处'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='问题欲设答案表';

--
-- 插入之前先把表清空（truncate） `gym_question_preset`
--

TRUNCATE TABLE `gym_question_preset`;
--
-- 转存表中的数据 `gym_question_preset`
--

INSERT INTO `gym_question_preset` (`id`, `question_id`, `answer`, `supplement_after_selection`, `type_status`) VALUES
(1, 1, '是', 1, 1),
(2, 1, '否', 0, 1),
(3, 2, '是', 1, 1),
(4, 2, '否', 0, 1),
(5, 3, '是', 1, 1),
(6, 3, '否', 0, 1),
(7, 4, '是', 1, 1),
(8, 4, '否', 0, 1),
(9, 5, '是', 1, 1),
(10, 5, '否', 0, 1),
(11, 6, '是', 1, 1),
(12, 6, '否', 0, 1);

-- --------------------------------------------------------

--
-- 表的结构 `gym_question_types`
--

DROP TABLE IF EXISTS `gym_question_types`;
CREATE TABLE `gym_question_types` (
  `id` int(15) UNSIGNED NOT NULL COMMENT '自动增长id主键',
  `title` varchar(100) NOT NULL COMMENT '分类名称',
  `create_time` int(15) UNSIGNED NOT NULL COMMENT '创建的时间',
  `gym_id` char(64) NOT NULL COMMENT '这个分类的创建者'
) ENGINE=MEMORY DEFAULT CHARSET=utf8mb4 COMMENT='问题类型表';

--
-- 插入之前先把表清空（truncate） `gym_question_types`
--

TRUNCATE TABLE `gym_question_types`;
--
-- 转存表中的数据 `gym_question_types`
--

INSERT INTO `gym_question_types` (`id`, `title`, `create_time`, `gym_id`) VALUES
(1, '安全状况调查', 1507877888, '73afd98fb6d8f78d9cc75955cb79c29b'),
(2, '饮食规律', 1507877888, '73afd98fb6d8f78d9cc75955cb79c29b'),
(3, '类别', 1507877888, '73afd98fb6d8f78d9cc75955cb79c29b'),
(4, '静态体态评估', 1507877888, '73afd98fb6d8f78d9cc75955cb79c29b'),
(5, '体适能测/体功能测试', 1507877888, '73afd98fb6d8f78d9cc75955cb79c29b'),
(6, '运动规划', 1507877888, '73afd98fb6d8f78d9cc75955cb79c29b'),
(7, '训练课时安排', 1507877888, '73afd98fb6d8f78d9cc75955cb79c29b'),
(8, '工作室附属服务', 1507877888, '73afd98fb6d8f78d9cc75955cb79c29b'),
(9, '工作室训练指导宗旨及训练意义', 1507877888, '73afd98fb6d8f78d9cc75955cb79c29b');

-- --------------------------------------------------------

--
-- 表的结构 `gym_reservation_course`
--

DROP TABLE IF EXISTS `gym_reservation_course`;
CREATE TABLE `gym_reservation_course` (
  `id` int(15) UNSIGNED NOT NULL COMMENT '自动增长的键',
  `uniqid` varchar(64) NOT NULL,
  `reservation_person_id` char(64) NOT NULL COMMENT '会员的编码',
  `covenant_holder` char(64) NOT NULL COMMENT '受约人的编码',
  `create_time` int(15) UNSIGNED NOT NULL COMMENT '创建时间',
  `status` int(1) UNSIGNED NOT NULL DEFAULT '2' COMMENT '0:拒绝预约，1：同意预约，2：等待决定',
  `gym_id` char(64) NOT NULL COMMENT '教练所在的健身房id',
  `message` varchar(255) NOT NULL DEFAULT '' COMMENT '用户的留言'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='预约课程的信息';

--
-- 插入之前先把表清空（truncate） `gym_reservation_course`
--

TRUNCATE TABLE `gym_reservation_course`;
--
-- 转存表中的数据 `gym_reservation_course`
--

INSERT INTO `gym_reservation_course` (`id`, `uniqid`, `reservation_person_id`, `covenant_holder`, `create_time`, `status`, `gym_id`, `message`) VALUES
(1, 'dea_59ddf19d269e5', '8f8571f7071cc356587d597eeea9bdea', '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 1507717536, 2, '21754deec45f9f55a2ce5955f364dd90', ''),
(2, 'dea_59de4f89677d3', '8f8571f7071cc356587d597eeea9bdea', '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 1507741587, 2, '21754deec45f9f55a2ce5955f364dd90', '');

-- --------------------------------------------------------

--
-- 表的结构 `gym_see_activity`
--

DROP TABLE IF EXISTS `gym_see_activity`;
CREATE TABLE `gym_see_activity` (
  `activity_id` int(13) UNSIGNED NOT NULL COMMENT '活动的编号',
  `uuid` varchar(64) NOT NULL COMMENT '参加活动的人',
  `create_time` int(15) UNSIGNED NOT NULL DEFAULT '0' COMMENT '创建时间',
  `status` int(1) UNSIGNED NOT NULL DEFAULT '1' COMMENT '参加的状态1:确认参加0:确认不能参加',
  `id` int(15) UNSIGNED NOT NULL COMMENT '主键'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='参加活动明细的查询';

--
-- 插入之前先把表清空（truncate） `gym_see_activity`
--

TRUNCATE TABLE `gym_see_activity`;
--
-- 转存表中的数据 `gym_see_activity`
--

INSERT INTO `gym_see_activity` (`activity_id`, `uuid`, `create_time`, `status`, `id`) VALUES
(5, '8f8571f7071cc356587d597eeea9bdea', 1507718295, 1, 1);

-- --------------------------------------------------------

--
-- 表的结构 `gym_show_reviews`
--

DROP TABLE IF EXISTS `gym_show_reviews`;
CREATE TABLE `gym_show_reviews` (
  `create_time` int(15) UNSIGNED NOT NULL COMMENT '主键',
  `content` varchar(255) NOT NULL COMMENT '评论的内容',
  `reviews_presion_id` varchar(64) NOT NULL COMMENT '评论人的编号',
  `range_view` int(2) UNSIGNED NOT NULL COMMENT '可见的范围:1所有人都可以看见,2:只有老板健身房的老板可以看见,3:只有健身房的教练可以看见',
  `circle_id` int(15) UNSIGNED NOT NULL COMMENT '展示消息的内容',
  `id` int(15) UNSIGNED NOT NULL COMMENT '主键'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='课程动态评论表';

--
-- 插入之前先把表清空（truncate） `gym_show_reviews`
--

TRUNCATE TABLE `gym_show_reviews`;
-- --------------------------------------------------------

--
-- 表的结构 `gym_signing_fitness_instructor`
--

DROP TABLE IF EXISTS `gym_signing_fitness_instructor`;
CREATE TABLE `gym_signing_fitness_instructor` (
  `gym_id` char(64) NOT NULL COMMENT '健身房的编号',
  `member_id` char(64) NOT NULL COMMENT '会员的编号',
  `fitness_instructor_id` char(64) NOT NULL COMMENT '当前教练的编号',
  `bought_class` int(4) NOT NULL COMMENT '当前会员已经购买的课时',
  `rest_of_class` int(4) NOT NULL COMMENT '当前会员剩余的课时',
  `create_time` int(15) UNSIGNED NOT NULL COMMENT '创建的时间可以理解为签约的时间',
  `last_update_time` int(15) UNSIGNED NOT NULL COMMENT '最后修改的时间(也可以作为会员与教练解约的时间)',
  `status` int(1) UNSIGNED NOT NULL DEFAULT '1' COMMENT '签约的状态1:已经签约,0:已经解约',
  `id` int(15) UNSIGNED NOT NULL COMMENT '主键'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='会员与教练签约的模块';

--
-- 插入之前先把表清空（truncate） `gym_signing_fitness_instructor`
--

TRUNCATE TABLE `gym_signing_fitness_instructor`;
--
-- 转存表中的数据 `gym_signing_fitness_instructor`
--

INSERT INTO `gym_signing_fitness_instructor` (`gym_id`, `member_id`, `fitness_instructor_id`, `bought_class`, `rest_of_class`, `create_time`, `last_update_time`, `status`, `id`) VALUES
('21754deec45f9f55a2ce5955f364dd90', '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 0, 0, 1499251792, 1504858470, 0, 1),
('73afd98fb6d8f78d9cc75955cb79c29b', '8f8571f7071cc356587d597eeea9bdea', '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 110, 60, 1507720284, 1507721102, 1, 28);

-- --------------------------------------------------------

--
-- 表的结构 `gym_sign_gym`
--

DROP TABLE IF EXISTS `gym_sign_gym`;
CREATE TABLE `gym_sign_gym` (
  `fitness_instructor_id` char(64) NOT NULL COMMENT '教练的标识',
  `gym_id` char(64) NOT NULL COMMENT '健身房的标识',
  `create_time` varchar(100) NOT NULL DEFAULT '0' COMMENT '签约的时间',
  `last_update_time` int(15) UNSIGNED NOT NULL DEFAULT '0' COMMENT '最后修改的时间可以理解为解约的时间',
  `status` int(1) UNSIGNED NOT NULL DEFAULT '1' COMMENT '健身房与教练的签约状态1:已经签约,0:已经解约',
  `id` int(15) UNSIGNED NOT NULL COMMENT '自动增长id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='教练签约健身房';

--
-- 插入之前先把表清空（truncate） `gym_sign_gym`
--

TRUNCATE TABLE `gym_sign_gym`;
--
-- 转存表中的数据 `gym_sign_gym`
--

INSERT INTO `gym_sign_gym` (`fitness_instructor_id`, `gym_id`, `create_time`, `last_update_time`, `status`, `id`) VALUES
('528a11f3378b0ee3bc40d8a3125b2668a8524cd8', '21754deec45f9f55a2ce5955f364dd90', '1499326982', 1499326982, 0, 1),
('528a11f3378b0ee3bc40d8a3125b2668a8524cd8', '73afd98fb6d8f78d9cc75955cb79c29b', '0', 0, 1, 2);

-- --------------------------------------------------------

--
-- 表的结构 `gym_thumb_up`
--

DROP TABLE IF EXISTS `gym_thumb_up`;
CREATE TABLE `gym_thumb_up` (
  `id` int(15) UNSIGNED NOT NULL COMMENT '自动id',
  `uuid` char(64) NOT NULL COMMENT '点赞用户的编号',
  `create_time` int(15) UNSIGNED NOT NULL COMMENT '点赞用户的编号',
  `circle_id` int(15) UNSIGNED NOT NULL COMMENT '对于哪一条用户的点赞'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='点赞表(圈子功能的附属表)';

--
-- 插入之前先把表清空（truncate） `gym_thumb_up`
--

TRUNCATE TABLE `gym_thumb_up`;
--
-- 转存表中的数据 `gym_thumb_up`
--

INSERT INTO `gym_thumb_up` (`id`, `uuid`, `create_time`, `circle_id`) VALUES
(1, '8f8571f7071cc356587d597eeea9bdea', 1507717781, 23),
(2, '8f8571f7071cc356587d597eeea9bdea', 1507722826, 16),
(3, '8f8571f7071cc356587d597eeea9bdea', 1507803799, 0),
(4, '8f8571f7071cc356587d597eeea9bdea', 1507804358, 24);

-- --------------------------------------------------------

--
-- 表的结构 `gym_transcript`
--

DROP TABLE IF EXISTS `gym_transcript`;
CREATE TABLE `gym_transcript` (
  `id` int(15) UNSIGNED NOT NULL COMMENT '自动增长id作为主键使用',
  `fitness_id` char(64) NOT NULL COMMENT '用户的唯一标识',
  `create_time` int(15) UNSIGNED NOT NULL COMMENT '创建时间',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT '1' COMMENT '1：可以显示，0：为不可以显示',
  `last_update_time` int(15) UNSIGNED NOT NULL COMMENT '用户的最后的修改时间',
  `member_id` char(64) NOT NULL COMMENT '会员的编号'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='成绩单表';

--
-- 插入之前先把表清空（truncate） `gym_transcript`
--

TRUNCATE TABLE `gym_transcript`;
--
-- 转存表中的数据 `gym_transcript`
--

INSERT INTO `gym_transcript` (`id`, `fitness_id`, `create_time`, `status`, `last_update_time`, `member_id`) VALUES
(57, '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 1502949392, 1, 1502949392, '528a11f3378b0ee3bc40d8a3125b2668a8524cd8'),
(56, '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 1502948872, 1, 1502948872, '528a11f3378b0ee3bc40d8a3125b2668a8524cd8'),
(58, '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 1504858675, 1, 1504858675, '8f8571f7071cc356587d597eeea9bdea'),
(59, '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 1504858682, 1, 1504858682, '8f8571f7071cc356587d597eeea9bdea'),
(60, '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 1507634830, 1, 1507634830, 'undefined'),
(61, '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 1507634830, 1, 1507634830, 'undefined'),
(62, '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 1507634999, 1, 1507634999, '8f8571f7071cc356587d597eeea9bdea'),
(63, '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 1507635009, 1, 1507635009, '8f8571f7071cc356587d597eeea9bdea'),
(64, '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 1507635029, 1, 1507635029, '8f8571f7071cc356587d597eeea9bdea'),
(65, '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 1507635051, 1, 1507635051, '8f8571f7071cc356587d597eeea9bdea'),
(66, '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 1507660792, 1, 1507660792, 'undefined'),
(67, '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 1507689806, 1, 1507689806, '8f8571f7071cc356587d597eeea9bdea'),
(68, '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 1507689806, 1, 1507689806, '8f8571f7071cc356587d597eeea9bdea'),
(69, '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 1507689807, 1, 1507689807, '8f8571f7071cc356587d597eeea9bdea'),
(70, '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 1507718514, 1, 1507718514, '8f8571f7071cc356587d597eeea9bdea'),
(71, '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', 1507804312, 1, 1507804312, '8f8571f7071cc356587d597eeea9bdea');

-- --------------------------------------------------------

--
-- 表的结构 `gym_transcript_content`
--

DROP TABLE IF EXISTS `gym_transcript_content`;
CREATE TABLE `gym_transcript_content` (
  `id` int(15) UNSIGNED NOT NULL COMMENT '主键自动增长id几乎没有任何的用处',
  `photo` varchar(100) NOT NULL COMMENT '过去的照片',
  `create_time` int(15) UNSIGNED NOT NULL COMMENT '创建的时间',
  `last_update_time` int(15) UNSIGNED NOT NULL COMMENT '最后修改的时间',
  `weight` double NOT NULL COMMENT '体重',
  `three_dimension` varchar(20) NOT NULL COMMENT '三维',
  `bmi` double NOT NULL COMMENT 'bmi指数',
  `transcript_id` int(15) UNSIGNED NOT NULL COMMENT '成绩单的id'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='成绩单';

--
-- 插入之前先把表清空（truncate） `gym_transcript_content`
--

TRUNCATE TABLE `gym_transcript_content`;
--
-- 转存表中的数据 `gym_transcript_content`
--

INSERT INTO `gym_transcript_content` (`id`, `photo`, `create_time`, `last_update_time`, `weight`, `three_dimension`, `bmi`, `transcript_id`) VALUES
(55, 'tmp_693598214o6zAJs2dBCQBEizrcpbQ9aJtdKrs30c98632e719a9f4883a604ffbd1d448.png', 1502949392, 1502949392, 32, '32', 32, 57),
(54, 'tmp_693598214o6zAJs2dBCQBEizrcpbQ9aJtdKrs37bdf2a41ca21af5c269d5315bc28457.png', 1502949392, 1502949392, 45, '54', 54, 57),
(52, 'tmp_693598214o6zAJs2dBCQBEizrcpbQ9aJtdKrsc8b8ea8a3e13c34a18d7deaf428112ab.png', 1502948872, 1502948872, 12, '12,12,12', 12, 56),
(53, 'tmp_693598214o6zAJs2dBCQBEizrcpbQ9aJtdKrsc9abcbb1741525b4ecc7120d3aa15a4c.png', 1502948872, 1502948872, 12, '12', 12, 56),
(56, 'tmp_cdd7f6c7d4a0cdb96022d1176e9d56d1135e7be69cf797df.jpg', 1504858675, 1504858675, 22, '22', 33, 58),
(57, 'tmp_222f9f2abd5b2d5dfa1bf8bcaa5fb0fab4e69517f0cfb2dc.jpg', 1504858675, 1504858675, 64, '58', 13, 58),
(58, 'tmp_cdd7f6c7d4a0cdb96022d1176e9d56d1135e7be69cf797df.jpg', 1504858682, 1504858682, 22, '22', 33, 59),
(59, 'tmp_222f9f2abd5b2d5dfa1bf8bcaa5fb0fab4e69517f0cfb2dc.jpg', 1504858682, 1504858682, 64, '58', 13, 59),
(60, 'tmp_e811812392c56d4412f435030b5971bf4dc0d131ae408395.jpg', 1507804312, 1507804312, 50, '12,12', 150, 71),
(61, 'tmp_fd3371961b2a0d0497757b00403c85ab86a5bf38c58311e4.jpg', 1507804312, 1507804312, 120, '15,15', 150, 71);

-- --------------------------------------------------------

--
-- 表的结构 `gym_user_info`
--

DROP TABLE IF EXISTS `gym_user_info`;
CREATE TABLE `gym_user_info` (
  `id` int(11) UNSIGNED NOT NULL COMMENT '自动增长id仅用于计数',
  `name` char(80) NOT NULL COMMENT '用户的姓名(最多不能超过20个汉字,英文80个字)',
  `nick_name` char(80) NOT NULL COMMENT '用户的昵称(最多不能超过20个汉字,英文80个字)',
  `phone_num` char(13) DEFAULT NULL COMMENT '用户的电话(仅作为用户的联系电话使用)',
  `email_address` varchar(100) DEFAULT NULL COMMENT '用户的电子邮箱地址',
  `password` char(64) NOT NULL COMMENT '最长是32位的字符串',
  `gender` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '用户的性别(0未知,1男,2女)',
  `create_time` int(20) UNSIGNED NOT NULL COMMENT '用户的创建时间',
  `note` varchar(200) DEFAULT NULL COMMENT '用户的其他备注内容(最多不能超过200个字节)',
  `uuid` varchar(64) NOT NULL COMMENT '用户的唯一表示(由系统生成)',
  `hash_value` char(64) NOT NULL COMMENT '用户登录的标识符',
  `last_login_time` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '最后登录的时间',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '用户的状态,开启:1,0:关闭',
  `head_figure` text NOT NULL COMMENT '头像',
  `motto` varchar(100) NOT NULL DEFAULT '写一个格言吧！' COMMENT '用户的格言'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='用户信息表(此信息表仅用于存储用户的信息)';

--
-- 插入之前先把表清空（truncate） `gym_user_info`
--

TRUNCATE TABLE `gym_user_info`;
--
-- 转存表中的数据 `gym_user_info`
--

INSERT INTO `gym_user_info` (`id`, `name`, `nick_name`, `phone_num`, `email_address`, `password`, `gender`, `create_time`, `note`, `uuid`, `hash_value`, `last_login_time`, `status`, `head_figure`, `motto`) VALUES
(1, '郭鑫强', '王鑫鑫', '18811382869', '850528319@qq.com', '19c8cb15ce57ff9f1356423f83d266065daaea42', 2, 144251120, NULL, '528a11f3378b0ee3bc40d8a3125b2668a8524cd8', '5f0334f091f1a31754aa4dee482a8edb11db50e8', 1501478995, 1, 'tmp_1166517312o6zAJs9HkQKYUsVmUfoXtpxINqOc0122e2783af1c5464e16eab10812f698.jpg', '才华'),
(2, '郭鑫强', '郭鑫强', '18513583718', 'g98051815@aliyun.com', '19c8cb15ce57ff9f1356423f83d266065daaea42', 0, 1500442393, NULL, '7e5de181e191436c5dd1596eef7a9627', '', 1500442393, 1, '2016-04-14_1460643868.png', ''),
(3, 'name', 'nickname', '17600201478', '1827115989@qq.com', 'b0399d2029f64d445bd131ffaa399a42d2f8e7dc', 0, 1501470585, NULL, '66b651a71a0114555886597e9fe6eed5', '47e18d87d9b66258defdbc76f762e7168d3d8878', 1501470585, 1, '2016-04-14_1460644209.png', ''),
(12, '', '王鑫', '18811382869', '18404967791@163.com', 'f7c3bc1d808e04732adf679965ccc34ca7ae3441', 1, 1501490688, NULL, '8f8571f7071cc356587d597eeea9bdea', 'e0c71d545821b5ffc619380855332ee8975610a6', 1501490688, 1, '2016-04-14_1460644779.png', '这个是我自己的格言');

-- --------------------------------------------------------

--
-- 表的结构 `gym_week_nutritious_meal`
--

DROP TABLE IF EXISTS `gym_week_nutritious_meal`;
CREATE TABLE `gym_week_nutritious_meal` (
  `id` int(15) UNSIGNED NOT NULL COMMENT '自动增长id作为键进行使用',
  `week` int(7) UNSIGNED NOT NULL COMMENT '星期几'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='健身房营养餐的颗粒度';

--
-- 插入之前先把表清空（truncate） `gym_week_nutritious_meal`
--

TRUNCATE TABLE `gym_week_nutritious_meal`;
--
-- 转存表中的数据 `gym_week_nutritious_meal`
--

INSERT INTO `gym_week_nutritious_meal` (`id`, `week`) VALUES
(1, 0),
(2, 1),
(3, 2),
(4, 3),
(5, 4),
(6, 5),
(7, 6);

-- --------------------------------------------------------

--
-- 表的结构 `manual_operation`
--

DROP TABLE IF EXISTS `manual_operation`;
CREATE TABLE `manual_operation` (
  `id` int(15) UNSIGNED NOT NULL COMMENT '主键',
  `number_of` varchar(100) NOT NULL COMMENT '数量',
  `member_id` char(64) NOT NULL COMMENT '减少数量的会员的id',
  `gym_id` char(64) NOT NULL COMMENT '健身房的编号',
  `fitness_id` char(64) NOT NULL COMMENT '教练的id',
  `number_of_pre_changes` int(15) UNSIGNED NOT NULL COMMENT '修改前的数量',
  `create_time` int(15) UNSIGNED NOT NULL COMMENT '创建的时间',
  `last_update_time` int(15) UNSIGNED NOT NULL COMMENT '手动减少课时的数量',
  `operation` tinyint(1) UNSIGNED NOT NULL COMMENT '操作的方式,1:为较少,2:为增加'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='手动减少课时';

--
-- 插入之前先把表清空（truncate） `manual_operation`
--

TRUNCATE TABLE `manual_operation`;
--
-- Indexes for dumped tables
--

--
-- Indexes for table `gym_answer_supplement`
--
ALTER TABLE `gym_answer_supplement`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gym_question_supplement_id_IDX` (`id`) USING BTREE,
  ADD KEY `gym_question_supplement_answer_id_IDX` (`answer_id`) USING BTREE;

--
-- Indexes for table `gym_auth_group`
--
ALTER TABLE `gym_auth_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gym_auth_group_relation`
--
ALTER TABLE `gym_auth_group_relation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gym_auth_group_relation_group_id_IDX` (`group_id`,`uid`) USING BTREE;

--
-- Indexes for table `gym_auth_rule`
--
ALTER TABLE `gym_auth_rule`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gym_captcha_code`
--
ALTER TABLE `gym_captcha_code`
  ADD PRIMARY KEY (`code_key`),
  ADD KEY `gym_captcha_code_code_key_IDX` (`code_key`) USING BTREE;

--
-- Indexes for table `gym_certificate_of_merit`
--
ALTER TABLE `gym_certificate_of_merit`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gym_certificate_of_merit_id_IDX` (`id`,`sender_id`,`recipient_id`,`gym_id`) USING BTREE;

--
-- Indexes for table `gym_circle`
--
ALTER TABLE `gym_circle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gym_circle_type_IDX` (`type`,`create_time`,`last_update_time`,`uuid`,`index_id`) USING BTREE;

--
-- Indexes for table `gym_circle_comment`
--
ALTER TABLE `gym_circle_comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uid` (`uid`),
  ADD KEY `circle_id` (`circle_id`),
  ADD KEY `uid_2` (`uid`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `gym_course`
--
ALTER TABLE `gym_course`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gym_course_id_IDX` (`id`,`create_time`) USING BTREE;

--
-- Indexes for table `gym_course_content`
--
ALTER TABLE `gym_course_content`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gym_course_content_id_IDX` (`id`,`course_id`) USING BTREE;

--
-- Indexes for table `gym_course_dynamic`
--
ALTER TABLE `gym_course_dynamic`
  ADD PRIMARY KEY (`id`),
  ADD KEY `NewTable_id_IDX` (`id`,`coach_id`,`gym_id`,`member_id`,`course_content_id`) USING BTREE;

--
-- Indexes for table `gym_course_dynamic_img`
--
ALTER TABLE `gym_course_dynamic_img`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gym_course_dynamic_img_course_dynamic_id_IDX` (`course_dynamic_id`,`id`) USING BTREE;

--
-- Indexes for table `gym_disable`
--
ALTER TABLE `gym_disable`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uuid` (`uuid`);

--
-- Indexes for table `gym_exercise_vent`
--
ALTER TABLE `gym_exercise_vent`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gym_exercise_vent_id_IDX` (`id`,`parent`) USING BTREE;

--
-- Indexes for table `gym_gym_activity`
--
ALTER TABLE `gym_gym_activity`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gym_gym_activity_id_IDX` (`id`,`title`,`date_time`) USING BTREE,
  ADD KEY `gym_gym_activity_uuid_IDX` (`uuid`) USING BTREE;

--
-- Indexes for table `gym_gym_info`
--
ALTER TABLE `gym_gym_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `boss` (`boss`),
  ADD KEY `id` (`id`),
  ADD KEY `unique_id` (`unique_id`);

--
-- Indexes for table `gym_hour_course`
--
ALTER TABLE `gym_hour_course`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gym_hour_course_fitness_id_IDX` (`fitness_id`,`member_id`,`gym_id`) USING BTREE;

--
-- Indexes for table `gym_icinfo`
--
ALTER TABLE `gym_icinfo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gym_icinfo_id_IDX` (`id`) USING BTREE,
  ADD KEY `gym_icinfo_uuid_IDX` (`uuid`) USING BTREE;

--
-- Indexes for table `gym_indicators`
--
ALTER TABLE `gym_indicators`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gym_helath_index_uuid_IDX` (`uuid`,`create_time`) USING BTREE;

--
-- Indexes for table `gym_member_sign_fitness_application`
--
ALTER TABLE `gym_member_sign_fitness_application`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gym_member_sign_fitness_application_id_IDX` (`id`) USING BTREE;

--
-- Indexes for table `gym_motto_tpl`
--
ALTER TABLE `gym_motto_tpl`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gym_motto_tpl_id_IDX` (`id`) USING BTREE;

--
-- Indexes for table `gym_notifycation`
--
ALTER TABLE `gym_notifycation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `unique_identification` (`addressee`);

--
-- Indexes for table `gym_notifycation_content`
--
ALTER TABLE `gym_notifycation_content`
  ADD KEY `notify_id` (`notify_id`);

--
-- Indexes for table `gym_nutritious_meal`
--
ALTER TABLE `gym_nutritious_meal`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gym_nutritious_meal_id_IDX` (`id`) USING BTREE,
  ADD KEY `gym_nutritious_meal_gym_id_IDX` (`gym_id`) USING BTREE;

--
-- Indexes for table `gym_nutritious_meal_history`
--
ALTER TABLE `gym_nutritious_meal_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gym_nutritious_meal_id_IDX` (`id`) USING BTREE,
  ADD KEY `gym_nutritious_meal_gym_id_IDX` (`gym_id`) USING BTREE;

--
-- Indexes for table `gym_other_account`
--
ALTER TABLE `gym_other_account`
  ADD PRIMARY KEY (`id`),
  ADD KEY `platform_type` (`platform_type`,`openid`);

--
-- Indexes for table `gym_photo_album`
--
ALTER TABLE `gym_photo_album`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gym_photo_album_id_IDX` (`id`,`uuid`) USING BTREE;

--
-- Indexes for table `gym_question`
--
ALTER TABLE `gym_question`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gym_question_id_IDX` (`id`,`question_value`,`types_id`) USING BTREE;

--
-- Indexes for table `gym_questionnaire_answer`
--
ALTER TABLE `gym_questionnaire_answer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `NewTable_id_IDX` (`id`,`health_id`) USING BTREE;

--
-- Indexes for table `gym_question_preset`
--
ALTER TABLE `gym_question_preset`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gym_question_types`
--
ALTER TABLE `gym_question_types`
  ADD PRIMARY KEY (`id`),
  ADD KEY `question_types_id_IDX` (`id`,`title`) USING BTREE;

--
-- Indexes for table `gym_reservation_course`
--
ALTER TABLE `gym_reservation_course`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gym_reservation_course_id_IDX` (`id`,`reservation_person_id`,`covenant_holder`) USING BTREE,
  ADD KEY `uniqid` (`uniqid`);

--
-- Indexes for table `gym_see_activity`
--
ALTER TABLE `gym_see_activity`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gym_see_activiry_activity_id_IDX` (`activity_id`,`uuid`,`create_time`) USING BTREE,
  ADD KEY `gym_see_activity_id_IDX` (`id`) USING BTREE;

--
-- Indexes for table `gym_show_reviews`
--
ALTER TABLE `gym_show_reviews`
  ADD PRIMARY KEY (`id`),
  ADD KEY `show_reviews_id_IDX` (`id`,`circle_id`) USING BTREE;

--
-- Indexes for table `gym_signing_fitness_instructor`
--
ALTER TABLE `gym_signing_fitness_instructor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gym_signing_fitness_instructor_gym_id_IDX` (`gym_id`,`member_id`,`fitness_instructor_id`,`status`) USING BTREE,
  ADD KEY `gym_signing_fitness_instructor_id_IDX` (`id`) USING BTREE;

--
-- Indexes for table `gym_sign_gym`
--
ALTER TABLE `gym_sign_gym`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gym_sign_gym_fitness_instructor_id_IDX` (`fitness_instructor_id`,`gym_id`,`status`) USING BTREE,
  ADD KEY `gym_sign_gym_id_IDX` (`id`) USING BTREE;

--
-- Indexes for table `gym_thumb_up`
--
ALTER TABLE `gym_thumb_up`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gym_thumb_up_id_IDX` (`id`) USING BTREE,
  ADD KEY `gym_thumb_up_circle_id_IDX` (`circle_id`) USING BTREE;

--
-- Indexes for table `gym_transcript`
--
ALTER TABLE `gym_transcript`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gym_transcript_id_IDX` (`id`,`fitness_id`) USING BTREE;

--
-- Indexes for table `gym_transcript_content`
--
ALTER TABLE `gym_transcript_content`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gym_transcript_id_IDX` (`id`) USING BTREE;

--
-- Indexes for table `gym_user_info`
--
ALTER TABLE `gym_user_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gym_week_nutritious_meal`
--
ALTER TABLE `gym_week_nutritious_meal`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gym_week_nutritious_meal_id_IDX` (`id`) USING BTREE,
  ADD KEY `gym_week_nutritious_meal_week_IDX` (`week`) USING BTREE;

--
-- Indexes for table `manual_operation`
--
ALTER TABLE `manual_operation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `manual_operation_id_IDX` (`id`,`gym_id`,`fitness_id`,`member_id`) USING BTREE;

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `gym_answer_supplement`
--
ALTER TABLE `gym_answer_supplement`
  MODIFY `id` int(15) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '自动增长id主键', AUTO_INCREMENT=7;

--
-- 使用表AUTO_INCREMENT `gym_auth_group`
--
ALTER TABLE `gym_auth_group`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '自动增长的id内容', AUTO_INCREMENT=4;

--
-- 使用表AUTO_INCREMENT `gym_auth_group_relation`
--
ALTER TABLE `gym_auth_group_relation`
  MODIFY `id` int(15) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键', AUTO_INCREMENT=9;

--
-- 使用表AUTO_INCREMENT `gym_auth_rule`
--
ALTER TABLE `gym_auth_rule`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '自动增长id', AUTO_INCREMENT=9;

--
-- 使用表AUTO_INCREMENT `gym_certificate_of_merit`
--
ALTER TABLE `gym_certificate_of_merit`
  MODIFY `id` int(15) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键', AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `gym_circle`
--
ALTER TABLE `gym_circle`
  MODIFY `id` int(15) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '自动增长类型', AUTO_INCREMENT=25;

--
-- 使用表AUTO_INCREMENT `gym_circle_comment`
--
ALTER TABLE `gym_circle_comment`
  MODIFY `id` int(15) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键', AUTO_INCREMENT=38;

--
-- 使用表AUTO_INCREMENT `gym_course`
--
ALTER TABLE `gym_course`
  MODIFY `id` int(15) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '作为主键使用', AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `gym_course_content`
--
ALTER TABLE `gym_course_content`
  MODIFY `id` int(15) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id做为主键使用', AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `gym_course_dynamic`
--
ALTER TABLE `gym_course_dynamic`
  MODIFY `id` int(15) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '课程动态表,连接圈子功能', AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `gym_course_dynamic_img`
--
ALTER TABLE `gym_course_dynamic_img`
  MODIFY `id` int(15) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键', AUTO_INCREMENT=72;

--
-- 使用表AUTO_INCREMENT `gym_disable`
--
ALTER TABLE `gym_disable`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `gym_exercise_vent`
--
ALTER TABLE `gym_exercise_vent`
  MODIFY `id` int(15) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '自动增长id作为主见使用', AUTO_INCREMENT=4;

--
-- 使用表AUTO_INCREMENT `gym_gym_activity`
--
ALTER TABLE `gym_gym_activity`
  MODIFY `id` int(15) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '活动的序列id', AUTO_INCREMENT=6;

--
-- 使用表AUTO_INCREMENT `gym_gym_info`
--
ALTER TABLE `gym_gym_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自动增长id仅用于计数', AUTO_INCREMENT=11;

--
-- 使用表AUTO_INCREMENT `gym_hour_course`
--
ALTER TABLE `gym_hour_course`
  MODIFY `id` int(15) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键', AUTO_INCREMENT=3;

--
-- 使用表AUTO_INCREMENT `gym_icinfo`
--
ALTER TABLE `gym_icinfo`
  MODIFY `id` int(15) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键', AUTO_INCREMENT=4;

--
-- 使用表AUTO_INCREMENT `gym_indicators`
--
ALTER TABLE `gym_indicators`
  MODIFY `id` int(15) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键', AUTO_INCREMENT=5;

--
-- 使用表AUTO_INCREMENT `gym_member_sign_fitness_application`
--
ALTER TABLE `gym_member_sign_fitness_application`
  MODIFY `id` int(15) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键', AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `gym_motto_tpl`
--
ALTER TABLE `gym_motto_tpl`
  MODIFY `id` int(15) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '自动增长id作为键';

--
-- 使用表AUTO_INCREMENT `gym_notifycation`
--
ALTER TABLE `gym_notifycation`
  MODIFY `id` int(15) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- 使用表AUTO_INCREMENT `gym_nutritious_meal`
--
ALTER TABLE `gym_nutritious_meal`
  MODIFY `id` int(15) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '自动增长id作为键使用', AUTO_INCREMENT=9;

--
-- 使用表AUTO_INCREMENT `gym_nutritious_meal_history`
--
ALTER TABLE `gym_nutritious_meal_history`
  MODIFY `id` int(15) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '自动增长id作为键使用', AUTO_INCREMENT=3;

--
-- 使用表AUTO_INCREMENT `gym_other_account`
--
ALTER TABLE `gym_other_account`
  MODIFY `id` int(15) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键', AUTO_INCREMENT=9;

--
-- 使用表AUTO_INCREMENT `gym_photo_album`
--
ALTER TABLE `gym_photo_album`
  MODIFY `id` int(15) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键', AUTO_INCREMENT=18;

--
-- 使用表AUTO_INCREMENT `gym_question`
--
ALTER TABLE `gym_question`
  MODIFY `id` int(15) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '自动增长id主键', AUTO_INCREMENT=7;

--
-- 使用表AUTO_INCREMENT `gym_questionnaire_answer`
--
ALTER TABLE `gym_questionnaire_answer`
  MODIFY `id` int(15) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '自动增长的id主键';

--
-- 使用表AUTO_INCREMENT `gym_question_preset`
--
ALTER TABLE `gym_question_preset`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT COMMENT '自动增长编号', AUTO_INCREMENT=13;

--
-- 使用表AUTO_INCREMENT `gym_question_types`
--
ALTER TABLE `gym_question_types`
  MODIFY `id` int(15) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '自动增长id主键', AUTO_INCREMENT=10;

--
-- 使用表AUTO_INCREMENT `gym_reservation_course`
--
ALTER TABLE `gym_reservation_course`
  MODIFY `id` int(15) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '自动增长的键', AUTO_INCREMENT=3;

--
-- 使用表AUTO_INCREMENT `gym_see_activity`
--
ALTER TABLE `gym_see_activity`
  MODIFY `id` int(15) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键', AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `gym_show_reviews`
--
ALTER TABLE `gym_show_reviews`
  MODIFY `id` int(15) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键';

--
-- 使用表AUTO_INCREMENT `gym_signing_fitness_instructor`
--
ALTER TABLE `gym_signing_fitness_instructor`
  MODIFY `id` int(15) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键', AUTO_INCREMENT=29;

--
-- 使用表AUTO_INCREMENT `gym_sign_gym`
--
ALTER TABLE `gym_sign_gym`
  MODIFY `id` int(15) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '自动增长id', AUTO_INCREMENT=3;

--
-- 使用表AUTO_INCREMENT `gym_thumb_up`
--
ALTER TABLE `gym_thumb_up`
  MODIFY `id` int(15) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '自动id', AUTO_INCREMENT=5;

--
-- 使用表AUTO_INCREMENT `gym_transcript`
--
ALTER TABLE `gym_transcript`
  MODIFY `id` int(15) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '自动增长id作为主键使用', AUTO_INCREMENT=72;

--
-- 使用表AUTO_INCREMENT `gym_transcript_content`
--
ALTER TABLE `gym_transcript_content`
  MODIFY `id` int(15) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键自动增长id几乎没有任何的用处', AUTO_INCREMENT=62;

--
-- 使用表AUTO_INCREMENT `gym_user_info`
--
ALTER TABLE `gym_user_info`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '自动增长id仅用于计数', AUTO_INCREMENT=13;

--
-- 使用表AUTO_INCREMENT `gym_week_nutritious_meal`
--
ALTER TABLE `gym_week_nutritious_meal`
  MODIFY `id` int(15) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '自动增长id作为键进行使用', AUTO_INCREMENT=8;

--
-- 使用表AUTO_INCREMENT `manual_operation`
--
ALTER TABLE `manual_operation`
  MODIFY `id` int(15) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键';
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
