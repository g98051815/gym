<?php
    header('Access-Control-Allow-Origin: *');
    /**
    *  thinkphp5 入口配置内容
    */
    define('APP_PATH',__DIR__.'/../application/');
    define('CONF_PATH',__DIR__.'/../config/');
    define('PUBLIC_PATH',__DIR__.'/');
    define('EXTEND_PATH',__DIR__.'/../extension/');
    require __DIR__.'/../vendor/topthink/framework/start.php';
