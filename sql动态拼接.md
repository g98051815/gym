#sql 动态拼接的好处

sql动态拼接的好处共有如下四点

1. 通过整体封装以及高度抽象解决不同数据库之间的驱动问题
2. 避免编码中的sql使用错误
3. 可以对缓存数据库进行高度抽象
4. 减少编成人员的代码数量



#1.通过整体封装以及高度抽象解决不同数据库之间的驱动问题

  目前市面上的数据库产品种类很多，并且与数据库交互的方法也不禁相同,基本统计后最具代表性的数据库有如下7种

  1. mysql  关系型数据库
  2. mssql  关系型数据库
  3. mongodb 文档型数据库
  4. redis key=>value数据库
  5. memcached key=>value 数据库
  6. oracle 关系型数据库
  7. postgresql 关系型数据库

  上述其中数据库交互语句如下(以查询语句为例)

##mysql
```sql
  SELECT * FROM TABLENAME WHERE 1
```

##mssql
```sql
  SELECT * FROM TABLENAME WHERE 1
```

##mongodb
```sql
  db.collection.find(query,projection)
```

---------------------------------
缓存数据库
##memcached
```sql
  get key
```

##oracle
```sql
SELECT * FROM TABLENAME WHERE 1
```

##postgresql

```sql
SELECT * FROM TABLENAME WHERE 1
```
